var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-terser');
var trycatch = require('gulp-trycatch-closure');
var merge = require('merge-stream');
var babel = require("gulp-babel");

gulp.task('autoprefixer', function () {
    var postcss = require('gulp-postcss');
    var sourcemaps = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer');
    return gulp.src('css/*.css', {base: './css'})
            .pipe(sourcemaps.init())
            .pipe(postcss([autoprefixer()]))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest('./css'));
});
gulp.task("fontAwesomeCustomBuild", function() {
	return gulp
		.src([
			"./preview/libs/js/FontAwesome/fontawesome.js",
			"./preview/libs/js/FontAwesome/*.edit.js"
		])
		.pipe(trycatch())
		.pipe(concat("FontAwesomeCustom.js"))
		.pipe( rename({ extname: '.min.js' }) )
        .pipe( sourcemaps.init() )
        .pipe( uglify() )
        .pipe( sourcemaps.write('./') )
		.pipe(gulp.dest("./preview/libs/js"));
});
gulp.task("scripts", function() {
	return gulp
		.src([
			"./preview/libs/js/jquery.min.js",
			"./preview/libs/js/popper.min.js",
			"./preview/libs/js/*.js",
			"./preview/config/js/config.js",
			"./preview/config/js/Languages.js",
			"./preview/config/js/images.js",
			"./preview/config/js/forms.js",
			"./preview/config/js/swiper.js",
			"./preview/config/js/booking.js",
			"./preview/config/js/theme.js",
			"./preview/config/js/youtube.js",
			"./preview/config/js/masonry.js",
			"./preview/config/js/sharer.js"
		])
		.pipe(trycatch())
		.pipe(concat("all.js"))
		.pipe(gulp.dest("./preview/config/js"))
		.pipe( rename({ extname: '.min.js' }) )
		.pipe( sourcemaps.init() )
        .pipe( uglify() )
        .pipe( sourcemaps.write('./') )
		.pipe(gulp.dest("./preview/config/js"));
});

gulp.task("installAllLibs", function() {
	var buildLibJS = gulp
		.src([
			"./preview/node_modules/jquery/dist/jquery.min.js",
			"./preview/node_modules/jquery/dist/jquery.min.js.map",
			"./preview/node_modules/bootstrap/dist/js/bootstrap.min.js",
			"./preview/node_modules/bootstrap/dist/js/bootstrap.min.js.map",
			"./preview/node_modules/flatpickr/dist/flatpickr.min.js",
			"./preview/node_modules/popper.js/dist/umd/popper.min.js",
			"./preview/node_modules/popper.js/dist/umd/popper.min.js.map",
			"./preview/node_modules/swiper/dist/js/swiper.min.js",
			"./preview/node_modules/swiper/dist/js/swiper.min.js.map",
			"./preview/node_modules/svg-injector/dist/svg-injector.min.js",
			"./preview/node_modules/svg-injector/dist/svg-injector.min.js.map",
			"./preview/node_modules/jquery-viewport-checker/src/jquery.viewportchecker.js",
			"./preview/node_modules/intersection-observer/intersection-observer.js",
			"./preview/node_modules/jquery-validation/dist/jquery.validate.min.js",
			//        './preview/node_modules/masonry-layout/dist/masonry.pkgd.min.js',
			"./preview/node_modules/isotope-layout/dist/isotope.pkgd.min.js",
			"./preview/node_modules/imagesloaded/imagesloaded.pkgd.min.js",
			"./preview/node_modules/cookieconsent/build/cookieconsent.min.js",
			"./preview/node_modules/aos/dist/aos.js",
			"./preview/node_modules/js-cookie/src/js.cookie.js",
			"./preview/node_modules/@babel/polyfill/dist/polyfill.min.js"

		])
		.pipe(gulp.dest("./preview/libs/js"));

	var buildLibCSS = gulp
		.src([
			"./preview/node_modules/swiper/dist/css/swiper.min.css",
			"./preview/node_modules/flatpickr/dist/themes/airbnb.css",
			"./preview/node_modules/cookieconsent/build/cookieconsent.min.css"
		])
		.pipe(gulp.dest("./preview/libs/css"));

	var buildLibSCSS = gulp
		.src([
			"./preview/node_modules/bootstrap/scss/**/*",
			"./preview/node_modules/aos/src/sass/**/*",
		])
		.pipe(gulp.dest("./preview/libs/scss"));

	var buildFontAwesome = gulp
		.src([
			"./preview/node_modules/@fortawesome/fontawesome-pro/js/fontawesome.js",
			"./preview/node_modules/@fortawesome/fontawesome-pro/js/brands.js",
			"./preview/node_modules/@fortawesome/fontawesome-pro/js/light.js",
			"./preview/node_modules/@fortawesome/fontawesome-pro/js/regular.js",
			"./preview/node_modules/@fortawesome/fontawesome-pro/js/solid.js"
		])
		.pipe(gulp.dest("./preview/libs/js/FontAwesome"));

	var scripts = gulp
		.src([
			"./preview/libs/js/jquery.min.js",
			"./preview/libs/js/popper.min.js",
			"./preview/libs/js/*.js",
			"./preview/config/js/config.js",
			"./preview/config/js/Languages.js",
			"./preview/config/js/images.js",
			"./preview/config/js/forms.js",
			"./preview/config/js/swiper.js",
			"./preview/config/js/booking.js",
			"./preview/config/js/theme.js",
			"./preview/config/js/youtube.js",
			"./preview/config/js/masonry.js",
			"./preview/config/js/sharer.js"
		])
		.pipe(trycatch())
		.pipe(concat("all.js"))
		.pipe(gulp.dest("./preview/config/js"));

	return merge(
		buildLibJS,
		buildLibCSS,
		buildLibSCSS,
		buildFontAwesome,
		scripts
	);
});

gulp.task("iesupport", function() {
	return gulp 
	.src(["./preview/modules/**/*.js", "!./preview/modules/**/*.ie.js", "!./preview/modules/**/*.min.js"])
		.pipe(babel({
			plugins: ["./preview/node_modules/@babel/plugin-transform-template-literals", "./preview/node_modules/@babel/plugin-transform-block-scoping", "./preview/node_modules/@babel/plugin-transform-for-of"],
			presets: [
				["./preview/node_modules/@babel/preset-env", { "modules": false }]
			]
			  
		}))
		.pipe( rename({ extname: '.ie.js' }) )
		.pipe( sourcemaps.init() )
		.pipe( uglify() )
		.pipe( sourcemaps.write('./') )
		.pipe( gulp.dest("./preview/modules"));
});

gulp.task("default", function() {
	var iesupport = gulp 
	.src(["./preview/modules/**/*.js", "!./preview/modules/**/*.ie.js", "!./preview/modules/**/*.min.js"])
		.pipe(babel({
			plugins: ["./preview/node_modules/@babel/plugin-transform-template-literals", "./preview/node_modules/@babel/plugin-transform-block-scoping", "./preview/node_modules/@babel/plugin-transform-for-of" ],
			presets: [
				["./preview/node_modules/@babel/preset-env", { "modules": false }]
			]
		}))
		.pipe( rename({ extname: '.ie.js' }) )
		.pipe( sourcemaps.init() )
		.pipe( uglify() )
		.pipe( sourcemaps.write('./') )
		.pipe( gulp.dest("./preview/modules"));
	var minify = gulp
	.src(["./preview/modules/**/*.js", "!./preview/modules/**/*.ie.js", "!./preview/modules/**/*.min.js"])
        .pipe( rename({ extname: '.min.js' }) )
        .pipe( sourcemaps.init() )
        .pipe( uglify() )
        .pipe( sourcemaps.write('./') )
		.pipe( gulp.dest("./preview/modules"));
	return merge(
			iesupport,
			minify
		);
});
//******************************************
// 
// Single Gulp Tasks
//
//******************************************
//gulp.task('buildLibJS', function(){
//    return gulp.src([
//        '../node_modules/jquery/dist/jquery.min.js',
//        '../node_modules/jquery/dist/jquery.min.js.map',
//        '../node_modules/bootstrap/dist/js/bootstrap.min.js',
//        '../node_modules/bootstrap/dist/js/bootstrap.min.js.map',
//        '../node_modules/flatpickr/dist/flatpickr.min.js',
//        '../node_modules/popper.js/dist/umd/popper.min.js',
//        '../node_modules/popper.js/dist/umd/popper.min.js.map',
//        '../node_modules/swiper/dist/js/swiper.min.js',
//        '../node_modules/swiper/dist/js/swiper.min.js.map',
//        '../node_modules/svg-injector/dist/svg-injector.min.js',
//        '../node_modules/svg-injector/dist/svg-injector.min.js.map',
//        '../node_modules/jquery-viewport-checker/src/jquery.viewportchecker.js',
//        '../node_modules/intersection-observer/intersection-observer.js',
//        '../node_modules/js-yaml/dist/js-yaml.min.js',
//        '../node_modules/jquery-validation/dist/jquery.validate.min.js',
//        '../node_modules/masonry-layout/dist/masonry.pkgd.min.js',
//        '../node_modules/imagesloaded/imagesloaded.pkgd.min.js'
//    ])
//    .pipe(gulp.dest('../libs/js'));
//});
//gulp.task('buildLibCSS', function(){
//    return gulp.src([
//        '../node_modules/swiper/dist/css/swiper.min.css',
//        '../node_modules/flatpickr/dist/themes/airbnb.css'
//    ])
//    .pipe(gulp.dest('../libs/css'));
//});
//gulp.task('buildLibSCSS', function(){
//    return gulp.src([
//        '../node_modules/bootstrap/scss/**/*'
//    ])
//    .pipe(gulp.dest('../libs/scss'));
//});
//gulp.task('buildFontAwesome', function () {
//   return gulp.src([
//        '../node_modules/@fortawesome/fontawesome-free/js/all.js'
//    ])
//    .pipe(rename('FontAwesomeAll.js'))
//    .pipe(gulp.dest('./js')); 
//});