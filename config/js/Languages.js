/* global Storage, defaultLanguage, projectName, configFile, jsyaml, getLanguages, token, fetch */

var language; //initial set in index.php
var getLanguage;
var urlLanguage;
function getLanguageFunc() {
        if (typeof (Storage) !== "undefined") {
        //localStorage.setItem(projectName + "_language", defaultLanguage);
        getLanguage = localStorage.getItem(projectName + "_language");
        if (getLanguage && (getLanguage !== 'default')) {
            language = '_' + getLanguage;
            urlLanguage = getLanguage;
        } else {
            language = '';
            urlLanguage = defaultLanguage;
        }
    } else {
        //sessionStorage.setItem(projectName + "_language", defaultLanguage);
        getLanguage = sessionStorage.getItem(projectName + "_language");
        if (getLanguage && (getLanguage !== 'default')) {
            language = '_' + getLanguage;
            urlLanguage = getLanguage;
            
        } else {
            language = '';
            urlLanguage = defaultLanguage;
        }
    }
};

function setLanguageFunc(lang) {
    if (typeof (Storage) !== "undefined") {
        if ((lang === defaultLanguage) || (lang === 'default')) {
            localStorage.setItem(projectName + "_language", 'default');
            getLanguageFunc();
        } else {
            localStorage.setItem(projectName + "_language", lang);
            getLanguageFunc();
        }
    } else {
        if ((lang === defaultLanguage) || (lang === 'default')) {
            sessionStorage.setItem(projectName + "_language", 'default');
            getLanguageFunc();
        } else {
            sessionStorage.setItem(projectName + "_language", lang);
            getLanguageFunc();
        }
    }
}

//moved in index.php
////fetch main language collection with system translations
//var languageArray = [];
//var translations = false;
//fetch(getLanguages + token)
//    .then(data => data.json())
//    .then(data => languageArray = data)
//    .then(translations = true);


