//*********************
//v1.0
//Initial
//1.1
//Added multihotel support
//**********************
/* global bookingID */
function bookingFormFunc(module) { 
    $('.datepicker').focus(function() {
        this.blur();
    });
    var arrival = flatpickr('#' + module + 'DatepickerArrival', {
//        dateFormat: 'd/m/Y',
        altFormat: 'd/m/Y',
        altInput: true,
        minDate: 'today',
        onChange: function (selectedDates, dateStr) {
            var nextDay = new Date(selectedDates);
            nextDay.setDate(nextDay.getDate() + 1);
            var selected = Date.parse(dateStr);
            var depart = Date.parse(departure.input.value);
            if ((depart <= selected) || !depart) {
                departure.set({
                    minDate: new Date(selectedDates).fp_incr(1)
                });
                departure.setDate(new Date(selectedDates).fp_incr(1));
            }
        }
    });
    var departure = flatpickr('#' + module + 'DatepickerDeparture', {
        altFormat: 'd/m/Y',
        altInput: true,
        minDate: new Date().fp_incr(1)
    });
    if (arrival.altInput.value > departure.altInput.value) {
        alert("Arrival date cannot be after the departure date!");
    }
    $('#'+module+'Form').submit(function (e) {
        e.preventDefault();
        var $this = $(this);
        var url = '';

        var values = {};
        $.each($this.serializeArray(), function (k, v) {
            values[v.name] = v.value;
        });

        if (!bookingID) {
            url = 'https://'+values.hotel+'.book-onlinenow.net/index.aspx?Page=19&';
        } else {
            url = 'https://'+bookingID+'.book-onlinenow.net/index.aspx?Page=19&';
        }
        delete values.hotel;
               
        values.arrival = arrival.altInput.value;
        values.departure = departure.altInput.value;
        
        $.each(values, function (k, v) {
            if (v.length > 0) {
                url += k + '=' + v + '&';
            }
        });
        url = url.substr(0, url.length - 1);
        //uncomment below if results in _self
        //window.location.href = url;

        //comment below if you need result in _self
        var BookSearch = window.open(url, "_blank");
    });
};
