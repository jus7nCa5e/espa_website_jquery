//*********************
//v1.0
//Initial
//v.1.1
//Added captive cart support 
//v.2.0
//Changed the way forms work. Inplmented with PHP due to security reasons
//
//**********************
/* global saveToken, formName, formURL, formData */

function sendFormFunc(formName, formData) {
    $.ajax({
        type: 'post',
        url: 'tmpl/sendForm.php',
        data: formData,
        success: function (data) {
            console.log(data);
        }
    });
}
function sendCartFunc(formName, cartData, formData) {
    console.log(formData);
    var data = [];
    $.each(cartData, function (index, value) { //create array from incomming data (formdata)
        data.push(value);
    });
    var final = [];
    $.each(formData, function (index, value) { //push data from form
        final.push(value);
    });
    $.each(data, function (index, value) { //build objects inside the final array
        final.push({name: '::::::::::::::::::::::::::::::::::::::::::::::::::::::::::', value: ''});
        if (value.addons) {
            final.push({name: 'item' + (index + 1), value: value.quantity + ' x ' + value.name + ' | Addons: ' + value.addons});
        } else {
            final.push({name: 'item' + (index + 1), value: value.quantity + ' x ' + value.name});
        }

    });
    $.ajax({
        type: 'post',
        url: 'tmpl/sendForm.php',
        data: final,
        success: function (data) {
            console.log(data);
        }
    });
}
