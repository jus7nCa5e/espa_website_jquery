//*********************
//v1.0
//Initial
//v.1.1
//Added functions for lazy loading images 
//v.1.2
//Added callback functions to all functions in the file
//Added some description to all functions in the file
//v.1.3
//Added responsive function
//Added Lazy load function
//**********************
/* global fetch, getImage, token */

//standart function for <img /> elements. 
function getImagesFunc(ImageID, ImageWidth, ImageHeight, index, callback) {
    fetch(getImage + token, {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            src: ImageID,
            m: 'thumbnail',
            w: ImageWidth, // width
            h: ImageHeight, // height
            q: 80, // quality
            d: true, // include full domain path
            b64: false, // return base64 encoded image string
            o: false
        })
    })
    .then(function (response) {
        return response.text();
    })
    .catch(function (error) {
        console.log('Request failed', error);
    })
    .then(function (image) {
        $('.' + index).attr('src', image);

        if (callback && typeof(callback) === "function") {
            callback();
        }
    });
}

//Function for <img /> elements with lazy load. Adding content with data-src 
function getImagesSrcFunc(ImageID, ImageWidth, ImageHeight, index, callback) {
    fetch(getImage + token, {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            src: ImageID,
            m: 'thumbnail',
            w: ImageWidth, // width
            h: ImageHeight, // height
            q: 80, // quality
            d: true, // include full domain path
            b64: false, // return base64 encoded image string
            o: false
        })
    })
    .then(function (response) {
        return response.text();
    })
    .catch(function (error) {
        console.log('Request failed', error);
    })
    .then(function (image) {
        $('.' + index).attr('data-src', image);
    })
    .then(function () {
        //lazy load with IntersectionObserver

        const images = document.querySelectorAll('[data-src]');
        const config = {
            rootMargin: '0px 0px 100px 0px',
            threshold: 0
        };
        let loaded = 0;
        if (isIE) {
           $.each(images, function(index, image){
                preloadImage(image);
            });
        } else {
            // It is supported, load the images
            let observer = new IntersectionObserver(function (entries, self) {
                entries.forEach(function(entry) {
                    if (entry.isIntersecting) {
                        // console.log(`Image ${entry.target.src} is in the viewport!`);
                        preloadImage(entry.target);
                        // Stop watching and load the image
                        self.unobserve(entry.target);
                    }
                });
            }, config);

            images.forEach(function(image) {
                observer.observe(image);
            });
        }


        function preloadImage(img) {
            const src = img.getAttribute('data-src');
            if (!src) {
                return;
            }
            img.src = src;
            
            if (callback && typeof(callback) === "function") {
                callback();
            }
        }

    });
}
//Function for responsive <img /> elements. 
function getRespImagesFunc(ImageID, ImageWidth, ImageHeight, index, imgWidth, callback) {
    $.each(imgWidth, function(index, value){
        fetch(getImage + token, {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                src: ImageID,
                m: 'thumbnail',
                w: imgWidth, // width
                h: 200, // height
                q: 80, // quality
                d: true, // include full domain path
                b64: false, // return base64 encoded image string
                o: false
            })
        })
        .then(function (response) {
            return response.text();
        })
        .catch(function (error) {
            console.log('Request failed', error);
        })
        .then(function (image) {
            console.log(image)
            $('.' + index).attr('srcset', image);

        });
    });
    fetch(getImage + token, {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            src: ImageID,
            m: 'thumbnail',
            w: ImageWidth, // width
            h: ImageHeight, // height
            q: 80, // quality
            d: true, // include full domain path
            b64: false, // return base64 encoded image string
            o: false
        })
    })
    .then(function (response) {
        return response.text();
    })
    .catch(function (error) {
        console.log('Request failed', error);
    })
    .then(function (image) {
        $('.' + index).attr('src', image);

        if (callback && typeof(callback) === "function") {
            callback();
        }
    });
}


//standart function for adding background image to element via inline style. 
function getImagesBgFunc(ImageID, ImageWidth, ImageHeight, index, callback) {
    fetch(getImage + token, {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            src: ImageID,
            m: 'thumbnail',
            w: ImageWidth, // width
            h: ImageHeight, // height
            q: 80, // quality
            d: true, // include full domain path
            b64: false      // return base64 encoded image string
        })
    })
    .then(function (response) {
        return response.text();
    })
    .catch(function (error) {
        console.log('Request failed', error);
    })
    .then(function (image) {
        $('.' + index).css('background-image', 'url("' + image + '")');

        if (callback && typeof(callback) === "function") {
            callback();
        }
    });
}
//Function for background images with lazy load. Adding content with data-background
function getImagesSrcBgFunc(ImageID, ImageWidth, ImageHeight, index, callback) {
    fetch(getImage + token, {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            src: ImageID,
            m: 'thumbnail',
            w: ImageWidth, // width
            h: ImageHeight, // height
            q: 80, // quality
            d: true, // include full domain path
            b64: false      // return base64 encoded image string
        })
    })
            .then(function (response) {
                return response.text();
            })
            .catch(function (error) {
                console.log('Request failed', error);
            })
            .then(function (image) {
                $('.' + index).attr('data-background', image);
            })
            .then(function () {
                //lazy load with IntersectionObserver

                const images = document.querySelectorAll('[data-background]');
                const config = {
                    rootMargin: '0px 0px 100px 0px',
                    threshold: 0
                };
                let loaded = 0;
                if (isIE) {
                    $.each(images, function(index, image){
                        preloadImage(image);
                    });
                } else {
                    // It is supported, load the images
                    let observer = new IntersectionObserver(function (entries, self) {
                        entries.forEach(function(entry) {
                            if (entry.isIntersecting) {
                                // console.log(`Image ${entry.target.src} is in the viewport!`);
                                preloadImage(entry.target);
                                // Stop watching and load the image
                                self.unobserve(entry.target);
                            }
                        });
                    }, config);

                    images.forEach(function(image) {
                        observer.observe(image);
                    });
                }
                function preloadImage(img) {
                    const src = img.getAttribute('data-background');
                    if (!src) {
                        return;
                    }
                    img.setAttribute('style', 'background-image: url("' + src + '")');
                    
                    if (callback && typeof(callback) === "function") {
                        callback();
                    }
                }
            });
}
//Inlinning svg files
function injectSVGFunc() {
    //inject SVG
    var mySVGsToInject = document.querySelectorAll('img.inject-me');
    // Do the injection
    SVGInjector(mySVGsToInject);
}
function LazyImages() {
    //lazy load with IntersectionObserver

    const images = document.querySelectorAll('[data-src]');
    const config = {
        rootMargin: '0px 0px 100px 0px',
        threshold: 0
    };
    let loaded = 0;
    if (isIE) {
        $.each(images, function (index, image) {
            preloadImage(image);
        });
    } else {
        // It is supported, load the images
        let observer = new IntersectionObserver(function (entries, self) {
            entries.forEach(function (entry) {
                
                if (entry.isIntersecting) {
//                     console.log(`Image ${entry.target.src} is in the viewport!`);
                    preloadImage(entry.target);
                    // Stop watching and load the image
                    self.unobserve(entry.target);
                }
            });
        }, config);

        images.forEach(function (image) {
            observer.observe(image);
        });
    }


    function preloadImage(img) {
        const src = img.getAttribute('data-src');
        if (!src) {
            return;
        }
        img.src = src;
    }
}
LazyImages();