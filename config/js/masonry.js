/* 
 * Masonry activation
 * Additional modul
 * installation:
 * 1. npm install masonry-layout --save
 * 1.2 $ npm install isotope-layout --save     //This is the full library. Needed for filtering    
 * 2. npm install imagesloaded --save
 * 3. Gulp buildLibJS
 * v.1.0 initial
 */
function activateMasonry(index) {      //simple function mainly for galleries
    grid = $('.grid' + index).masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        transitionDuration: 0
    });
    grid.imagesLoaded().progress(function () {
        grid.masonry('layout');
  });
}

