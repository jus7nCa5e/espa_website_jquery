
//This module will look for container with class="newsletter-container"
//******************************************
var newsletter = 'newsletter'; //module name

function loadNewsletter() {
    invalidMessage = '';
    $('.newsletter-container').append(
        '<div class="input-group mb-3" id="newsletterForm">'+
            '<input type="email" name="Email" id="newsletterInput" class="form-control bg-info text-white border-0" placeholder="'+translationArray['your_email' + language]+'" aria-label="'+translationArray['newsletter_subscribe_input' + language]+'" aria-describedby="newsletterSubscribe" required>'+
            '<div class="input-group-append">'+
                '<button class="btn btn-secondary text-light" id="newsletterSubscribe" type="button"><i class="fas fa-share-square fa-lg"></i></button>'+
            '</div>'+
            '<div class="invalid-feedback"></div>'+
            '<div class="valid-feedback">'+translationArray['newsletter_thankyou' + language]+'</div>'+
        '</div>'
    );
    $('#newsletterSubscribe').on('click', function () {
        newsLetterValidate();
    });
}
function newsLetterValidate() {
    var email = $('#newsletterInput');
    if (validateMail(email)) {
        existEmail(email);
    }
}
function validateMail(email) {
    var mailformat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.val().match(mailformat)) {
        email.removeClass('is-invalid');
        email.addClass('is-valid');
        return true;
    } else {
        $('#newsletterForm .invalid-feedback').html(translationArray['newsletter_invalid_mail' + language]);
        email.removeClass('is-valid');
        email.addClass('is-invalid');
        return false;
    }
}
function existEmail(email) {
    fetch(getURL + newsletter + token)
     .then (function(response) {
        return response.json();
    })
    .then (function (data){
        var hasEmail = false;
        $.each(data.entries, function(index, value){
            if (email.val() === value.Email) {
                $('#newsletterForm .invalid-feedback').html(translationArray['newsletter_already_exists' + language]);
                email.removeClass('is-valid');
                email.addClass('is-invalid');
                hasEmail = true;
            } 
        });
        if (!hasEmail) {
            saveEmail(email);
            email.val('');
        }
    });
}
function saveEmail(email) {
    var formData = {};
    formData.data = [{'Email': email.val()}];
    $.ajax({
        type: 'post',
        url: 'tmpl/newsletter.php',
        data: formData,
        success: function (data) {
            console.log(data);
        }
    });
};