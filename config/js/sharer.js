function facebookSharer() {
    window.fbAsyncInit = function () {
        FB.init({
            appId: '415005002691951',
            xfbml: true,
            version: 'v3.1'
        });
        FB.AppEvents.logPageView();
        
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}
function facebookshare() {
    FB.ui({
        method: 'share',
        href: window.location.href,
    }, function (response) {});
}

function twitterSharer() {

    var getWindowOptions = function () {
        var width = 500;
        var height = 350;
        var left = (window.innerWidth / 2) - (width / 2);
        var top = (window.innerHeight / 2) - (height / 2);

        return [
            'resizable,scrollbars,status',
            'height=' + height,
            'width=' + width,
            'left=' + left,
            'top=' + top,
        ].join();
    };
    var twitterBtn = document.querySelector('.twitter-share');
    var twitterText = encodeURIComponent($('meta[name="og:title"]').attr("content"));
    var twitterShareUrl = 'https://twitter.com/intent/tweet?url=' + location.href + '&text=' + twitterText;
    twitterBtn.href = twitterShareUrl; // 1

    twitterBtn.addEventListener('click', function (e) {
        e.preventDefault();
        var win = window.open(twitterShareUrl, 'ShareOnTwitter', getWindowOptions());
        win.opener = null; // 2
    });
}

function linkedinSharer() {
//    var newScript = document.createElement("script");
//    newScript.src = "https://platform.twitter.com/widgets.js";
//    $('body').append(newScript);

    var getWindowOptions = function () {
        var width = 500;
        var height = 350;
        var left = (window.innerWidth / 2) - (width / 2);
        var top = (window.innerHeight / 2) - (height / 2);

        return [
            'resizable,scrollbars,status',
            'height=' + height,
            'width=' + width,
            'left=' + left,
            'top=' + top,
        ].join();
    };
    var linkedinBtn = document.querySelector('.linkedin-share');
    var linkedinTitle = encodeURIComponent($('meta[name="og:title"]').attr("content"));
    var linkedinDescription = encodeURIComponent($('meta[name="og:description"]').attr("content"));
    var linkedinShareUrl = 'https://www.linkedin.com/shareArticle?mini=true&url=' + location.href + '&title=' + linkedinTitle + '&summary=' + linkedinDescription + '&source=' + projectTitle;
    linkedinBtn.href = linkedinShareUrl; // 1
    linkedinBtn.addEventListener('click', function (e) {
        e.preventDefault();
        var win = window.open(linkedinShareUrl, 'ShareOnLinkedin', getWindowOptions());
        win.opener = null; // 2
    });
}