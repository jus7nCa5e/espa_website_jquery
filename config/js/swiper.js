/* global triggerYoutubeVideo */

//*********************
//v1.0
//Initial
//v1.1
//Added manual with parameter after the function is called. Used for adding additional parameters, before init the swiper.
//Added 'activateCarouselThumbsFunc' function for triggering active classes to standart BS carousel with thumbs.
//v1.2
//Added 'activateSwiperGalleryZoomFunc' function for triggering fullscreen slider from gallery page with tabs and youtube videos
//Added 'activateSwiperMultipleFunc' function for triggering slider with multiple slides at once
//v1.3
//Added 'activateSwiperAutoplayFunc' function for autoplay triggering slides. Simmilar to activateSwiperMultipleFunc but with more options
//v1.4
//Added 'activateSwiperVideoGalleryZoomFunc' function for triggering fullscreen slider from youtube videos only
//Added 'activateSwiperImagesGalleryZoomFunc' function for triggering fullscreen slider for images only
//**********************
function activateSwiperFunc(swiperAlias) {
    mySwiper = new Swiper('.' + swiperAlias + '-swiper-container', {
        loop: true,
        init: false, //it will be inited with parameter, after the function is called. 
         
        pagination: {
            el: '.' + swiperAlias + '-swiper-pagination',
            clickable: true
        },
        navigation: {
            nextEl: '.' + swiperAlias + '-swiper-button-next',
            prevEl: '.' + swiperAlias + '-swiper-button-prev'
        }
    });
}
//Swiper with zoom
function activateSwiperZoomFunc(swiperAlias) {

    var swiperThumb = new Swiper('.' + swiperAlias + '-thumb-swiper-container', {
        loop: true,
        slidesPerView: 1,
        navigation: {
            nextEl: '.' + swiperAlias + '-thumb-swiper-button-next',
            prevEl: '.' + swiperAlias + '-thumb-swiper-button-prev'
        },
        pagination: {
            el: '.' + swiperAlias + '-thumb-swiper-pagination',
            clickable: true
        }
    });
    var swiperZoom = new Swiper('.' + swiperAlias + '-zoom-swiper-container', {
        loop: true,
        init: false,
        navigation: {
            nextEl: '.' + swiperAlias + '-zoom-swiper-button-next',
            prevEl: '.' + swiperAlias + '-zoom-swiper-button-prev'
        },
        pagination: {
            el: '.' + swiperAlias + '-zoom-swiper-pagination',
            clickable: true
        }
    });
    swiperZoom.controller.control = swiperThumb;
    swiperThumb.controller.control = swiperZoom;
    $('.zoom').on('click', function () {
        $('.full-screen').show();
        swiperZoom.init();
    });
    $('.close').on('click', function () {
        $('.full-screen').hide();
    });
}
function activateSwiperMultipleFunc(swiperAlias, slidesPerView, galleryVar) {
    if (!slidesPerView) {
        slidesPerView = 1;
    }
    galleryVar = new Swiper('.' + swiperAlias + '-swiper-container', {
        slidesPerView: slidesPerView,
        loop: true,
//        init: false,
        pagination: {
            el: '.' + swiperAlias + '-swiper-pagination',
            clickable: true
        },
        navigation: {
            nextEl: '.' + swiperAlias + '-swiper-button-next',
            prevEl: '.' + swiperAlias + '-swiper-button-prev'
        }
    });
}
function activateSwiperMultipleZoomFunc(swiperAlias, slidesPerView) {
    if (!slidesPerView) {
        slidesPerView = 1;
    }
    swiperThumb = new Swiper('.' + swiperAlias + '-thumb-swiper-container', {
        loop: true,
        slidesPerView: slidesPerView,
        navigation: {
            nextEl: '.' + swiperAlias + '-thumb-swiper-button-next',
            prevEl: '.' + swiperAlias + '-thumb-swiper-button-prev'
        },
        pagination: {
            el: '.' + swiperAlias + '-thumb-swiper-pagination',
            clickable: true
        }
    });
    swiperZoom = new Swiper('.' + swiperAlias + '-zoom-swiper-container', {
        loop: true,
        init: false
    });
    $('.zoom').on('click', function () {
        initialSlide = $(this).data('slide');
        swiperZoom.params.initialSlide = initialSlide;
        swiperZoom.destroyed = false;
        $('.' + swiperAlias + '-zoom-swiper-button-next').on('click', function () {
            swiperZoom.params.allowSlideNext = true;
            swiperZoom.slideNext();
        });
        $('.' + swiperAlias + '-zoom-swiper-button-prev').on('click', function () {
            swiperZoom.params.allowSlidePrev = true;
            swiperZoom.slidePrev();
        });
        $('.full-screen').show();
        console.log(swiperZoom.params.navigation);
        swiperZoom.init();
    });
    $('.close').on('click', function () {
        $('.full-screen').hide();
        swiperZoom.destroy(false, true);
    });
}
function activateSwiperAutoplayFunc(swiperAlias, slidesPerView, spaceBetween, delayInMs) {

    if (!slidesPerView) {
        slidesPerView = 1;
    }
    if (!spaceBetween || (spaceBetween < 0) || (spaceBetween === "")) {
        spaceBetween = 0;
        spaceBetweenSM = 0;
        spaceBetweenMD = 0;
        spaceBetweenLG = 0;
    } else {
        spaceBetweenSM = spaceBetween * 0.25;
        spaceBetweenMD = spaceBetween * 0.50;
        spaceBetweenLG = spaceBetween * 0.75;
    }
    swiperThumb = new Swiper('.' + swiperAlias + '-swiper-container', {
        loop: true,
        autoHeight: false, //if false, equal height
        disableOnInteraction: false,
        slidesPerView: slidesPerView,
        spaceBetween: spaceBetween,
        navigation: {
            nextEl: '.' + swiperAlias + '-swiper-button-next',
            prevEl: '.' + swiperAlias + '-swiper-button-prev'
        },
        pagination: {
            el: '.' + swiperAlias + '-swiper-pagination',
            type: 'bullets',
            dynamicBullets: true,
            clickable: true
        }, 
        autoplay: {
            delay: delayInMs
        }, 
        breakpoints: {
            //to center the points set items container with bootstrap classes: text-center and position-absolute
            576: {
                slidesPerView: 1,
                spaceBetween: spaceBetweenSM
            },
            768: {
                slidesPerView: 2,
                spaceBetween: spaceBetweenMD
            },
            992: {
                slidesPerView: 3,
                spaceBetween: spaceBetweenLG
            }
        }
    });
}


function activateCarouselThumbsFunc(swiperAlias) {
    $('.' + swiperAlias + '-zoom-swiper-wrapper div').first().addClass('active');
    $('.' + swiperAlias + '-thumb-swiper-wrapper div').first().addClass('active');
}


/*
 * Swiper with zoom for gallery with youtube videos
 * @param {type} swiperAlias - unique var when function is calling
 * @param {type} galleryVar - unique name for multiple instances
 * @returns {undefined} - slider for gallery module with categories
 */
var galleryVar;
//mixed
function activateSwiperGalleryZoomFunc(swiperAlias, galleryVar) {
    setTimeout(triggerYoutubeVideo, 1000);
    galleryVar = new Swiper('.' + swiperAlias + '-zoom-swiper-container', {
        loop: false,
        init: false,
        navigation: {
            nextEl: '.' + swiperAlias + '-zoom-swiper-button-next',
            prevEl: '.' + swiperAlias + '-zoom-swiper-button-prev'
          }
    });
    
    $('#' + swiperAlias + '-panel .zoom').on('click', function () {
        initialSlide = $(this).data('slide');
        galleryVar.params.initialSlide = initialSlide;
        galleryVar.slideTo(initialSlide);
        $('.' + swiperAlias + '-zoom-swiper-container.full-screen').show();
        galleryVar.init();
    });
    $('.close').on('click', function () {
        $('.full-screen').hide();
        stopVideo();
    });
    galleryVar.on('slideChange', function () {
        stopVideo();
    });
}
//only for videos
function activateSwiperVideoGalleryZoomFunc(swiperAlias, galleryVar, playerVars) {
    
    galleryVar = new Swiper('.' + swiperAlias + '-zoom-swiper-container', {
        loop: false,
        init: false,
        navigation: {
            nextEl: '.' + swiperAlias + '-zoom-swiper-button-next',
            prevEl: '.' + swiperAlias + '-zoom-swiper-button-prev'
        }
    });

    $('#' + swiperAlias + '-panel .zoom').on('click', function () {
        initialSlide = $(this).data('slide');
        galleryVar.params.initialSlide = initialSlide;
        galleryVar.slideTo(initialSlide);
        $('.' + swiperAlias + '-zoom-swiper-container.full-screen').show();
        galleryVar.init();
    });
   
    
    function triggerYoutubeVideoGallery(Pvars) {
        var temp = $(".iframe-youtube");
        for (var i = 0; i < temp.length; i++) {
            var t = new YT.Player($(temp[i]).attr('id'), {
                videoId: videos[i], //array build in Gallery.js
                playerVars: Pvars,
                events: {
                    'onReady': onPlayerReadyGallery
                }
            });
            players.push(t);
        }
    }
    function onPlayerReadyGallery(event) {
        $('.close').on('click', function () {
            $('.full-screen').hide();
            event.target.stopVideo();
        });
        galleryVar.on('slideChange', function () {
            event.target.stopVideo();
        });
    }
    setTimeout(triggerYoutubeVideoGallery(playerVars), 1000);

    // galleryVar.on('slideChange', function () {
    //     stopVideo();
    // });
}
//only for images
function activateSwiperImagesGalleryZoomFunc(swiperAlias, galleryVar) {
    galleryVar = new Swiper('.' + swiperAlias + '-zoom-swiper-container', {
        loop: false,
        init: false,
        navigation: {
            nextEl: '.' + swiperAlias + '-zoom-swiper-button-next',
            prevEl: '.' + swiperAlias + '-zoom-swiper-button-prev'
        },
        keyboard: {
            enabled: true
        }
    });

    $('#' + swiperAlias + '-panel .zoom').on('click', function () {
        initialSlide = $(this).data('slide');
        galleryVar.params.initialSlide = initialSlide;
        galleryVar.slideTo(initialSlide);
        $('.' + swiperAlias + '-zoom-swiper-container.full-screen').show();
        galleryVar.init();
    });
    $('.close').on('click', function () {
        $('.full-screen').hide();
    });
}

//demos
//$('.image-container').append(
//        '<div class="' + Alias + '-swiper-container swiper-container">' +
//        '<div class="' + Alias + '-swiper-wrapper swiper-wrapper"></div>' +
//        '<div class="' + Alias + '-swiper-pagination swiper-pagination"></div>' +
//        '</div>'
//        );
//$.each(value.Gallery, function (ind, val) {
//    galleryImageID = val.meta.asset;
//    galleryImageWidth = 768;
//    galleryImageHeight = '';
//    $('.' + Alias + '-swiper-wrapper').append(
//            '<div class="swiper-slide"><img class="img-fluid ' + Alias + 'image-slide' + ind + '" id="image-slide' + ind + '" src="' + baseUrl + loader + '"></div>'
//            );
//    getImagesFunc(galleryImageID, galleryImageWidth, galleryImageHeight, Alias + 'image-slide' + ind);
//});
//activateSwiperFunc(Alias);