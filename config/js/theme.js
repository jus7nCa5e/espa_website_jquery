//when the window is scrolling, do stuff - Parallax
$(window).scroll(function () {
    //capture the curren position of the window
    var windowTop = $(window).scrollTop();
    //shift the body background very slowly
    $(".foreground").css("background-position-y", 0 - windowTop / 1.5);
    $(".midground").css("background-position-y", 0 - windowTop / 2);
    $(".background").css("background-position-y", 0 - windowTop / 3);

    //sticky navbar
    if ($(document).scrollTop() > 100) {
        $('header').removeClass('not-sticked');
        $('header').addClass('sticked');
    } else {
        $('header').removeClass('sticked');
        $('header').addClass('not-sticked');
    }
});
//animate
function animatePost() {
    var i;
    var s = 0;
    $('.post.animate-fade').addClass("invisible").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 200
    });
    for (i = -1; i < 3; i++) {
        s = s + 50;
        $('.post.animate-slide-'+i).addClass("invisible").viewportChecker({
            classToAdd: 'visible animated slideInUp',
            offset: 100 + s
        });
    }
    
};
setTimeout(function () {
    animatePost();
}, 500);

//Scroll to Top Arrow
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});