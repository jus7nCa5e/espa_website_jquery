/* 
 * Youtube Javascript API video embed module
 * videos[i] - array build in Gallery.js
 * v1.0 initial
 * v1.1 
 * Added ability to pass player variables and callback function ex.
 * playerVars = {
        'autoplay': 1,
        'modestbranding': 1, 
        'rel': 0,
        'showinfo': 0
    }
    triggerYoutubeVideo(playerVars, callback);
 * v1.2
 * Added new functions 'triggerYoutubeVideoSlider' for fullscreen muted slider and 'triggerYoutubeVideoModal' for videos in modal. 
 * The main function 'triggerYoutubeVideo' should be overwritten, because it's not very good.
 */


/* global YT, videos */

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var players = [];

function triggerYoutubeVideo(Pvars, callback) {
    var temp = $(".iframe-youtube");
    for (var i = 0; i < temp.length; i++) {
        var t = new YT.Player($(temp[i]).attr('id'), {
            videoId: videos[i], //array build in Gallery.js
            playerVars: Pvars
        });
        players.push(t);
    }
    if (callback && typeof(callback) === "function") {
        setTimeout(function(){ callback(); }, 1000);
    }
}
function stopVideo() {
    var temp = $(".iframe-youtube");
    for (var i = 0; i < temp.length; i++) {
        players[i].stopVideo();
    }
}
function playVideo() {
    var temp = $(".iframe-youtube");
    for (var i = 0; i < temp.length; i++) {
        players[i].playVideo();
    }
}

function triggerYoutubeVideoSlider(Pvars) {
    var temp = $(".iframe-youtube");
    for (var i = 0; i < temp.length; i++) {
        var t = new YT.Player($(temp[i]).attr('id'), {
            videoId: videos[i], //array build in Gallery.js
            playerVars: Pvars,
            events: {
                'onReady': onPlayerReadySlider
            }
        });
        players.push(t);
    }
}
function onPlayerReadySlider(event) {
    if ($('.swiper-slide-active').hasClass('bgVideo')) {
        event.target.playVideo();
        event.target.mute();
    }
    mySwiper.on("slideChangeTransitionEnd", function () {
        event.target.stopVideo();
        if ($('.swiper-slide-active').hasClass('bgVideo')) {
            event.target.playVideo();
            event.target.mute();
        }
    });
}

function triggerYoutubeVideoModal(vidID, Pvars, containerID) {
    player = new YT.Player(containerID, {
        videoId: vidID, //array build in main js file
        playerVars: Pvars,
        events: {
            'onReady': onPlayerReadyModal
        }
    });
}

function onPlayerReadyModal(event) {
    $('.modal').on('hidden.bs.modal', function () {
        event.target.stopVideo();
    });
    $('.modal').on('show.bs.modal', function () {
        event.target.playVideo();
    });
}