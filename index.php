<?php 
    session_start();
    //************************
    require 'tmpl/AltoRouter.php'; //include router
    include_once 'tmpl/config.php';
    include_once 'tmpl/modules.php';
    include_once 'tmpl/languages.php';
    include_once 'tmpl/paths.php';
    include_once 'tmpl/routes.php';
    include_once 'tmpl/seo.php';
    include_once 'tmpl/users/session.php';
//    include_once 'tmpl/ui-kit.php'; 
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <?php
        if(isset($_SESSION['login_user'])){
            if ($is_logged) {
                echo '<script>user = "' . $is_logged . '"; </script>';
            } 
        } else {
            echo '<script>user = false; </script>';
        }
    ?>
    <link rel="preconnect" href="<?php echo $streamSRV; ?>">
    <script type="text/javascript">
        
        var streamSRV = <?php echo json_encode($streamSRV); ?>;
        var token = <?php echo json_encode($token); ?>;
        var defaultLanguage = <?php echo json_encode($defaultLanguage); ?>;
        var langArray =  <?php echo json_encode($otherLanguages); ?>;
        var streamSRVuploads = <?php echo json_encode($streamSRVuploads); ?>;
        var mainData = <?php echo json_encode($mainData); ?>;
        var getURL = <?php echo json_encode($getURL); ?>;
		var getSingletoneURL = <?php echo json_encode($getSingletoneURL); ?>;
        var getImage = <?php echo json_encode($getImageJS); ?>;
        var getLanguages = <?php echo json_encode($getLanguages); ?>;
        var getRegions = <?php echo json_encode($getRegions); ?>;
        var loader = <?php echo json_encode($loader); ?>;
        var devider = <?php echo json_encode($devider); ?>;
        var imgPrefix = <?php echo json_encode($imgPrefix); ?>;
        var baseUrl = <?php echo json_encode($pageBaseUrl); ?>;
        var relPath = <?php echo json_encode($routerBase); ?>;
        function loadHeadlessModules(){
        ////    loadNewsletter();
        }
    </script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base id="baseUrl" href="<?php echo $pageBaseUrl;  ?>" />
    <meta http-equiv="Content-Type" content="text/html">
    
    <title><?php echo $metaTitle; ?></title>
    <meta name="title" content='<?php echo $metaTitle; ?>' />
    <meta name="og:title" content='<?php echo $metaTitle; ?>' />
    <meta name="og:description" content='<?php echo $metaDesc; ?>' />
    <meta name="og:image" content='<?php echo $metaImage; ?>' />
    <meta name="description" content='<?php echo $metaDesc; ?>'>
    <meta name="keywords" content='<?php echo $keywords; ?>'>
    <meta property="og:url" content='https://www.europeanspas.eu/'>
    <link rel="canonical" href='https://www.europeanspas.eu/'/>

    <!-- Facebook Static OG -->
    <meta property="og:type" content="website"/>


    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="7 days">
    <link rel="shortcut icon" href="<?php echo $pageBaseUrl;  ?>favicon.ico">
    
    
    <?php echo $headTag; ?>
</head>

<body>
    <?php echo $bodyTag; ?>
    <script type="text/javascript">
        var isIE = /*@cc_on!@*/false || !!document.documentMode, // Internet Explorer 6-11
            isEdge = !isIE && !!window.StyleMedia; // Edge 20+
        // Check if Internet Explorer 6-11 OR Edge 20+
        if(isIE || isEdge) {
            document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/fetch/2.0.3/fetch.js"><\/script>');
            document.write('<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@7/dist/polyfill.min.js"><\/script>');
            document.write('<script src="<?php echo $pageBaseUrl ?>libs/js/intersection-observer.js"><\/script>');
            
        }
        //pass module from php to js
        var urlModule = '<?php echo $module; ?>';
    </script>
    
    <script src="<?php echo $pageBaseUrl ?>config/js/all.min.js" type="text/javascript"></script>
    <?php //if captive
        if ($captive === true) {
            echo '<script src="'.$pageBaseUrl.'libs/simplecart/simpleCart.js" type="text/javascript"></script>';
        } 
    ?>
    <?php //additional libs from param.json
    if ((isset($param['js_external'])) && (is_array($param['js_external']))) {
        foreach ($param['js_external'] as $src) {
            echo '<script async defer type="text/javascript" src="' . $src . '"></script>';
        }
    }
    ?>
    <?php //if captive && Production
        if ($production === true) {
            $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
            if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0; rv:11.0') !== false)) {
                if ($captive === true) {
                    echo '<script src="'.$pageBaseUrl.'modules/cart/cart.ie.js" type="text/javascript"></script>'
                        .'<script src="'.$pageBaseUrl.'modules/HFsections/HFsections.captive.ie.js" type="text/javascript"></script>';
                } else {
                    echo '<script src="'.$pageBaseUrl.'modules/HFsections/HFsections.ie.js" type="text/javascript"></script>';
                }
            } else {
                if ($captive === true) {
                    echo '<script src="'.$pageBaseUrl.'modules/cart/cart.min.js" type="text/javascript"></script>'
                        .'<script src="'.$pageBaseUrl.'modules/HFsections/HFsections.captive.min.js" type="text/javascript"></script>';
                } else {
                    echo '<script src="'.$pageBaseUrl.'modules/HFsections/HFsections.min.js" type="text/javascript"></script>';
                }
            }
        } else {
            if ($captive === true) {
                echo '<script src="'.$pageBaseUrl.'modules/cart/cart.js" type="text/javascript"></script>'
                    .'<script src="'.$pageBaseUrl.'modules/HFsections/HFsections.captive.js" type="text/javascript"></script>';
            } else {
                
                echo '<script src="'.$pageBaseUrl.'modules/HFsections/HFsections.js" type="text/javascript"></script>';
            }
        }
    ?>
    
    <?php //module js from param.json and producion
        if ($production === true) {
            $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
            if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0; rv:11.0') !== false)) {
                foreach ($param['productionIE'] as $src) {
                    echo '<script type="text/javascript" src="' . $pageBaseUrl . $src . '"></script>';
                }
            } else {
                foreach ($param['production'] as $src) {
                    echo '<script type="text/javascript" src="' . $pageBaseUrl . $src . '"></script>';
                }
            }
        }
        else {
            if ((isset($param['js_footer'])) && (is_array($param['js_footer']))) {
                foreach ($param['js_footer'] as $src) {
                    echo '<script type="text/javascript" src="' . $pageBaseUrl . $src . '"></script>';
                }
            }
        }
    ?>
    <script>
        projectName = '<?php echo $projectName; ?>';
        setLanguageFunc('<?php echo $getLang; ?>');
        getLanguageFunc();

        //fetch main language collection with system translations and then load content
        var translationArray = [];
        fetch(getLanguages + token)
            .then (function(response) {
                return response.json();
            })
            .then (function (data){
               translationArray = data[urlLanguage];
            })
            .then(function () {
                getNavigation();
                <?php echo $addLoadContent; ?>
            });
    </script>
    <!--Styles-->
    <link href="<?php echo $pageBaseUrl ?>config/css/style.css" rel="stylesheet" type="text/css"/>
    <?php //additional libs from param.json
    if ((isset($param['head'])) && (is_array($param['head']))) {
        foreach ($param['head'] as $url) {
            echo '<link rel="stylesheet" type="text/css" media="all" href="' . $pageBaseUrl . $url . '">';
        }
    }
    if ((isset($param['css_external'])) && (is_array($param['css_external']))) {
        foreach ($param['css_external'] as $url) {
            echo '<link rel="stylesheet" type="text/css" media="all" href="' . $url . '">';
        }
    }
    //additional footer libs from param.json
    if ((isset($param['js_external_footer'])) && (is_array($param['js_external_footer']))) {
        foreach ($param['js_external_footer'] as $src) {
            echo '<script async defer type="text/javascript" src="' . $src . '"></script>';
        }
    }
    ?>
    <!--fonts-->
    <!-- <link rel="preload" href="<?php echo $pageBaseUrl;  ?>libs/webfonts/fonts/Butler-Bold.woff2" as="font" type="font/woff2" crossorigin="anonymous"> -->
    <link rel="preload" href="<?php echo $pageBaseUrl;  ?>libs/webfonts/fonts/Butler-Medium.woff2" as="font" type="font/woff2" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400&display=swap" rel="stylesheet">
</body>
</html>