
/* global token, language, fetch, getImage, getURL, urlLanguage, translationArray, loader, baseUrl */
var home = '404'; //module name
module = 'error';
function errorLoadContent() {
    fetch(getSingletoneURL + module + token)
    .then (function(response) {
        return response.json();
    })
    .then (function (data){
        $('header').after(
            '<div class="'+module+'-container d-sm-flex flex-sm-column justify-content-center" style="background-image: url(modules/404/images/404.png)">'+
                '<div class="container py-5">'+
                    '<div class="row">'+
                        '<div class="col-lg-8 text-center">'+
                            data['Message'+ language] +
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'
        );
    });
    
    $("#PageCover").remove();
}
