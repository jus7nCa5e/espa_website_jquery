

var header = 'navigation'; //module name
var module; //every page will assign module name to define current page
var bookingID;
var listingPage = false;
projectTitle = '';
var navContainer = /*html*/ `
<header>
    <div class="bg-info">
        <div class="container d-none d-sm-flex">
            <!-- <ul class="nav" id="langContainer"></ul> -->
            <ul class="nav mx-auto" id="middleNav"></ul>
            <ul class="nav " id="loginContainer"></ul>
            <ul class="nav" id="rightNav"></ul>
        </div>
    </div>
    <nav class="navbar navbar-light bg-white py-0" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="#" id="navbarLogo"></a>
            <button class="navbar-toggler x collapsed d-flex" type="button" data-toggle="offcanvas" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <div class="mr-2 lead text-primary" id="menuBtn"></div>
                <div>
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </div>		
            </button>
            <div class="offcanvas-collapse navbar-collapse flex-column d-flex" id="navbarNavDropdown">
                <div class="container d-flex flex-column d-sm-block">
                    <ul class="navbar-nav text-right" id="navContainer"></ul>
                    <hr class="d-block d-sm-none w-100" />
                    <div class="language-switch d-block d-sm-none ml-auto"></div>
                </div>
            </div>
        </div>
    </nav>
    <div id="PageCover" class="">
        <div class="container-fluid" id="cover">
            <div class="row h-100">
                <div class="col-12 d-flex align-items-center justify-content-center" id="coverTitle">

                </div>
            </div>
        </div>
        <div class="container pt-4" id="shareContainer">
            <div class="row">
                <div class="col-12 d-flex">
                    <div class="mr-2">Share</div> <div id="sharer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
`;
$('body').append(navContainer);

$('[data-toggle="offcanvas"]').on('click', function() {
    $('.offcanvas-collapse').toggleClass('open');
    $('.navbar-toggler').toggleClass('collapsed');
})


var footerContainer = /*html*/ `
<div class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center py-4 d-flex justify-content-center">
                <ul class="footer-logos nav"></ul>
            </div>
        </div>
    </div>
</div>
<div class="bg-primary pt-5 pb-1 footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-4">
                <div id="footerAddress"></div>
                <ul class="nav pt-4" id="footerSocials"></ul>
            </div>
            <div class="col-xl-8">
                <div class="row nav" id="footerNav"></div>
            </div>
        </div>
        <div class="row pt-3">
            <div class="col-12" id="footerDown">
                <div class="footer-left text-center text-white small mt-3">
                </div>
            </div>
            
        </div>
    </div>
</div>`;
$('body').append(footerContainer);
//animate dropdown


//get navigation
function getNavigation() {
    fetch(getURL + header + token + '&simple=1')
    .then (function(response) {
        return response.json();
    })
    .then (function (data){
        $('#menuBtn').append(`${translationArray['menu' + language]}`);
        count = data.length;
        
        $( "#PageCover" ).addClass(`${module}-cover`);
        console.log(module);

        for (let value of data) {
            var Title = value['Title'+ language];
            var Alias = value.Alias_slug;
            var Cover;
            ParentAlias = "";

            var navItem, footerItem;
            if (value.Show_in_menu){
                if (value.children && (value.children.length > 0)){
                    navItem = /*html*/ `
                        <li class="nav-item module-${value.Alias_slug} dropdown-submenu">
                            <a class="nav-link dropdown" href="#" id="${value.Alias_slug}NavbarDropdown" role="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${Title}</a>
                            <ul class="dropdown-menu" aria-labelledby="${value.Alias_slug}NavbarDropdown" id="${value.Alias_slug}NavbarDropdownContainer"></ul>
                        </li>
                    `;
                    $('#navContainer').append(navItem);
                    // footerItem = /*html*/ `
                    //     <li class="nav-item">
                    //         <!-- <a class="nav-link text-white">${Title}</a> -->
                    //         <ul class="nav flex-column mt-2" id="${value.Alias_slug}FooterSubNav"></ul>
                    //     </li>
                    // `;
                    // $('#footerNav').append(footerItem);

                    for (let val of value.children) {
                        if (val.children && (val.children.length > 0)){
                            navItem = /*html*/ `
                                <li class="dropdown-submenu">
                                    <a class="dropdown-item nav-link px-3 second-level-dropdown" id="${val.Alias_slug}NavbarDropdown" href="#" role="button">${val['Title'+ language]}</a>
                                    <ul class="dropdown-menu" aria-labelledby="${val.Alias_slug}NavbarDropdown" id="${val.Alias_slug}NavbarDropdownContainer"></ul>
                                </li>
                            `;
                            $(`#${value.Alias_slug}NavbarDropdownContainer`).append(navItem);
                            // footerItem = /*html*/ `
                            //     <li class="nav-item">
                            //         <!-- <a class="nav-link text-white">${val['Title'+ language]}</a> -->
                            //         <ul class="nav flex-column mt-2" id="${val.Alias_slug}FooterSubNav"></ul>
                            //     </li>
                            // `;
                            // $(`#${value.Alias_slug}FooterSubNav`).append(footerItem);
                            for (let v of val.children) {
                                navItem = /*html*/ `
                                    <li>
                                        <a class="dropdown-item nav-link px-3 last-link" href="${urlLanguage}/${value.Alias_slug}/${val.Alias_slug}#${v.Alias_slug}">${v['Title'+ language]}</a>
                                    </li>
                                `;
                                $(`#${val.Alias_slug}NavbarDropdownContainer`).append(navItem);
                                footerItem = /*html*/ `
                                    <div class="nav-item col-md-6 col-lg-4">
                                        <a class="nav-link text-white" href="${urlLanguage}/${value.Alias_slug}/${val.Alias_slug}#${v.Alias_slug}">${v['Title'+ language]}</a>
                                    </div>
                                `;
                                // $(`#${val.Alias_slug}FooterSubNav`).append(footerItem);
                                $(`#footerNav`).append(footerItem);
                                //cover image and Page title
                                if ((v.Module === module) && (module !== 'home') && (listingPage === true)) {
                                    // console.log(v.Title);   
                                }
                            }
                        } else {
                            if (!val.Module){
                                navItem = /*html*/ `
                                    <li>
                                        <a class="dropdown-item nav-link px-3 cococ" href="${urlLanguage}/${value.Alias_slug}#${val.Alias_slug}">${val['Title'+ language]}</a>
                                    </li>
                                `;
                            } else {
                                navItem = /*html*/ `
                                    <li>
                                        <a class="dropdown-item nav-link px-3 cococ" href="${urlLanguage}/${value.Alias_slug}/${val.Alias_slug}">${val['Title'+ language]}</a>
                                    </li>
                                `;
                            }
                            $(`#${value.Alias_slug}NavbarDropdownContainer`).append(navItem);
                            footerItem = /*html*/ `
                                <div class="nav-item col-md-6 col-lg-4">
                                    <a class="nav-link text-white"  href="${urlLanguage}/${value.Alias_slug}/${val.Alias_slug}">${val['Title'+ language]}</a>
                                </div>
                            `;
                            // $(`#${value.Alias_slug}FooterSubNav`).append(footerItem);
                            $(`#footerNav`).append(footerItem);
                        }
                        //cover image and Page title
                        if ((val.Module === module) && (module !== 'home') && (listingPage === true)) {
                            if (val.Cover_image) {
                                Cover = val.Cover_image.path;
                                $('#cover').css("background-image", `url('${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Cover}&w=1920&o=1&m=thumbnail&q=80')`);
                                $('#coverTitle').append(`<h1 class="display-2 light text-white">${val['Title'+ language]}</h1>`);
                            }
                        }
                    }
                } else if (value.External) {
                    navItem = /*html*/ `
                        <li class="nav-item module-${value.Alias_slug}">
                            <a class="nav-link" href="${value.Link}" target="_blank">${Title}</a>
                        </li>
                    `;
                    $('#navContainer').append(navItem);
                    footerItem = /*html*/ `
                        <div class="nav-item col-md-6 col-lg-4 module-${value.Alias_slug}">
                            <a class="nav-link text-white" href="${value.Link}" target="_blank">${Title}</a>
                        </div>
                    `;
                    $('#footerNav').append(footerItem);
                }
                else {
                    navItem = /*html*/ `
                    <li class="nav-item module-${value.Alias_slug}">
                    <a class="nav-link" href="${urlLanguage}/${Alias}">${Title}</a>
                    </li>
                    `;
                    $('#navContainer').append(navItem);
                    footerItem = /*html*/ `
                    <div class="nav-item col-md-6 col-lg-4 module-${value.Alias_slug}">
                    <a class="nav-link text-white" href="${urlLanguage}/${Alias}">${Title}</a>
                    </div>
                    `;
                    $('#footerNav').append(footerItem);
                }
            } else if (value.Show_in_top) {
                if ((value.Registered) && (user)) {
                // if (value.Alias_slug === 'members-area') {
                    navItem = /*html*/ `
                        <li class="nav-item module-${value.Alias_slug}">
                            <a class="nav-link small text-white font-weight-normal py-1" href="${urlLanguage}/${Alias}">${Title}</a>
                        </li>
                    `;
                    $('#rightNav').append(navItem);
                } else if(!value.Registered) {
                    navItem = /*html*/ `
                        <li class="nav-item module-${value.Alias_slug}">
                            <a class="nav-link small text-white font-weight-normal py-1" href="${urlLanguage}/${Alias}">${Title}</a>
                        </li>
                    `;
                    $('#middleNav').append(navItem);
                    footerItem = /*html*/ `
                        <div class="nav-item col-md-6 col-lg-4 module-${value.Alias_slug}">
                            <a class="nav-link text-white" href="${urlLanguage}/${Alias}">${Title}</a>
                        </div>
                    `;
                    $('#footerNav').append(footerItem);
                }
            } else if (value.Show_in_footer) {
                footerItem = /*html*/ `
                    <div class="nav-item col-md-6 col-lg-4 module-${value.Alias_slug}">
                        <a class="nav-link text-white" href="${urlLanguage}/${Alias}">${Title}</a>
                    </div>
                `;
                $('#footerNav').append(footerItem);
            }
            
            //cover image and Page title on listing page
            if (((value.Module === module) && (module !== 'home') && (listingPage === true)) || ((value.Module === "congress") && (module === "ESPAcongress"))) {
                if (value.Cover_image) {
                    Cover = value.Cover_image.path;                 
                    $('#cover').css("background-image", `url('${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Cover}&w=1920&o=1&m=thumbnail&q=80')`);
                    $('#coverTitle').append(`<h1 class="display-2 light text-white">${value['Title'+ language]}</h1>`);
                }
            }       
            if (!--count) {
                $('.module-' + urlModule).addClass('active');
                $('.module-' + urlModule).closest('.dropdown').addClass('active');
            }
            
        };

        $(`.first-level-dropdown`).on('click', function(e){
            e.preventDefault();
            $(this).dropdown('show');

        })
        $(`.second-level-dropdown`).on('click', function(e){
            e.preventDefault();
            $(this).closest('.nav-item').find('.first-level-dropdown').dropdown('show');
            $(this).dropdown('show');
        })
        $('.last-link').on('click', function() {
            $('.offcanvas-collapse').toggleClass('open');
            $('.navbar-toggler').toggleClass('collapsed');
        })
        getHotelRegion();
        languageSwitch();
        navbarLogin();
    });
}
function getHotelRegion() {
    fetch(getSingletoneURL + mainData + token)
        .then (function(response) {
            return response.json();
        })
        .then (function (data){
            var navLogo = /*html*/ `<img class="navbar-logo img-fluid" src="${streamSRV}/storage/uploads${data.Logo.path}" alt="${data['Title'+language]}">`;
            $('.navbar-brand').append(navLogo);

            //footer logos
            for (let footval of data.Footer_logos) {
                let logoTemplate = /*html*/ `
                    <li class="nav-item">
                        <a class="nav-link px-0 px-sm-4" href="${footval.meta.link}" target="_blank"><img src="${streamSRV}${footval.path}" style="height: 82px;"></a>
                    </li> 
                `;
                $('.footer-logos').append(logoTemplate);
            }
            //footer address
            var contactTemplate = /*html*/ `
                <div>
                    <span class="text-white">${data['Address'+language].Label}</span>
                    <a href="${data.Google_maps}" target="_blank" class="text-white">
                        ${data['Address' + language].Data}
                    </a>
                    <div>
                        <span class="text-white">${data['Phone'+language].Label} </span>
                        <a href="tel:${data['Phone'+language].Data}" class="text-white">${data['Phone'+language].Data}</a>
                    </div>
                    <div>
                        <span class="text-white">${data['Email'+language].Label} </span>
                        <a href="mailto:${data['Email'+language].Data}" class="text-white">${data['Email'+language].Data}</a>
                    </div>
                </div>
            `;
            $('#footerAddress').append(contactTemplate);

            //populate text on contacts section
            Address = data.Address;
            Location = data.Location;
            Email = data.Email;
            Direction = data.Google_maps;
            setTimeout(function(){
                if (data.Congress_location) {
                    $('.address-container').append('<p> ' + data.Congress_location + '</p>');
                }
                if (data.Address) {
                    $('.location-container').append(data.Address.Label + data.Address.Data);
                }
                if (data.Email) {
                    $('.email-container').append('<p><a href="mailto:' + data.Email.Data + '" target="_blank" class="text-primary"> ' + data.Email.Data + '</a></p>');
                }
            }, 700);
            
            // //socials
            // projectTitle = data.Title;
            // setTimeout(function(){
            //     $.each(data.Social, function(index, value){
            //         Icon = value.value.Name;
            //         IconLink = value.value.Link;
            //         footerSocialIcon = /*html*/ `
            //             <a href="${IconLink}" class="btn btn-link p-2 mr-2" target="_blank" rel="noopener">
            //                 <i class="fab fa-${Icon}"></i>
            //             </a>
            //         `;
            //         $('#footerSocials').append(footerSocialIcon);
            //     });
            // }, 700);

            //socials
            for (let value of data.Social) {
                // console.log(value);
                Icon = value.value.Icon;
                IconTitle = value.value.Name;
                IconLink = value.value.Link;
                footerSocialIcon = /*html*/ `
                    <li class="nav-item">
                        <a href="${IconLink}" class="nav-link pr-2 pl-0 bg-transparent text-white" aria-label="${IconTitle}" target="_blank" rel="noopener">
                            <span class="fa-stack">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-${Icon} fa-fw fa-stack-1x fa-inverse text-primary"></i>
                            </span>
                        </a>
                    </li>
                `;
                $('#footerSocials').append(footerSocialIcon);
            }    

            projectTitle = data.Title;
            

            var year = new Date().getFullYear();
            var footerCopyrightContent = /*html*/ `
                © ${year} ${data['Footer_text'+language]} ${data['Title'+language]}
                <small class="text-light pl-2 small">Powered by 
                    <a href="https://www.clientric.bg/tools/waw-digital-identity" target="_blank" class="text-light" rel="noopener">WAW</a> | 
                    <a href="https://www.clientric.bg/" target="_blank" class="text-light" rel="noopener">Clientric</a>
                </small>
            `;
            $('#footerDown .footer-left').append(footerCopyrightContent);

             //Cookie consent
             window.cookieconsent.initialise({
                elements: {
                    header: '<span class="cc-header">{{header}}</span>&nbsp;',
                    message: '<span id="cookieconsent:desc" class="cc-message">{{message}}</span>',
                    messagelink: '<span id="cookieconsent:desc" class="cc-message">{{message}} <a aria-label="learn more about cookies" tabindex="0" class="cc-link text-primary" href="{{href}}" target="_blank">{{link}}</a></span>',
                    dismiss: '<a aria-label="dismiss cookie message" tabindex="0" class="cc-btn cc-dismiss btn btn-secondary btn-sm float-right">{{dismiss}}</a>',
                    allow: '<a aria-label="allow cookies" tabindex="0" class="cc-btn cc-allow">{{allow}}</a>',
                    deny: '<a aria-label="deny cookies" tabindex="0" class="cc-btn cc-deny">{{deny}}</a>',
                    link: '<a aria-label="learn more about cookies" tabindex="0" class="cc-link" href="{{href}}" target="_blank">{{link}}</a>',
                    close: '<span aria-label="dismiss cookie message" tabindex="0" class="cc-close">{{close}}</span>',
                },
                content: {
                    "message": data['Cookie_consent_text' + language],
                    "dismiss": data['Cookie_consent_button' + language],
                    "link": data['Cookie_consent_link_text' + language],
                    "href": data['Cookie_consent_link' + language]
                }
            });

            //sharer
            $('#sharer').append(
                `<a href="javascript:facebookshare();" class="mr-1 text-decoration-none" title="Facebook">
                    <span class="fa-stack small" style="color: #4267B2;">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fab fa-facebook-f fa-fw fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <a href="#" class="twitter-share mr-1 text-decoration-none" title="Twitter">
                    <span class="fa-stack small" style="color: #1DA1F2;">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fab fa-twitter fa-fw fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <a href="#" class="linkedin-share mr-1 text-decoration-none" title="LinkedIn">
                    <span class="fa-stack small" style="color: #0077B5;">
                        <i class="fas fa-circle fa-stack-2x"></i>
                        <i class="fab fa-linkedin-in fa-fw fa-stack-1x fa-inverse"></i>
                    </span>
                </a>`
            );
            facebookSharer();
            twitterSharer();
            linkedinSharer();

        });
}


function languageSwitch() {
    setTimeout(function () { // let's wait 200 ms. hope it's enough to send the request and receive and read the response
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
        
        showLanguage = '';
        if (getLanguage === 'default') {
            showLanguage = 'en';
        } else {
            showLanguage = 'de';
        }
        var langContent = /*html*/ `
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle d-none d-sm-block text-uppercase text-white small font-weight-normal py-1" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${showLanguage}
                </a>
                <div class="dropdown-menu language-switch" aria-labelledby="navbarDropdown" id="navbarDropdownMenu"></div>
            </li>
            
        `;
        $('#langContainer').append(langContent);
        
        var activeLanguageTemplate = /*html*/ `
            <a href="${baseUrl}${defaultLanguage}${window.location.href.substr(window.location.origin.length + relPath.length + 2)}" data-lang="${defaultLanguage}" class="dropdown-item d-inline d-sm-block text-uppercase small">
                ${showLanguage}
            </a>
        `;
        $('.language-switch').append(activeLanguageTemplate);

        for (let value of langArray) {
            var otherLanguageTemplates = /*html*/ `
                <a href="${baseUrl}${value}${window.location.href.substr(window.location.origin.length + relPath.length + 2)}" data-lang="${value}" class="dropdown-item d-inline d-sm-block text-uppercase small">
                    ${value}
                </a>
            `;
            $('.language-switch').append(otherLanguageTemplates);
        }
        $('#navbarLogo').attr('href', baseUrl+urlLanguage);
       
    }, 200);
    $('.lang-btn').on('click', function () {
        var lang = $(this).data('lang');
        if (lang) {
            setLanguageFunc(lang);
            location.reload();
        }
    });
}
//shrink nav
$(document).ready(function(){
    var scrollTop = 0;
    $(window).scroll(function(){
      scrollTop = $(window).scrollTop();
      if (scrollTop >= 113) {
        $('nav').addClass('scrolled-nav');
      } else if (scrollTop < 113) {
        $('nav').removeClass('scrolled-nav');
      } 
      
    }); 
    
  });

  function navbarLogin() {
    //    console.log(user);
        if (!user) {
            var loginTemplate = /*html*/ `
            
                <li class="dropdown nav-item">
                    <a href="#" id="dropdownMenu1" data-toggle="dropdown" class="nav-link small text-white font-weight-normal py-1 dropdown-toggle"><i class="far fa-users"></i> ${translationArray['membersarea'+language]} <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right shadow-sm">
                        <li class="px-4 pt-3">
                            <form class="form" role="form" id="loginForm">
                                <div class="form-group">
                                    <input id="userInput" name="user" placeholder="${translationArray['user'+language]}" class="form-control form-control-sm" type="text" required=""  autocomplete="username">
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input id="passwordInput" name="password" placeholder="${translationArray['password'+language]}" class="form-control form-control-sm border-right-0" type="password" required=""  autocomplete="current-password">
                                        <div class="input-group-append">
                                            <span class="form-control form-control-sm py-2 border-left-0 toggle-password" toggle="#passwordInput"><i class="far fa-eye-slash"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">${translationArray['login'+language]}</button>
                                </div>
                                <div class="form-group text-center">
                                    <small><a href="${streamSRV}/auth/forgotpassword" target="_blank">${translationArray['forgot_password'+language]}</a></small>
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
            `;
            $('#loginContainer').append(loginTemplate);    
            $('#loginForm').submit(function(e) {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: baseUrl + 'tmpl/users/login.php',
                    data: $('#loginForm').serialize(),
                    success: function(data) {
                        if(data === 'success'){
                            location.reload();
                        } else if(data === 'Authentication failed'){
                            $('#passwordInput').popover({
                                content: ''+translationArray['wrong_password' + language]+'',
                                template: 	'<div class="popover border-danger" role="tooltip"><div class="arrow"></div><div class="popover-body text-danger"></div></div>'
                            });
                            $('#passwordInput').popover('show');
                            $('#passwordInput').addClass('is-invalid');
                            setTimeout(function () {
                                $('#passwordInput').popover('hide');
                                $('#passwordInput').popover('dispose');
                                $('#passwordInput').removeClass('is-invalid');
                            },2000);
                            $( "#passwordInput" ).focus(function() {
                                $('#passwordInput').popover('hide');
                                $('#passwordInput').popover('dispose');
                                $('#passwordInput').removeClass('is-invalid');
                            });
                        } else if(data === 'Wrong username'){
                            $('#userInput').popover({
                                content: ''+translationArray['wrong_user' + language]+'',
                                template: 	'<div class="popover border-danger" role="tooltip"><div class="arrow"></div><div class="popover-body text-danger"></div></div>'
                            });
                            $('#userInput').popover('show');
                            $('#userInput').addClass('is-invalid');
                            setTimeout(function () {
                                $('#userInput').popover('hide');
                                $('#userInput').popover('dispose');
                                $('#userInput').removeClass('is-invalid');
                            },2000);
                            $( "#userInput" ).focus(function() {
                                $('#userInput').popover('hide');
                                $('#userInput').popover('dispose');
                                $('#userInput').removeClass('is-invalid');
                            });
                        } else if(data === 'Missing user or password'){
                            alert("Missing user or password");
                        }
                    }
                });
            });
        } else {
            var loggedinTemplate = /*html*/ `
                <li class="dropdown nav-item">
                    <a href="#" id="dropdownMenu1" data-toggle="dropdown" class="nav-link small text-white font-weight-normal py-1 dropdown-toggle"><i class="far fa-user"></i> ${user} <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right shadow-sm">
                        <li class="px-3 py-2">
                            <button type="button" class="btn btn-link btn-block" id="changePasswordBtn" data-toggle="modal" data-target="#passwordModal">${translationArray['password_change'+language]}</button>
                            <button type="button" class="btn btn-danger btn-block" id="logout">${translationArray['logout'+language]}</button>
                        </li>
                    </ul>
                </li>
            `;
           $('#loginContainer').append(loggedinTemplate);
            $('#logout').on('click', function(e) {
                $.ajax({
                    type: 'post',
                    url:  baseUrl + 'tmpl/users/logout.php',
                    success: function () {
                      location.reload();
                    }
                });
            });
            //change password modal
            var changePassModal = /*html*/ `
                <div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form id="changePasswordForm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input id="passwordInputOld" name="password" placeholder="${translationArray['password_old'+language]}" class="form-control form-control-sm border-right-0" type="password" required=""  autocomplete="current-password">
                                            <div class="input-group-append">
                                                <span class="form-control form-control-sm py-2 border-left-0 toggle-password" toggle="#passwordInputOld"><i class="far fa-eye-slash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input id="passwordInputNew" name="passwordNew" placeholder="${translationArray['password_new'+language]}" class="form-control form-control-sm border-right-0" type="password" required=""  autocomplete="new-password">
                                            <div class="input-group-append">
                                                <span class="form-control form-control-sm py-2 border-left-0 toggle-password" toggle="#passwordInputNew"><i class="far fa-eye-slash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input id="passwordInputRenew" name="passwordRenew" placeholder="${translationArray['password_retype'+language]}" class="form-control form-control-sm border-right-0" type="password" required=""  autocomplete="new-password"> 
                                            <div class="input-group-append">
                                                <span class="form-control form-control-sm py-2 border-left-0 toggle-password" toggle="#passwordInputRenew"><i class="far fa-eye-slash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">${translationArray['cancel'+language]}</button>
                                    <button type="submit" class="btn btn-primary btn-block">${translationArray['password_change'+language]}</button>
                                </div>
                            </div>
                            <input type="hidden" name="user" value="${user}" autocomplete="username">
                        </form>
                    </div>
                </div>
            `;
            $('body').append(changePassModal);
            var changePassSuccess = /*html*/ `
                <div class="modal fade" id="passwordModalSuccess" tabindex="-1" role="dialog" aria-labelledby="passwordModalSuccessLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content p-5">
                            <div class="modal-body p-5">
                                <h4 class="text-center">${translationArray['password_change_success'+language]}</h4>
                                <div class="progress">
                                    <div id="progressbar" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="10" style="width: 100%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;
            $('body').append(changePassSuccess);
            //validate form
            $.validator.setDefaults({
                submitHandler: function () {
                    $.ajax({
                        type: 'post',
                        url:  baseUrl + 'tmpl/users/editprofile.php',
                        data: $('#changePasswordForm').serialize(),
                        success: function(data) {
                            if(data === 'success'){
                                $('#passwordModalSuccess').modal('show');
                                
                                var current_progress = 0;
                                var interval = setInterval(function() {
                                    current_progress += 1;
                                    $("#progressbar")
                                    .css("width", current_progress + "%")
                                    .attr("aria-valuenow", current_progress);
                                    if (current_progress >= 100)
                                        clearInterval(interval);
    
                                }, 50);
                                setTimeout(function(){ location.reload(); }, 4000);
                            } else if(data === 'Authentication failed'){
                                $('#passwordInputOld').popover({
                                    content: 'Wrong password! Please provide a valid password!',
                                    template: 	'<div class="popover border-danger" role="tooltip"><div class="arrow"></div><div class="popover-body text-danger"></div></div>'
                                });
                                $('#passwordInputOld').popover('show');
                                $('#passwordInputOld').addClass('is-invalid');
                                setTimeout(function () {
                                    $('#passwordInputOld').popover('hide');
                                    $('#passwordInputOld').popover('dispose');
                                    $('#passwordInputOld').removeClass('is-invalid');
                                },2000);
                                $( "#passwordInputOld" ).focus(function() {
                                    $('#passwordInputOld').popover('hide');
                                    $('#passwordInputOld').popover('dispose');
                                    $('#passwordInputOld').removeClass('is-invalid');
                                });
                            } else if(data === 'Wrong username'){
                                alert("Wrong username");
                            } else if(data === 'Missing user or password'){
                                alert("Missing user or password");
                            }
                        }
                    });
    //                console.log('valid');
                }
            });
            $('#changePasswordForm').validate({
                rules: {
                    password: "required",
                    passwordNew: "required",
                    passwordRenew: {
                        equalTo: "#passwordInputNew",
                        required: true
                    }
                    
                },
                messages: {
                    password: translationArray['required' + language],
                    passwordNew: translationArray['required' + language],
                    passwordRenew: {
                        equalTo: translationArray['equal' + language],
                        required: translationArray['required' + language]
                    }
                },
                errorElement: "div",
                errorPlacement: function (error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element.parent('.input-group'));
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass("is-invalid");
                }
            });
        }
        
        //toggle for visible password
        $(".toggle-password").click(function() {
            $(this).find('svg').toggleClass("fa-eye-slash fa-eye");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") === "password") {
              input.attr("type", "text");
            } else {
              input.attr("type", "password");
            }
        });
    }