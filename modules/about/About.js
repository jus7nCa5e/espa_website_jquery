//-------------------- Start fixing code ------------------
// This code fix bug in some version in Chrome browser when we use # to jump to some id in the page
$(document).ready(function () {
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	if (window.location.hash && isChrome) {
		setTimeout(function () {
			var hash = window.location.hash;
			window.location.hash = "";
			window.location.hash = hash;
		}, 300);
	}
});
//--------------------  end fixing code -------------------

var myLatLng;
var locationLat, locationLng;
function callMap() {
    setTimeout(function () {
        initMap();
    }, 500);
}
function aboutLoadContent() {
	listingPage = true;
	module = "about";
	var mainTemplate = /*html*/ `
		<div id="introduction" >
			<div class="container pt-5">
				<div class="row py-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_introduction' + language]}</span></h2>
					</div>
				</div>
				<div class="row">
					<div class="col-12" id="introductionBody"></div>
				</div>
			</div>
		</div>
		<div id="whoweare">
			<div class="container">
				<div class="row py-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_whoweare' + language]}</span></h2>
					</div>
				</div>
				<div class="row" id="whoweareBody"></div>
				<div id="whoweareColumns" class="card-columns"></div>
			</div>
		</div>
		<div id="benefitsformembers">
			<div class="container">
				<div class="row py-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_benefitsformembers' + language]}</span></h2>
					</div>
				</div>
				<div id="benefitsformembersColumns" class="card-columns"></div>
			</div>
		</div>
		<div id="partners">
			<div class="container">
				<div class="row py-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_partners' + language]}</span></h2>
					</div>
				</div>
				<div class="row" id="partnersBody"></div>
			</div>
		</div>
		<div id="qualitycriteria" >
			<div class="container pt-5">
				<div class="row pt-5">
					<div class="col-12" id="qualitycriteriaBody" ></div>
				</div>
			</div>
		</div>
		<div id="contacts" >
			<div class="container pt-5 pb-0 pb-sm-3 pb-md-4 pb-lg-5">
				<div class="row py-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_contacts' + language]}</span></h2>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="embed-responsive embed-responsive-21by9">
							<div class="embed-responsive-item" id="map"></div>
						</div>
					</div>
				</div>
				<div class="row py-5" id="contactsBody"></div>
			</div>
		</div>
    `;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + 'aboutus' + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			//#region Introduction
			let IntroductionText = data['Introduction_text' + language];
			$('#introductionBody').append(IntroductionText);
			//#endregion Introduction
			
			//#region Who We Are
			let WhoweareText = data['Whoweare_text' + language];
			
			let whoweareTemplate = /*html*/ `
				<div class="col-12">
					${WhoweareText}
				</div>
			`;
			$('#whoweareBody').append(whoweareTemplate);
			if (data.Whoweare_data) {
				var whoweareData = data['Whoweare_data' + language];
				
				for (let value of whoweareData) {
					let Image = value.value.Image.path;
					let item = /*html*/ `
						<div class="card mb-3 border-0" >
							<div class="row no-gutters">
								<div class="col-4 col-lg-2 col-xl-1 pt-3">
									<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=64&h=64&o=1&m=thumbnail&q=80" class="card-img img-fluid" alt="${value.value.Title}">
								</div>
								<div class="col-8 col-lg-10 col-xl-11">
									<div class="card-body">
										<h4>${value.value.Title}</h4>
										${value.value.Text}
									</div>
								</div>
							</div>
						</div>
					`;
					$('#whoweareColumns').append(item);
				}
			}
			//#endregion Who We Are

			//#region Benefits for members
			
			if (data.Benefits_data) {
				let benefitsData = data['Benefits_data' + language];
				
				for (let value of benefitsData) {
					let Image = value.value.Image.path;
					let item = /*html*/ `
						<div class="card mb-3 border-0" >
							<div class="row no-gutters">
								<div class="col-4 col-lg-2 col-xl-1 pt-3">
									<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=64&h=64&o=1&m=thumbnail&q=80" class="card-img img-fluid" alt="${value.value.Title}">
								</div>
								<div class="col-8 col-lg-10 col-xl-11">
									<div class="card-body">
										${value.value.Text}
									</div>
								</div>
							</div>
						</div>
					`;
					$('#benefitsformembersColumns').append(item);
				}
			}
			//#endregion Benefits for membersS
			
			//#region Partners
			
			if (data.Partners_data) {
				var partnersData = data.Partners_data;
				
				for (let value of partnersData) {
					let Image = value.value.Image.path;
					let item = /*html*/ `
						<div class="col-lg-3 col-md-4 col-sm-6 col-12 text-center pt-3 pt-sm-4 pt-lg-5">
							<a class="text-dark text-decoration-none" href="${value.value.Link}">
								<img src="${streamSRV}/storage/uploads${Image}" class="img-fluid" style="height: 96px;" alt="${value.value.Title}">
								<h5 class="pt-3 pt-sm-4 pt-lg-5">${value.value.Title}</h5>
							</a>
						</div>
					`;
					$('#partnersBody').append(item);
				}
			}
			//#endregion Partners

			//#region QualityCriteria
			
			var qualityData = data['Quality_data'+language];

			if(qualityData) {
				let Image = qualityData.Image.path;
				let button  = /*html*/ `<a href="${streamSRV}/storage/uploads${qualityData.File.path}" class="btn btn-secondary">${qualityData.Button_text}</a> `;
				let item = /*html*/ `
					<div class="card bg-dark text-white border-0">
						<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=1338&h=400&o=1&m=thumbnail&q=80" class="card-img" alt="${qualityData.Title}">
						<div class="card-img-overlay p-0 p-sm-3 p-lg-5">
							<h5>${qualityData.Title}</h5>
							${qualityData.Text}
							${button}
						</div>
					</div>
				`;
				$('#qualitycriteriaBody').append(item);
			}
			//#endregion QualityCriteria

			//#region Contacts
			
			fetch(getSingletoneURL + 'mainbig' + token)
				.then(function (responseMain) {
					return responseMain.json();
				})
				.then(function (dataMain) {
					locationLat = parseFloat(dataMain.Location.lat);
					locationLng = parseFloat(dataMain.Location.lng);

					var contactTemplate = /*html*/ `
						<div class="col-lg-4">
							${dataMain['Address'+language].Label}
							<a href="${dataMain.Google_maps}" target="_blank">
								${dataMain['Address' + language].Data}
							</a>
							<div>
								<span>${dataMain['Phone'+language].Label} </span>
								<a href="tel:${dataMain['Phone'+language].Data}">${dataMain['Phone'+language].Data}</a>
							</div>
							<div>
								<span>${dataMain['Email'+language].Label} </span>
								<a href="mailto:${dataMain['Email'+language].Data}">${dataMain['Email'+language].Data}</a>
							</div>
						</div>
						<div class="col-lg-8">
							${data.Contacts_text}
						</div>
					`;
					$('#contactsBody').append(contactTemplate);
				})
				.then(function(){
					myLatLng = {lat: locationLat, lng: locationLng};
				});

			//#endregion Contacts


		});
}

//init map
function initMap() {
        
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        zoom: 18
    });
    // Create a marker and set its position.
    // var logoPin = mapPin;
    var marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
        animation: google.maps.Animation.DROP,
        // icon: logoPin
    });
    var styles = {
        default: null,
        hide: [
            {
                featureType: 'poi.business',
                stylers: [{visibility: 'off'}]
            },
            {
                featureType: 'transit',
                elementType: 'labels.icon',
                stylers: [{visibility: 'off'}]
            }
        ]
    };
    map.setOptions({styles: styles['default']});
}