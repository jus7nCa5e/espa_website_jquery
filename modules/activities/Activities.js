
function activitiesLoadContent() {
	listingPage = true;
	module = "activities";
	var mainTemplate = /*html*/ `
		<div id="euprogrammes" >
			<div class="container">
				<div class="row pt-5 pb-4 pb-sm-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_euprogrammes' + language]}</span></h2>
					</div>
				</div>
				<div class="row" id="euprogrammesBody"></div>
			</div>
			<div class="card-group pt-3 pt-sm-4 pt-md-5" id="euprogrammesPanels"></div>
		</div>
    `;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + 'activities' + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {

			//#region EUprogrammes
			let euprogrammesText = data['Euprogrammes_text' + language];
			
			let euprogrammesTemplate = /*html*/ `
				<div class="col-12">
					${euprogrammesText}
				</div>
			`;
			$('#euprogrammesBody').append(euprogrammesTemplate);
			if (data.Euprogrammes_data) {
				var euprogrammesData = data['Euprogrammes_data' + language];
				
				for (let value of euprogrammesData) {
					let Image = value.value.Image.path;
					let button;
					if (value.value.Button_link) {
						button = /*html*/ `<a href="${streamSRV}/storage/uploads${value.value.Button_link}" class="btn btn-secondary">${value.value.Button_text}</a> `;
					}
					let item = /*html*/ `
						<div class="card bg-dark text-white border-0">
							<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&h=400&o=1&m=thumbnail&q=80" class="card-img" alt="${value.value.Title}">
							<div class="card-img-overlay p-5">
								<h4>${value.value.Title}</h4>
								${value.value.Text}
								${button}
							</div>
						</div>
					`;
					$('#euprogrammesPanels').append(item);
				}
			}
			//#endregion EUprogrammes
		});
}
