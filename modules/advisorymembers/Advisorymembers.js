module = 'advisorymembers';

//inner page
function advisorymembersInnerPage() {
    var pageTmplate = /*html*/ `
        <div id="introduction" >
			<div class="container py-5">
				<div class="row">
					<div class="col-12" id="introductionBody"></div>
				</div>
			</div>
        </div>
    `;
    $('header').after(pageTmplate);
    //get location from URL (explode url)
    var url = window.location.pathname;
    var pageParam = decodeURI(url.substr(url.lastIndexOf('/') + 1));
    
    fetch(getURL + module + token + '&simple=1&populate=1&filter[Alias_slug]='+pageParam)
    .then (function(response) {
        return response.json();
    })
    .then (function (data){
        // console.log(data);
        value = data[0];

       
        //populate cover image 
        if (value.Cover_image) {
            let Cover = value.Cover_image.path;
            setTimeout(function(){
                $('#cover').css("background-image", `url('${streamSRV}/storage/uploads${Cover}')`);
                $('#coverTitle').append(`<h1 class="display-2 light text-white">${value['Title'+ language]}</h1>`);
            }, 500);
        } else {
            setTimeout(function(){
                $('#cover').addClass("bg-info");
                $('#coverTitle').append(`<h1 class="display-2 light text-white">${value['Title'+ language]}</h1>`);
            }, 500);
        }

        $('#introductionBody').append(value['Description' + language]);

        // //asscoiations
        // if (value.Associations) {
        //     for (let val of value.Associations) {
        //         var associationTemplate = /*html*/ `
        //             <div class=" col-xl col-lg-4 col-md-3 col-sm-6 col-12 text-center">
        //                 <a href="${val.value.Link}" target="_blank">
        //                     <img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${val.value.Image.path}&h=160&o=1&m=thumbnail&q=80" class="img-fluid" alt="${val.value.Title}">
        //                     <h4 class="pt-3">${val.value.Title}</h4>
        //                 </a>
        //             </div>
        //         `;
        //         $('#associationsBody').append(associationTemplate);
        //     }
        // }
        // //Indications data
        // if (value.Indications_data) {
        //     var indicationsData = value['Indications_data' + language];
            
        //     for (let val of indicationsData) {
        //         let Image = val.value.Image.path;
        //         let item = /*html*/ `
        //             <div class="col-lg-6 col-12">
        //                 <div class="card mb-3 border-0" >
        //                     <div class="row no-gutters">
        //                         <div class="col-4 col-lg-2 col-xl-1 pt-3">
        //                             <img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=64&h=64&o=1&m=thumbnail&q=80" class="card-img img-fluid" alt="${val.value.Title}">
        //                         </div>
        //                         <div class="col-8 col-lg-10 col-xl-11">
        //                             <div class="card-body">
        //                                 <h4>${val.value.Title}</h4>
        //                                 ${val.value.Text}
        //                             </div>
        //                         </div>
        //                     </div>
        //                 </div>
        //             </div>
        //         `;
        //         $('#indicationsColumns').append(item);
        //     }
        // }

        // //Main Indications section
        // if(value.Destinations.length > 0) { 
        //     fetch(getURL + 'indications' + token + '&simple=1&populate=1')
        //     .then (function(responseInd) {
        //         return responseInd.json();
        //     })
        //     .then (function (dataInd){
        //         for (let valInd of dataInd) {
        //             let item = /*html*/ `
        //                 <div class="col-lg-6 col-12 d-none" id="item${valInd.Alias_slug}">
        //                     <h4>${valInd['Title' + language]}</h4>
        //                     <ul class="list-inline" id="${valInd.Alias_slug}"></ul>
        //                 </div>
        //             `;
        //             $('#indicationsAllColumns').append(item);
        //         }
        //     }).then(function(){
        //         for (let [index, hotels] of value.Destinations.entries()) {
        //             //populate indications
        //             for (let indic of hotels.Indication) {
        //                 $(`#item${indic.display}`).removeClass('d-none');
        //                 $(`#${indic.display}`).append(`<li class="list-inline-item"><a href="${hotels.Link}" class="small" target="_blank" >${hotels.Title}</a></li>`);
        //             }

        //             let Address = hotels.Address ? `<li class=""><a href="${hotels.Google_maps}" target="_blank"><i class="far fa-map-marker-alt"></i> &nbsp;${hotels.Address}</a></li>` : '';
        //             let Phone = hotels.Phone ? `<li class=""><a href="tel:${hotels.Phone}" target="_blank"><i class="far fa-phone"></i> &nbsp;${hotels.Phone}</a></li>` : '';
        //             let Email = hotels.Email ? `<li class=""><a href="mailto:${hotels.Email}" target="_blank"><i class="far fa-envelope"></i> &nbsp;${hotels.Email}</a></li>` : '';

        //             //populate hotels as destinations
        //             var hotelTemplate = /*html*/ `
        //                 <div class="card pb-3">
        //                     <img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${hotels.Image.path}&w=512&h=384&o=1&m=thumbnail&q=80" class="card-img-top" alt="${hotels.Title}">
        //                     <div class="card-body">
        //                         <h5 class="card-title serif-font mb-0">${hotels.Title}</h5>
        //                         <div class="category-index-${index} pb-3"></div>
        //                         <ul class="list-unstyled">
        //                             ${Address}
        //                             ${Phone}
        //                             ${Email}
        //                         </ul>
        //                         <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
        //                     </div>
        //                     <div class="card-footer bg-transparent border-0 pt-0">
        //                         <div class="text-right"><a href="${hotels.Link}" class="btn btn-primary" target="_blank">${translationArray['view_hotel' + language]}</a></div>
        //                     </div>
        //                 </div>
        //             `;
        //             $('#destinationsAllColumns').append(hotelTemplate);
        //             // console.log(hotels)
        //             for (var i = 0; i < hotels.Category; i++) {
        //                 $(`.category-index-${index}`).append('<i class="far fa-star fa-sm text-muted"></i>');
        //             }
        //         }
        //     });
        // } else {
        //     $('#indicationsAllColumns').append('<h2 class="text-center w-100"><em>Information coming soon!</em></h2>');
        //     $('#destinationsAllColumns').append('<h2 class="text-center w-100"><em>Information coming soon!</em></h2>');
        // }
        
    });
}
