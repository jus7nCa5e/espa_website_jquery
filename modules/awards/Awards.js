/* global token, language, fetch, getImage, getURL, urlLanguage, translationArray, loader, baseUrl, mySwiper */

videos = [];
var module = 'awards'; //module name
listingPage = true;

//#region template

var pageTeplate = /*html*/`
	</div>
	<div class="container" id="overview">
		<div class="row pt-5">
			<div class="col-lg-10 offset-lg-1 col-12 offset-0 pt-5 text-center" id="${module}overview"></div>
		</div>
	</div>
	<div class="inline-container" id="categories">
		<div class="container">
			<div class="row py-5 px-0 px-sm-3">
				<div class="col-12 px-0 px-sm-3 pt-5 pb-0 pb-sm-5 text-center" id="${module}categories"></div>
			</div>
		</div>
	</div>
	<div class="inline-container" id="evaluation">
		<div class="row py-5 px-3">
			<div class="col-12 offset-0 py-5 text-center" id="${module}evaluation"></div>
		</div>
	</div>	
	<div class="inline-container" id="enter">
		<div class="container">
			<div class="row py-5 px-3">
				<div class="col-12 offset-0 py-5 text-center" id="${module}enter"></div>
			</div>
		</div>
	</div>
	<div class="inline-container" id="fees">
		<div class="container">
			<div class="row py-5 px-0 px-sm-3">
				<div class="col-12 pt-5 pb-0 pb-sm-5 px-0 text-center" id="${module}fees"></div>
			</div>
		</div>
	</div>
	<div class="inline-container" id="application">
		<div class="container">
			<div class="row py-5 px-3">
				<div class="col-12 offset-0 pt-5 text-center" id="${module}application"></div>
			</div>
		</div>
	</div>
	<div class="section" id="${module}WinnerBg">
		<div class="container pb-5" id="winner">
			<div class="row pt-5">
				<div class="col-12 pt-5 text-center" id="${module}winner"></div>
			</div>
		</div>
	</div>
	<div class="container" id="lastwinners">
		<div class="row py-5 px-3">
			<div class="col-12 offset-0 py-5 text-center" id="${module}lastwinners"></div>
		</div>
	</div>
	<div class="container" id="sponsors">
		<div class="row pt-5">
			<div class="col-12 pt-5 text-center" id="${module}sponsorsHeading"></div>
		</div>
		<div class="row justify-content-sm-center align-items-center" id="${module}sponsors"></div>	
	</div>
	<div class="inline-container" id="packages">
		<div class="container">
			<div class="row pt-5">
				<div class="col-12 pt-5 text-center" id="${module}packagesHeading"></div>
			</div>
			<div class="row justify-content-sm-center align-items-center" id="${module}packages"></div>	
		</div>
	</div>
	<div class="container" id="partners">
		<div class="row pt-5">
			<div class="offset-md-2 col-md-8 pt-5 text-center" id="${module}partnersHeading"></div>
		</div>
		<div class="row justify-content-sm-center align-items-center" id="${module}partners"></div>	
	</div>
	<div class="inline-container" id="${module}ContactsBg"">
		<div class="row pt-5">
			<div class="col-12 pt-5 text-center" id="${module}contactsHeading"></div>
		</div>
		<div class="row py-5">
			<div class="col-xl-6 offset-xl-3 col-lg-10 offset-lg-1 col-12 offset-0 text-center" id="${module}contactsForm"></div>
			<ul class="col-12 justify-content-center pt-3" id="footerSocials"></ul>
		</div>
	</div>
	<div class="container" id="contacts">
		<div class="row py-5" id="${module}contactsAddress"></div>
		<div class="row pt-5"><div class="col-12" id="footerDown"></div></div>
	</div>
`;
$('header').after(pageTeplate);

 //#endregion

function awardsLoadContent() {
    fetch(getSingletoneURL + module + token)
    .then (function(response) {
        return response.json();
    })
    .then (function (data){
		//#region Intro
		var IntroTop = data.Intro_top;
		var IntroHeading = data.Intro_heading;
		var IntroBottom = data.Intro_bottom;
		var IntroLogo = data.Intro_logo.path;
		var IntroLogoAlt = data.Intro_logo.meta.title;

		var introContent = /*html*/`
			<div class="container my-auto text-left">
				<div class="row">
					<div class="col-lg-8 col-12 offset-0 pr-0">
						<div class="cDisplay-4 bg-white d-block d-md-inline pl-3">${IntroTop}</div>
						<div class="cDisplay-2 bg-white my-3 text-primary pl-3">${IntroHeading}</div>
						<div class="bg-special text-white font-weight-bold d-block d-sm-inline py-1 px-3">${IntroBottom}</div>
					</div>
					<div class="col-lg-4 text-center">
						<img id="AwardsLogo" src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/${IntroLogo}&h=400&o=1&m=thumbnail&q=80" alt=${IntroLogoAlt}>
					</div>
				</div>
			</div>
		`;
		$(`#cover`).append(introContent);
		//#endregion

		//#region Overview
		var OverviewTop = data.Overview_top;
		var OverviewHeading = data.Overview_heading;
		var OverviewBody = data.Overview_body;

		var OverviewContent = /*html*/`
			<div class="cDisplay-4 text-primary border-top border-bottom d-block d-md-inline py-2">${OverviewTop}</div>
			<div class="cDisplay-1 mt-3 mb-5">${OverviewHeading}</div>
			<div class="">${OverviewBody}</div>
		`;
		$(`#${module}overview`).append(OverviewContent);
		//#endregion

		//#region Categories
		var CategoriesHeading = data.Categories_heading;
		var CategoriesBody = data.Categories_body;

		var CategoriesContent = /*html*/`
			<div class="cDisplay-1 mt-3 mb-5 text-primary">${CategoriesHeading}</div>
			<div class="awards-tables">${CategoriesBody}</div>
		`;
		$(`#${module}categories`).append(CategoriesContent);
		//#endregion

		//#region Evaluation
		var EvaluationHeading = data.Evaluation_heading;
		var EvaluationSubtitle= data.Evaluation_subtitle;
		var EvaluationBody = data.Evaluation_body;

		var EvaluationContent = /*html*/`
			<div class="cDisplay-1 mt-3 mb-5 text-white">${EvaluationHeading}</div>
			<div class="text-white">${EvaluationSubtitle}</div>
			<div class="text-white">${EvaluationBody}</div>
		`;
		$(`#${module}evaluation`).append(EvaluationContent);
		//#endregion

		//#region Enter
		var EnterHeading = data.Enter_heading;
		var EnterSubtitle= data.Enter_subtitle;

		var EnterContent = /*html*/`
			<div class="cDisplay-1 mt-3 mb-5 text-white font-weight-lighter">${EnterHeading}</div>
			<div class="text-white offset-1 col-10 pb-5">${EnterSubtitle}</div>
	
		`;
		$(`#${module}enter`).append(EnterContent);

		$.each(data.Enter_btns, function (index, value) {
			var EnterBtnTxt = value.value.item_btn_text;
			var EnterBtnLink = value.value.item_btn_link;

			EnterBtns = /*html*/`
				<div class="">
					<a class="btn btn-info position-relative text-white mb-4 pr-5 text-uppercase" href="${EnterBtnLink}" target="_blank">${EnterBtnTxt} <i class="fal fa-arrow-circle-right position-absolute"></i></a>
				</div>
			`;
			$(`#${module}enter`).append(EnterBtns);
		});
		//#endregion

		//#region Entry fees
		var FeesHeading = data.Entry_fees_heading;
		var FeesSubtitle= data.Entry_fees_body;


		var FeesContent = /*html*/`
			<div class="cDisplay-1 mt-3 mb-5 text-primary font-weight-lighter">${FeesHeading}</div>
			<div class="text-white offset-0 offset-md-1 m-0 col-12 col-md-10 pb-0 pb-md-5 px-0 px-sm-3 awards-tables">${FeesSubtitle}</div>
		`;
		$(`#${module}fees`).append(FeesContent);
		//#endregion

		//#region Application form
		var ApplicationTop = data.Application_top;
		var ApplicationHeading = data.Application_heading;
		var ApplicationSubtitle= data.Application_body;

		var ApplicationContent = /*html*/`
			<div class="cDisplay-4 text-primary border-top border-bottom d-block d-md-inline py-2">${ApplicationTop}</div>
			<div class="cDisplay-1 mt-3 mb-5 text-primary font-weight-lighter">${ApplicationHeading}</div>
			<div class="offset-1 col-10 pb-5">${ApplicationSubtitle}</div>
		`;
		$(`#${module}application`).append(ApplicationContent);
		
		$.each(data.Application_btns, function (index, value) {
			var ApplicationBtnTxt = value.value.item_btn_text;
			var ApplicationBtnLink = value.value.item_btn_link;

			ApplicationBtns = /*html*/`
				<div class="">
					<a class="btn btn-info position-relative text-white mb-4 pr-5 text-uppercase" href="${ApplicationBtnLink}" target="_blank">${ApplicationBtnTxt} <i class="fal fa-arrow-circle-right position-absolute"></i></a>
				</div>
			`;
			$(`#${module}application`).append(ApplicationBtns);
		});
		//#endregion

	// 	//Region awards
	// 	var AwardsFormModal = /*html*/ `
	// 	<!-- Modal -->
	// 	<div class="modal fade" id="AwardsFormModal" tabindex="-1" role="dialog" aria-labelledby="AwardsFormModalTitle" aria-hidden="true">
	// 		<div class="modal-dialog modal-xl" role="document">
	// 			<div class="modal-content">
	// 				<form class="text-left p-3" id="${module}AwardsForm">
	// 					<div class="modal-header">
	// 						<div class="modal-title w-100">${translationArray['Awards_intro' + language]}</div>
	// 						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	// 							<span aria-hidden="true">&times;</span>
	// 						</button>
	// 					</div>
	// 					<div class="modal-body">
	// 						<!-- //div class="text-center pb-4">${translationArray['Awards_name' + language]}</div> -->
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}Name">${translationArray['Awards_name' + language]} <sup class="text-danger">*</sup></label>
	// 									<input name="Name" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Name" type="text">
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<label for="${module}Category">${translationArray['Awards_category' + language]} <sup class="text-danger">*</sup></label>
	// 								<div class="form-group mb-3">
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category1" value="K1" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category1">K1 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category2" value="K2" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category2">K2 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category3" value="K3" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K3 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category4" value="K4" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K4 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category5" value="K5" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K5 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category6" value="K6" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K6 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category7" value="K7" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K7 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category8" value="K8" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K8 &nbsp;</label>
	// 									</div>
	// 								</div>
	// 								<div class="form-group mb-3">
	// 									<fieldset id="Stars">
	// 										<div class="custom-control custom-radio">
	// 											<input class="custom-control-input" type="radio" id="${module}Stars4" name="Stars" value="4" disabled>
	// 											<label class="custom-control-label" for="${module}Stars4">4 звезди</label>
	// 										</div>
	// 										<div class="custom-control custom-radio">
	// 											<input class="custom-control-input" type="radio" id="${module}Stars5" name="Stars" value="5" disabled>
	// 											<label class="custom-control-label" for="${module}Stars5">5 звезди</label>
	// 										</div>
	// 									</fieldset>
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}Phone">${translationArray['phone' + language]} <sup class="text-danger">*</sup></label>
	// 									<input name="Phone" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Phone" type="text">
	// 								</div>
	// 							</div>
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}Email">${translationArray['email' + language]} <sup class="text-danger">*</sup></label>
	// 									<input name="Email" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Email" type="text">
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}AwardsCandidate">${translationArray['Awards_candidate_why_us' + language]} <sup class="text-danger">*</sup></label>
	// 									<textarea name="AwardsCandidate" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}AwardsCandidate" type="textarea" rows="5"></textarea>
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}AgreementAwards">${translationArray['agreement' + language]} <sup class="text-danger">*</sup></label>
	// 									<div class="clearfix"></div>
	// 									<div class="custom-control custom-ckeckbox form-check form-check-inline d-inline">
	// 										<input type="checkbox" class="custom-control-input" id="${module}AgreementAwards" name="AgreementAwards" value="Checked" required>
	// 										<label class="custom-control-label" for="${module}AgreementAwards">${translationArray['agreement_text' + language]}</label>
	// 									</div>
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<!-- Awards_form_accompanying -->
	// 						<hr class="border-light"/>
	// 						<div class="text-center py-4">${translationArray['Awards_description' + language]}</div>
	// 					</div>
	// 					<div class="modal-footer">
	// 						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	// 						<button type="submit" class="btn btn-primary position-relative" id="${module}SendBtn">${translationArray['send_button' + language]} </button>
	// 					</div>
						
	// 					<input type="hidden" name="Page" value="Awards">
	// 				</form>
	// 			</div>
	// 		</div>
	// 	</div>
	// 	<div class="modal fade" tabindex="-1" role="dialog" id="AwardsthankyouModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
	// 		<div class="modal-dialog" role="document">
	// 			<div class="modal-content">
	// 				<div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	// 					<div class="m-auto text-center lead pt-4" id="${module}ThankYouMessage">${translationArray['thankyou_registration' + language]}</div>
	// 				</div>
	// 			</div>
	// 		</div>
	// 	</div>	
	// `;
	// $('body').append(AwardsFormModal);

	
	// $.validator.setDefaults({
	// 	submitHandler: function () {
	// 		var FormArray = $('#' + module + 'AwardsForm').serializeArray();
	// 		var formName = 'Awards';
	// 		sendFormFunc(formName, FormArray);
	// 		// console.log(formName);
	// 		// console.log(FormArray);
	// 		$('#' + module + 'AwardsForm').trigger("reset");
	// 		$('#AwardsFormModal').modal('hide');
	// 		$('#AwardsthankyouModal').modal('show');
	// 		setTimeout(function () {
	// 			$('#AwardsthankyouModal').modal('hide');
	// 		}, 15000);
	// 	}
	// });
	// $('#' + module + 'AwardsForm').validate({
	// 	rules: {
	// 		Name: "required",
	// 		Phone: "required",
	// 		'Category[]' : "required",
	// 		Stars: {
	// 			required: "#CongressCategory1:checked",
	// 			minlength: 1
	// 		},
	// 		Stars: {
	// 			required: "#CongressCategory2:checked",
	// 			minlength: 1
	// 		},
	// 		Email: {
	// 			required: true,
	// 			email: true
	// 		},
	// 		AwardsCandidate: "required",
	// 		AgreementAwards: "required"
	// 	},
	// 	messages: {
	// 		Name: translationArray['invalid_name' + language],
	// 		Email: translationArray['invalid_email' + language],
	// 		Category: "At least 1 check",
	// 		Message: translationArray['invalid_message' + language],
	// 	},
	// 	errorElement: "div",
	// 	errorPlacement: function (error, element) {
	// 		// Add the `help-block` class to the error element
	// 		error.addClass("invalid-feedback");
	// 		if ((element.prop("type") === "checkbox") || (element.prop("type") === "radio")) {
	// 			error.insertAfter(element.parent("label"));
	// 		} else {
	// 			error.insertAfter(element);
	// 		}
	// 	},
	// 	highlight: function (element, errorClass, validClass) {
	// 		$(element).addClass("is-invalid").removeClass("is-valid");
	// 	},
	// 	unhighlight: function (element, errorClass, validClass) {
	// 		$(element).addClass("is-valid").removeClass("is-invalid");
	// 	}
	// });

	// //code to hide topic selection, disable for demo
	// var checkedCategory1 = $("#" + module + "Category1"); 
	// // Hotel stars are optional, hide at first
	// var inital1 = checkedCategory1.is(":checked");
	// var checkStars1 = $("#Stars")[inital1 ? "removeClass" : "addClass"]("hide");
	// var StarsInputs1 = checkStars1.find("input").attr("disabled", !inital1);
	// //show when Category is checked
	// checkedCategory1.click(function() {
	// 	checkStars1[this.checked ? "removeClass" : "addClass"]("hide");
	// 	StarsInputs1.attr("disabled", !this.checked);
	// });
	// //code to hide topic selection, disable for demo
	// var checkedCategory2 = $("#" + module + "Category2");
	// // Hotel stars are optional, hide at first
	// var inital2 = checkedCategory2.is(":checked");
	// var checkStars2 = $("#Stars")[inital2 ? "removeClass" : "addClass"]("hide");
	// var StarsInputs2 = checkStars2.find("input").attr("disabled", !inital2);
	// //show when Category is checked
	// checkedCategory2.click(function() {
	// 	checkStars2[this.checked ? "removeClass" : "addClass"]("hide");
	// 	StarsInputs2.attr("disabled", !this.checked);
	// });
	// //#end region Awards registration

		//#region Winners
		var WinnerImage = data.Winners_image.path;
		var WinnerTop = data.Winners_top;
		var WinnerHeading = data.Winners_heading;
		var WinnerBody = data.Winners_body;

		$(`#${module}WinnerBg`).css('background-image', 'url("' + streamSRV + '/api/cockpit/image' + token + '&src=' + streamSRV + '/storage/uploads' + WinnerImage + '&w=1920&o=1&m=thumbnail&f[darken]=0)"');
		
		var winnerContent = /*html*/`
			<div class="cDisplay-4 text-primary border-top border-bottom d-block d-md-inline py-2">${WinnerTop}</div>
			<div class="cDisplay-1 text-white pt-3 pb-5">${WinnerHeading}</div>
			<div class="text-white">${WinnerBody}</div>
		`;
		$(`#${module}winner`).append(winnerContent);
		//#endregion

		//#region Last winners
		var LastWinnersTop = data.Last_winners_top;
		var LastWinnersHeading = data.Last_winners_heading;
		var LastWinnersBottomTxt = data.Last_winners_bottom;
		var LastWinnersBottomLink = data.Last_winners_bottom_link;

		var lastWinnersContent = /*html*/`
			<div class="cDisplay-4 text-primary border-top border-bottom d-block d-md-inline py-2">${LastWinnersTop}</div>
			<div class="cDisplay-1 text-primary pt-3 pb-5">${LastWinnersHeading}</div>
			<div class="row" id="${module}LastWinnerGallery">
			</div>
			<div class="pt-5">
				<hr class="col-4 mb-0">
				<a class="btn btn-ilink position-relative text-primary mb-4 pr-5" href="${LastWinnersBottomLink}" target="_blank">${LastWinnersBottomTxt}</a>
			</div>
		`;
		$(`#${module}lastwinners`).append(lastWinnersContent);

		$.each(data.Last_winners_gallery, function (index, value) {
			var thumbItem = /*html*/ `
				<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/${value.path}&h=600&o=1&m=thumbnail&q=80" class="col-4 pb-4 img-fluid ${module}image${index} w-100"></img>`
			$(`#${module}LastWinnerGallery`).append(thumbItem);
		});
		//#endregion
		

		//#region Sponsors
		var SponsorsTop = data.Sponsors_top;
		var SponsorsHeading = data.Sponsors_heading;
		var SponsorsSubtitle = data.Sponsors_subtitle;
		var SponsorsBody = data.Sponsors_body;

		var SponsorsContentHeading = /*html*/`
			<div class="cDisplay-4 text-primary border-top border-bottom d-block d-md-inline py-2">${SponsorsTop}</div>
			<div class="cDisplay-1 text-primary mt-3 mb-5 text-center">${SponsorsHeading}</div>
			<div class="cDisplay-2 text-primary mt-3 mb-5 text-center">${SponsorsSubtitle}</div>
			<div class="pb-5 mb-5">${SponsorsBody}</div>
		`;
		$(`#${module}sponsorsHeading`).append(SponsorsContentHeading);
		//#endregion

		//#region Packages
		var PackagesHeading = data.Sponsors_packages_heading;
		var PackagesBtnTxt = data.Sponsors_packages_btn_text;
		var PackagesBtnLink = data.Sponsors_packages_btn_link;
		
		var PackagesContentHeading = /*html*/`
			<div class="row justify-content-center">
				<div class="cDisplay-2 text-primary mt-3 mb-1 text-center">${PackagesHeading}</div>
				<hr class="col-10 border-white">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="infobox-first">
					<div>P</div>
					<h4 class="text-gray">${translationArray['premium_sponsor']}</h4>
						
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="infobox-second">
					<div>P</div>
					<h4 class="text-gray">${translationArray['gold_sponsor']}</h4>
					
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="infobox-third">
					<div>P</div>
					<h4 class="text-gray">${translationArray['silver_sponsor']}</h4>
					
					</div>
				</div>
			</div>
			<div class="row justify-content-center pb-5 mb-5">
				<a class="btn btn-info position-relative text-white mb-4 pr-5 text-uppercase" href="${PackagesBtnLink}" target="_blank">${PackagesBtnTxt} <i class="fal fa-arrow-circle-right position-absolute"></i></a>
			</div>
		`;
		$(`#${module}packagesHeading`).append(PackagesContentHeading);
		
		$.each(data.Sponsors_packages, function(index, value){
			var selected = value.value.packages;

			var PackageItem = /*html*/ `
				<p style="text-align-center">
					<a href="${value.value.logo_link}" target="_blank">
						<img src="${streamSRV}${value.value.sponsor_logo.path}" class="col-8 pb-4 img-fluid w-100">
					</a>
				</p>
			`;
			$(`.infobox-${selected}`).append(PackageItem);
        });
		//#endregion

		//#region Partners
		var PartnersTop = data.Partners_top;
		var PartnersHeading = data.Partners_heading;
		var PartnersSubtitle = data.Partners_subtitle;
		var PartnersBody = data.Partners_body;
		
		var PartnersContentHeading = /*html*/`
			<div class="cDisplay-4 text-primary border-top border-bottom d-block d-md-inline py-2">${PartnersTop}</div>
			<div class="cDisplay-1 text-primary mt-3 mb-5 text-center">${PartnersHeading}</div>
			<div class="pb-5 mb-5">${PartnersBody}</div>
			<div class="cDisplay-4 text-primary mt-3 mb-5 text-center text-uppercase">${PartnersSubtitle}</div>
			<div class="row pb-5 mb-4 align-items-center px-2" id="${module}PartnersLogoCountainer">

			</div>
		`;
		$(`#${module}partnersHeading`).append(PartnersContentHeading);

		console.log(data);
		$.each(data.Partners_logos_panel, function(index, value){
			var PartnerLogoItem = /*html*/ `
				<a href="${value.value.logo_item_link}" class="col-3 pl-0 pr-5" target="_blank">
					<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/${value.value.logo_item.path}&h=300&o=1&m=thumbnail&q=80" class="img-fluid w-100">
				</a>
			`;
			$(`#${module}PartnersLogoCountainer`).append(PartnerLogoItem);
        });
		//#endregion

		//#region Contacts
		var ContactsTop = data.Contacts_top;
		var ContactsHeading = data.Contacts_heading;
		var ContansBg = data.Contacts_image.path;
		console.log(data);

		$(`#${module}ContactsBg`).css('background-image', 'url("' + streamSRV + '/api/cockpit/image' + token + '&src=' + streamSRV + '/storage/uploads' + ContansBg + '&w=1920&o=1&m=thumbnail&f[darken]=0)"');

		var ContactsContentHeading = /*html*/`
			<div class="cDisplay-4 border-top border-bottom d-block d-md-inline py-2">${ContactsTop}</div>
			<div class="cDisplay-1 mt-3 mb-5 text-center text-white">${ContactsHeading}</div>
		`;
		$(`#${module}contactsHeading`).append(ContactsContentHeading);
		
		//form
		var formTemplate = /*html*/ `
			<form class="pb-5 text-left" id="${module}Form">
				<div class="input-group input-group-sm mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text px-3 py-2 border-0 bg-white"><i class="fal fa-user"></i></span>
					</div>
					<input name="Name" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Name" type="text" placeholder="${translationArray['form_name' + language]}">
					<label for="${module}Name" class="sr-only">${translationArray['form_name']}</label>
				</div>
				<div class="input-group input-group-sm mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text px-3 py-2 border-0 bg-white"><i class="fal fa-envelope"></i></span>
					</div>
					<input name="Email" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Email" type="text" placeholder="${translationArray['form_email' + language]}">
					<label for="${module}Email" class="sr-only">${translationArray['form_email']}</label>
				</div>
				<div class="input-group input-group-sm mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text px-3 py-2 border-0 bg-white"><i class="fal fa-align-justify"></i></span>
					</div>
					<textarea name="Message" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Message" type="textarea" rows="5" placeholder="${translationArray['form_message']}"></textarea>
					<label for="${module}Message" class="sr-only">${translationArray['form_message']}</label>
				</div>
				<div class="text-center"><button type="submit" class="btn btn-primary position-relative pr-5" id="${module}SendBtn">${translationArray['send_button']} <i class="fal fa-arrow-circle-right position-absolute"></i></button></div>
				<input type="hidden" name="Page" value="Contacts">
			</form>
			<div class="modal fade" tabindex="-1" role="dialog" id="thankyouModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<div class="m-auto text-center lead pt-4" id="${module}ThankYouMessage">${translationArray['thankyou_form']}</div>
						</div>
					</div>
				</div>
			</div>	
		`;
		$('#' + module + 'contactsForm').append(formTemplate);
		// setTimeout(function () {
		// 	$('#' + module + 'Name').attr('placeholder', translationArray['form_name' + language]).next().append(translationArray['your_name' + language]);
		// 	$('#' + module + 'Email').attr('placeholder', translationArray['form_email' + language]).next().append(translationArray['your_email' + language]);
		// 	$('#' + module + 'Message').attr('placeholder', translationArray['form_message' + language]).next().append(translationArray['your_message' + language]);
		// 	$('#' + module + 'SendBtn').append(translationArray['send_button' + language]);
		// 	$('#' + module + 'ThankYouMessage').append(translationArray['thankyou_form' + language]);
		// }, 700);


		$.validator.setDefaults({
			submitHandler: function () {
				const FormArray = $('#' + module + 'Form').serializeArray();
				var formName = 'Contact';
				sendFormFunc(formName, FormArray);
				$('#' + module + 'Form').trigger("reset");
				$('#thankyouModal').modal('show');
				setTimeout(function () {
					$('#thankyouModal').modal('hide');
				}, 5000);
			}
		});
		$('#' + module + 'Form').validate({
			rules: {
				Name: "required",
				Email: {
					required: true,
					email: true
				},
				Message: "required"
			},
			messages: {
				Name: translationArray['invalid_name' + language],
				Email: translationArray['invalid_email' + language],
				Message: translationArray['invalid_message' + language]
			},
			errorElement: "div",
			errorPlacement: function (error, element) {
				// Add the `help-block` class to the error element
				error.addClass("invalid-feedback");
				if (element.prop("type") === "checkbox") {
					error.insertAfter(element.parent("label"));
				} else {
					error.insertAfter(element);
				}
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass("is-invalid").removeClass("is-valid");
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).addClass("is-valid").removeClass("is-invalid");
			}
		});
		//#endForm

		//data is populated from HFSection (main singelton)
		var ContactsAddressContent = /*html*/`
			<div class="col-md-4 col-12 pb-3 text-center">
				<i class="fal fa-envelope text-primary fa-2x"></i>
				<h5 class="font-weight-normal pt-3">${translationArray['contact_email']}</h5>
				<div class="email-container text-primary"></div>
			</div>
			<div class="col-md-4 col-12 pb-3 text-center">
				<i class="fal fa-location-arrow text-primary fa-2x"></i>
				<h5 class="font-weight-normal pt-3">${translationArray['event_location']}</h5>
				<div class="location-container text-primary"></div>
			</div>
			<div class="col-md-4 col-12 pb-3 text-center">
				<i class="fal fa-map-marked-alt text-primary fa-2x"></i>
				<h5 class="font-weight-normal pt-3">${translationArray['contact_view_locations']}</h5>
				<div class="address-container text-primary"></div>
			</div>
		`;
		$(`#${module}contactsAddress`).append(ContactsAddressContent);
		//#end Contacts region
	})
	.then(function(){
		// Add smooth scrolling to all links
		$("nav a").on('click', function(event) {
			// Make sure this.hash has a value before overriding default behavior
			if (this.hash !== "") {
				// Prevent default anchor click behavior
				event.preventDefault();
			
				// Store hash
				var hash = this.hash;
			
				// Using jQuery's animate() method to add smooth page scroll
				// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
				$('html, body').animate({
				scrollTop: $(hash).offset().top
				}, 800, function(){
			
					// Add hash (#) to URL when done scrolling (default click behavior)
					window.location.hash = hash;
				});
			} // End if
		});
		$('#navbarLogo').on('click', function(){
			$('html, body').animate({scrollTop: 0}, 800);
		});
	});
}
