function certificationLoadContent() {
	listingPage = true;
	module = "certification";
	var mainTemplate = /*html*/ `
		<div id="introduction" >
			<div class="container py-5">
				<div class="row pt-3 p-md-5">
					<div class="col-12" id="introductionBody"></div>
				</div>
				<div class="row justify-content-md-center py-5" id="images"></div>
				<div class="row pb- p-md-3 p-lg-5">
					<div class="col-12" id="fullBody"></div>
				</div>
			</div>
		</div>
		
    `;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + module + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			console.log(data);
			//#region Introduction
			let IntroductionText = data['Intro_text' + language];
			$('#introductionBody').append(IntroductionText);
			//#endregion Introduction
			
			//#region Section data
			for (let value of data.Images) {
				let sectionTemaplate = /*html*/ `
					<div class="col-xl-2">
						<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}${value.path}&w=182&o=1&m=thumbnail&q=80" class="img-fluid">
					</div>
				`;
				$('#images').append(sectionTemaplate);
			}

			let FullText = data['Full_text' + language];
			$('#fullBody').append(FullText);
			
			//#endregion Section data

		});
}