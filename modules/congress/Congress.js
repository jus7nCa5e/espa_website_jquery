/* global token, language, fetch, getImage, getURL, urlLanguage, translationArray, loader, baseUrl, mySwiper */

videos = [];
var module = 'ESPAcongress'; //module name
listingPage = true;

//#region template

var pageTeplate = /*html*/`
	</div>
	<div class="container" id="overview">
		<div class="row pt-5">
			<div class="col-lg-10 offset-lg-1 col-12 offset-0 pt-5 text-center" id="${module}overview"></div>
		</div>
	</div>
	<div class="container" id="registration">
		<div class="row py-5">
			<div class="col-12 offset-0 py-5 text-center" id="${module}registration"></div>
		</div>
	</div>
	<div class="section" id="${module}DestiantionBg">
		<div class="container pb-5" id="destination">
			<div class="row pt-5">
				<div class="col-12 pt-5 text-center" id="${module}destination"></div>
			</div>
			<div class="row pb-5" id="${module}destinationGallery">
				<div class="${module}destinationGallery-swiper-container swiper-container">
					<div class="${module}destinationGallery-swiper-wrapper swiper-wrapper"></div>
					<div class="${module}destinationGallery-swiper-button-next swiper-button-next swiper-button-white"></div>
					<div class="${module}destinationGallery-swiper-button-prev swiper-button-prev swiper-button-white"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="container" id="programme">
		<div class="row pt-5">
			<div class="col-lg-10 offset-lg-1 col-12 offset-0 pt-5 px-0" id="${module}programme"></div>
		</div>
		<div class="row " id="${module}programmeGallery">
			<div class="${module}programmeGallery-swiper-container swiper-container">
				<div class="${module}programmeGallery-swiper-wrapper swiper-wrapper"></div>
				<div class="${module}programmeGallery-swiper-button-next swiper-button-next swiper-button-white"></div>
				<div class="${module}programmeGallery-swiper-button-prev swiper-button-prev swiper-button-white"></div>
			</div>
		</div>
		<div class="row pt-5">
			<div class="col-lg-10 offset-lg-1 col-12 offset-0" id="${module}programmeP2"></div>
		</div>
	</div>
	<div class="container" id="speakers">
		<div class="row pt-5">
			<div class="col-12 pt-5 text-center" id="${module}speakersHeading"></div>
		</div>
		<div class="row justify-content-sm-center align-items-top" id="${module}speakers"></div>	
	</div>
	<div class="container" id="sponsors">
		<div class="row pt-5">
			<div class="col-12 pt-5 text-center" id="${module}sponsorsHeading"></div>
		</div>
		<div class="row justify-content-sm-center align-items-center" id="${module}sponsors"></div>	
	</div>
	<div class="container" id="contacts">
		<div class="row pt-5">
			<div class="col-12 pt-5 text-center" id="${module}contactsHeading"></div>
		</div>
		<div class="row py-5 ">
			<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-12 offset-0 text-center" id="${module}contactsForm"></div>
			<ul class="col-12 justify-content-center pt-3" id="footerSocials"></ul>
		</div>
		<div class="row py-5" id="${module}contactsAddress"></div>
		<div class="row pt-5"><div class="col-12" id="footerDown"></div></div>
	</div>
`;
$('header').after(pageTeplate);

 //#endregion

function congressLoadContent() {
    fetch(getSingletoneURL + module + token)
    .then (function(response) {
        return response.json();
    })
    .then (function (data){
		// console.log(data);
		//#region Intro
		var IntroTop = data.Intro_top;
		var IntroHeading = data.Intro_heading;
		var IntroBottom = data.Intro_bottom;

		var introContent = /*html*/`
			<div class="container my-auto text-center">
				<div class="row">
					<div class="col-lg-8 offset-lg-2 col-12 offset-0">
						<div class="cDisplay-4 bg-white d-block d-md-inline">${IntroTop}</div>
						<div class="cDisplay-2 bg-white my-3 text-primary">${IntroHeading}</div>
						<div class="bg-special text-white font-weight-bold d-block d-sm-inline py-1 px-2">${IntroBottom}</div>
					</div>
				</div>
			</div>
		`;
		$(`#cover`).append(introContent);
		//#endregion

		//#region Overview
		var OverviewTop = data.Overview_top;
		var OverviewHeading = data.Overview_heading;
		var OverviewBody = data.Overview_body;

		var OverviewContent = /*html*/`
			<div class="cDisplay-4 text-secondary border-top border-bottom d-block d-md-inline py-2">${OverviewTop}</div>
			<div class="cDisplay-1 mt-3 mb-5">${OverviewHeading}</div>
			<div class="">${OverviewBody}</div>
		`;
		$(`#${module}overview`).append(OverviewContent);
		//#endregion

		//#region Registration
		var RegistrationTop = data.Registration_top;
		var RegistrationHeading = data.Registration_heading;
		var RegistrationBody = data.Registration_body;
		var RegistrationBtnTxt = data.Registration_btn_txt;
		var RegistrationFormat = data.Registration_info; 
		// var RegistrationPDFLink = data.Registration_pdf_link.path; 
		var CongressBookingBtnTxt = data.Congress_booking_btn_txt;
		var CongressBookingBtnLink = data.Congress_booking_btn_link;
		var ReservationBtnTxt1 = data.Reservation_btn_txt;
		var ReservationBtnLink1 = data.Reservation_btn_link;
		var ReservationBtnTxt2 = data.Reservation_btn_txt_2;
		var ReservationBtnLink2 = data.Reservation_btn_link_2;
		var ReservationBtnTxt3 = data.Reservation_btn_txt_3;
		var ReservationBtnLink3 = data.Reservation_btn_link_3;
		var TransferBookingBtnTxt = data.Transfer_booking_btn_txt;
		var TransferBookingBtnLink = data.Transfer_booking_btn_link;

		var RegistrationContent = /*html*/`
			<div class="cDisplay-4 border-top border-bottom d-block d-md-inline py-2">${RegistrationTop}</div>
			<div class="cDisplay-1 mt-3 mb-5">${RegistrationHeading}</div>
			<div class="">${RegistrationBody}</div>
		
				<div class="pt-4 clearfix row justify-content-center">
					<div id="RegInfo" class="col-12 col-lg-4"> 
						
					</div>
					<div class="col-12 col-lg-4">
						<a class="w-100 btn btn-primary position-relative mb-3 pr-5" href="${CongressBookingBtnLink}" target="_blank">${CongressBookingBtnTxt} <i class="fal fa-arrow-circle-right position-absolute"></i></a>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-lg-4">
						<a class="w-100 btn btn-primary position-relative mb-3 pr-5" href="${ReservationBtnLink1}" target="_blank">${ReservationBtnTxt1} <i class="fal fa-arrow-circle-right position-absolute"></i></a>
					</div>
					<div class="col-12 col-lg-4">
						<a class="w-100 btn btn-primary position-relative mb-3 pr-5" href="${ReservationBtnLink2}" target="_blank">${ReservationBtnTxt2} <i class="fal fa-arrow-circle-right position-absolute"></i></a>
					</div>
					<div class="col-12 col-lg-4">
						<a class="w-100 btn btn-primary position-relative mb-3 pr-5" href="${ReservationBtnLink3}" target="_blank">${ReservationBtnTxt3} <i class="fal fa-arrow-circle-right position-absolute"></i></a>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<a class="w-100 btn btn-primary position-relative mb-3 pr-5" href="${TransferBookingBtnLink}" target="_blank">${TransferBookingBtnTxt} <i class="fal fa-arrow-circle-right position-absolute"></i></a>
					</div>
				</div>
		`;

	$(`#${module}registration`).append(RegistrationContent);

	if ((RegistrationFormat === 'attach PDF file') && (data.Registration_pdf_link)) {
		var RegistrationInfo = /*html*/`
			<a class="w-100 btn btn-primary position-relative mb-3 pr-5"  target="_blank" href="${streamSRVuploads + data.Registration_pdf_link.path}">${RegistrationBtnTxt} <i class="fal fa-arrow-circle-right position-absolute"></i></a>`
		;
	} else if (RegistrationFormat === 'Registration form'){
		var RegistrationInfo = /*html*/`
			<a class="w-100 btn btn-primary position-relative mb-3 pr-5"  data-toggle="modal" data-target="#RegistrationFormModal" href="#RegistrationFormModal">${RegistrationBtnTxt} <i class="fal fa-arrow-circle-right position-absolute"></i></a>`
		;
	}
	$('#RegInfo').append(RegistrationInfo);

		var RegistrationFormModal = /*html*/ `		
				<!-- Modal -->
				<div class="modal fade" id="RegistrationFormModal" tabindex="-1" role="dialog" aria-labelledby="RegistrationFormModalTitle" aria-hidden="true">
					<div class="modal-dialog modal-xl" role="document">
						<div class="modal-content">
							<form class="text-left" id="${module}RegistrationForm">
								<div class="modal-header">
									<div class="modal-title w-100">${translationArray['Registration_form_intro' + language]}</div>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="text-center pb-4">${translationArray['Registration_form_participant' + language]}</div>
									<div class="form-row">
										<div class="col-lg-auto col-12">
											<div class="form-group mt-4 mb-3">
												<div class="custom-control custom-radio form-check form-check-inline d-inline">
													<input type="radio" class="custom-control-input" id="PersonTitleMrs" name="Title" value="${translationArray['person_title_mrs']}" required>
													<label class="custom-control-label" for="PersonTitleMrs">${translationArray['person_title_mrs']}</label>
												</div>
												<div class="custom-control custom-radio form-check form-check-inline d-inline">
													<input type="radio" class="custom-control-input" id="PersonTitleMs" name="Title" value="${translationArray['person_title_ms']}" required>
													<label class="custom-control-label" for="PersonTitleMs">${translationArray['person_title_ms']}</label>
												</div>
												<div class="custom-control custom-radio form-check form-check-inline d-inline">
													<input type="radio" class="custom-control-input" id="PersonTitleMr" name="Title" value="${translationArray['person_title_mr']}" required>
													<label class="custom-control-label" for="PersonTitleMr">${translationArray['person_title_mr']}</label>
												</div>
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}LastName">${translationArray['last_name' + language]} <sup class="text-danger">*</sup></label>
												<input name="LastName" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}LastName" type="text">
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}FirstName">${translationArray['first_name' + language]} <sup class="text-danger">*</sup></label>
												<input name="FirstName" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}FirstName" type="text">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}Address">${translationArray['address' + language]} <sup class="text-danger">*</sup></label>
												<input name="Address" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Address" type="text">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}PostCode">${translationArray['postcode' + language]} <sup class="text-danger">*</sup></label>
												<input name="PostCode" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}PostCode" type="text">
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}City">${translationArray['city' + language]} <sup class="text-danger">*</sup></label>
												<input name="City" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}City" type="text">
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}Country">${translationArray['country' + language]} <sup class="text-danger">*</sup></label>
												<input name="Country" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Country" type="text">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}Phone">${translationArray['phone' + language]} <sup class="text-danger">*</sup></label>
												<input name="Phone" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Phone" type="text">
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}Email">${translationArray['email' + language]} <sup class="text-danger">*</sup></label>
												<input name="Email" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Email" type="text">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}Organization">${translationArray['organization' + language]} <sup class="text-danger">*</sup></label>
												<input name="Organization" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Organization" type="text">
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}ESPAMemberOrganisationY">${translationArray['member' + language]} <sup class="text-danger">*</sup></label>
												<div class="clearfix"></div>
												<div class="custom-control custom-radio form-check form-check-inline d-inline">
													<input type="radio" class="custom-control-input" id="ESPAMemberOrganisationY" name="ESPAMemberOrganisation" value="${translationArray['yes' + language]}" required>
													<label class="custom-control-label" for="ESPAMemberOrganisationY">${translationArray['yes' + language]}</label>
												</div>
												<div class="custom-control custom-radio form-check form-check-inline d-inline">
													<input type="radio" class="custom-control-input" id="ESPAMemberOrganisationN" name="ESPAMemberOrganisation" value="${translationArray['no' + language]}" required>
													<label class="custom-control-label" for="ESPAMemberOrganisationN">${translationArray['no' + language]}</label>
												</div>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}Agreement">${translationArray['agreement' + language]} <sup class="text-danger">*</sup></label>
												<div class="clearfix"></div>
												<div class="custom-control custom-ckeckbox form-check form-check-inline d-inline">
													<input type="checkbox" class="custom-control-input" id="${module}Agreement" name="Agreement" value="Checked" required>
													<label class="custom-control-label" for="${module}Agreement">${translationArray['agreement_text' + language]}</label>
												</div>
											</div>
										</div>
									</div>
									<!-- //Registration_form_accompanying -->
									<hr class="border-light" />
									<div class="text-center py-4">${translationArray['Registration_form_accompanying' + language]}</div>
									<div class="form-row">
										<div class="col-lg-auto col-12">
											<div class="form-group mt-4 mb-3">
												<div class="custom-control custom-radio form-check form-check-inline d-inline">
													<input type="radio" class="custom-control-input" id="PersonTitleMrsA" name="TitleA" value="${translationArray['person_title_mrs']}">
													<label class="custom-control-label" for="PersonTitleMrsA">${translationArray['person_title_mrs']}</label>
												</div>
												<div class="custom-control custom-radio form-check form-check-inline d-inline">
													<input type="radio" class="custom-control-input" id="PersonTitleMsA" name="TitleA" value="${translationArray['person_title_ms']}">
													<label class="custom-control-label" for="PersonTitleMsA">${translationArray['person_title_ms']}</label>
												</div>
												<div class="custom-control custom-radio form-check form-check-inline d-inline">
													<input type="radio" class="custom-control-input" id="PersonTitleMrA" name="TitleA" value="${translationArray['person_title_mr']}">
													<label class="custom-control-label" for="PersonTitleMrA">${translationArray['person_title_mr']}</label>
												</div>
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}LastNameA">${translationArray['last_name' + language]}</label>
												<input name="LastNameA" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}LastNameA" type="text">
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}FirstNameA">${translationArray['first_name' + language]}</label>
												<input name="FirstNameA" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}FirstNameA" type="text">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}AddressA">${translationArray['address' + language]}</label>
												<input name="AddressA" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}AddressA" type="text">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}PostCodeA">${translationArray['postcode' + language]}</label>
												<input name="PostCodeA" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}PostCodeA" type="text">
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}CityA">${translationArray['city' + language]}</label>
												<input name="CityA" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}CityA" type="text">
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}CountryA">${translationArray['country' + language]}</label>
												<input name="CountryA" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}CountryA" type="text">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}PhoneA">${translationArray['phone' + language]}</label>
												<input name="PhoneA" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}PhoneA" type="text">
											</div>
										</div>
										<div class="col-lg col-12">
											<div class="form-group mb-3">
												<label for="${module}EmailA">${translationArray['email' + language]}</label>
												<input name="EmailA" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}EmailA" type="text">
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
									<button type="submit" class="btn btn-primary position-relative pr-5" id="${module}SendBtn">${translationArray['send_button' + language]} <i class="fal fa-arrow-circle-right position-absolute"></i></button>
								</div>
								
								<input type="hidden" name="Page" value="RegistrationCongress">
							</form>
						</div>
					</div>
				</div>
				<div class="modal fade" tabindex="-1" role="dialog" id="registrationthankyouModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<div class="m-auto text-center lead pt-4" id="${module}ThankYouMessageRegistration">${translationArray['thankyou_registration' + language]}</div>
							</div>
						</div>
					</div>
				</div>	
			`;
		$('body').append(RegistrationFormModal);


		$.validator.setDefaults({
			submitHandler: function () {
				const FormArray = $('#' + module + 'RegistrationForm').serializeArray();
				var formName = 'CongressRegistration';
				sendFormFunc(formName, FormArray);
				console.log(formName);
				console.log(FormArray);
				$('#' + module + 'RegistrationForm').trigger("reset");
				$('#RegistrationFormModal').modal('hide');
				$('#registrationthankyouModal').modal('show');
				setTimeout(function () {
					$('#registrationthankyouModal').modal('hide');
				}, 15000);
			}
		});
		$('#' + module + 'RegistrationForm').validate({
			rules: {
				Title: "required",
				LastName: "required",
				FirstName: "required",
				Phone: "required",
				Email: {
					required: true,
					email: true
				},
				Organization: "required",
				ESPAMemberOrganisation: "required",
				Agreement: "required",
				// TitleA: "required",
				// LastNameA: "required",
				// FirstNameA: "required",
				// AddressA: "required",
				// PostCodeA: "required",
				// CityA: "required",
				// CountryA: "required",
				// PhoneA: "required",
				// EmailA: {
				// 	required: true,
				// 	email: true
				// }
			},
			messages: {
				Name: translationArray['invalid_name' + language],
				Email: translationArray['invalid_email' + language],
				Message: translationArray['invalid_message' + language]
			},
			errorElement: "div",
			errorPlacement: function (error, element) {
				// Add the `help-block` class to the error element
				error.addClass("invalid-feedback");
				if ((element.prop("type") === "checkbox") || (element.prop("type") === "radio")) {
					error.insertAfter(element.parent("label"));
				} else {
					error.insertAfter(element);
				}
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass("is-invalid").removeClass("is-valid");
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).addClass("is-valid").removeClass("is-invalid");
			}
		});
		//#endregion

	// 	//Region awards
	// 	var AwardsFormModal = /*html*/ `
	// 	<!-- Modal -->
	// 	<div class="modal fade" id="AwardsFormModal" tabindex="-1" role="dialog" aria-labelledby="AwardsFormModalTitle" aria-hidden="true">
	// 		<div class="modal-dialog modal-xl" role="document">
	// 			<div class="modal-content">
	// 				<form class="text-left p-3" id="${module}AwardsForm">
	// 					<div class="modal-header">
	// 						<div class="modal-title w-100">${translationArray['Awards_intro' + language]}</div>
	// 						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	// 							<span aria-hidden="true">&times;</span>
	// 						</button>
	// 					</div>
	// 					<div class="modal-body">
	// 						<!-- //div class="text-center pb-4">${translationArray['Awards_name' + language]}</div> -->
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}Name">${translationArray['Awards_name' + language]} <sup class="text-danger">*</sup></label>
	// 									<input name="Name" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Name" type="text">
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<label for="${module}Category">${translationArray['Awards_category' + language]} <sup class="text-danger">*</sup></label>
	// 								<div class="form-group mb-3">
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category1" value="K1" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category1">K1 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category2" value="K2" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category2">K2 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category3" value="K3" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K3 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category4" value="K4" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K4 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category5" value="K5" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K5 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category6" value="K6" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K6 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category7" value="K7" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K7 &nbsp;</label>
	// 									</div>
	// 									<div class="form-check form-check-inline mr-2">
	// 										<input class="form-check-input check" type="checkbox" id="${module}Category8" value="K8" name="Category[]">
	// 										<label class="form-check-label" for="${module}Category">K8 &nbsp;</label>
	// 									</div>
	// 								</div>
	// 								<div class="form-group mb-3">
	// 									<fieldset id="Stars">
	// 										<div class="custom-control custom-radio">
	// 											<input class="custom-control-input" type="radio" id="${module}Stars4" name="Stars" value="4" disabled>
	// 											<label class="custom-control-label" for="${module}Stars4">4 звезди</label>
	// 										</div>
	// 										<div class="custom-control custom-radio">
	// 											<input class="custom-control-input" type="radio" id="${module}Stars5" name="Stars" value="5" disabled>
	// 											<label class="custom-control-label" for="${module}Stars5">5 звезди</label>
	// 										</div>
	// 									</fieldset>
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}Phone">${translationArray['phone' + language]} <sup class="text-danger">*</sup></label>
	// 									<input name="Phone" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Phone" type="text">
	// 								</div>
	// 							</div>
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}Email">${translationArray['email' + language]} <sup class="text-danger">*</sup></label>
	// 									<input name="Email" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Email" type="text">
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}AwardsCandidate">${translationArray['Awards_candidate_why_us' + language]} <sup class="text-danger">*</sup></label>
	// 									<textarea name="AwardsCandidate" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}AwardsCandidate" type="textarea" rows="5"></textarea>
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<div class="form-row">
	// 							<div class="col-lg col-12">
	// 								<div class="form-group mb-3">
	// 									<label for="${module}AgreementAwards">${translationArray['agreement' + language]} <sup class="text-danger">*</sup></label>
	// 									<div class="clearfix"></div>
	// 									<div class="custom-control custom-ckeckbox form-check form-check-inline d-inline">
	// 										<input type="checkbox" class="custom-control-input" id="${module}AgreementAwards" name="AgreementAwards" value="Checked" required>
	// 										<label class="custom-control-label" for="${module}AgreementAwards">${translationArray['agreement_text' + language]}</label>
	// 									</div>
	// 								</div>
	// 							</div>
	// 						</div>
	// 						<!-- Awards_form_accompanying -->
	// 						<hr class="border-light"/>
	// 						<div class="text-center py-4">${translationArray['Awards_description' + language]}</div>
	// 					</div>
	// 					<div class="modal-footer">
	// 						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	// 						<button type="submit" class="btn btn-primary position-relative" id="${module}SendBtn">${translationArray['send_button' + language]} </button>
	// 					</div>
						
	// 					<input type="hidden" name="Page" value="Awards">
	// 				</form>
	// 			</div>
	// 		</div>
	// 	</div>
	// 	<div class="modal fade" tabindex="-1" role="dialog" id="AwardsthankyouModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
	// 		<div class="modal-dialog" role="document">
	// 			<div class="modal-content">
	// 				<div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	// 					<div class="m-auto text-center lead pt-4" id="${module}ThankYouMessage">${translationArray['thankyou_registration' + language]}</div>
	// 				</div>
	// 			</div>
	// 		</div>
	// 	</div>	
	// `;
	// $('body').append(AwardsFormModal);

	
	// $.validator.setDefaults({
	// 	submitHandler: function () {
	// 		var FormArray = $('#' + module + 'AwardsForm').serializeArray();
	// 		var formName = 'Awards';
	// 		sendFormFunc(formName, FormArray);
	// 		// console.log(formName);
	// 		// console.log(FormArray);
	// 		$('#' + module + 'AwardsForm').trigger("reset");
	// 		$('#AwardsFormModal').modal('hide');
	// 		$('#AwardsthankyouModal').modal('show');
	// 		setTimeout(function () {
	// 			$('#AwardsthankyouModal').modal('hide');
	// 		}, 15000);
	// 	}
	// });
	// $('#' + module + 'AwardsForm').validate({
	// 	rules: {
	// 		Name: "required",
	// 		Phone: "required",
	// 		'Category[]' : "required",
	// 		Stars: {
	// 			required: "#CongressCategory1:checked",
	// 			minlength: 1
	// 		},
	// 		Stars: {
	// 			required: "#CongressCategory2:checked",
	// 			minlength: 1
	// 		},
	// 		Email: {
	// 			required: true,
	// 			email: true
	// 		},
	// 		AwardsCandidate: "required",
	// 		AgreementAwards: "required"
	// 	},
	// 	messages: {
	// 		Name: translationArray['invalid_name' + language],
	// 		Email: translationArray['invalid_email' + language],
	// 		Category: "At least 1 check",
	// 		Message: translationArray['invalid_message' + language],
	// 	},
	// 	errorElement: "div",
	// 	errorPlacement: function (error, element) {
	// 		// Add the `help-block` class to the error element
	// 		error.addClass("invalid-feedback");
	// 		if ((element.prop("type") === "checkbox") || (element.prop("type") === "radio")) {
	// 			error.insertAfter(element.parent("label"));
	// 		} else {
	// 			error.insertAfter(element);
	// 		}
	// 	},
	// 	highlight: function (element, errorClass, validClass) {
	// 		$(element).addClass("is-invalid").removeClass("is-valid");
	// 	},
	// 	unhighlight: function (element, errorClass, validClass) {
	// 		$(element).addClass("is-valid").removeClass("is-invalid");
	// 	}
	// });

	//code to hide topic selection, disable for demo
	var checkedCategory1 = $("#" + module + "Category1"); 
	// Hotel stars are optional, hide at first
	var inital1 = checkedCategory1.is(":checked");
	var checkStars1 = $("#Stars")[inital1 ? "removeClass" : "addClass"]("hide");
	var StarsInputs1 = checkStars1.find("input").attr("disabled", !inital1);
	//show when Category is checked
	checkedCategory1.click(function() {
		checkStars1[this.checked ? "removeClass" : "addClass"]("hide");
		StarsInputs1.attr("disabled", !this.checked);
	});
	//code to hide topic selection, disable for demo
	var checkedCategory2 = $("#" + module + "Category2");
	// Hotel stars are optional, hide at first
	var inital2 = checkedCategory2.is(":checked");
	var checkStars2 = $("#Stars")[inital2 ? "removeClass" : "addClass"]("hide");
	var StarsInputs2 = checkStars2.find("input").attr("disabled", !inital2);
	//show when Category is checked
	checkedCategory2.click(function() {
		checkStars2[this.checked ? "removeClass" : "addClass"]("hide");
		StarsInputs2.attr("disabled", !this.checked);
	});

	//#end region Awards registration

		//#region Destination
		var DestinationImage = data.Destination_image.path;
		var DestinationTop = data.Destination_top;
		var DestinationHeading = data.Destination_heading;
		var DestinationBottomHeading = data.Destination_bottom_heading;
		var DestinationBottomSubheading = data.Destination_bottom_subheading;
		var DestinationVideoBtn = data.Destination_video_btn;
		var DestinationVideoID = data.Destination_video_id;
		var DestinationBody = data.Destination_body;

		// $(`#${module}DestiantionBg`).css('background-image', `url('${streamSRV}/storage/uploads${DestinationImage}')`);
		$(`#${module}DestiantionBg`).css('background-image', 'url("' + streamSRV + '/api/cockpit/image' + token + '&src=' + streamSRV + '/storage/uploads' + DestinationImage + '&w=1920&o=1&m=thumbnail&f[darken]=25)"');
		
		

		var destinationContent = /*html*/`
			<div class="cDisplay-4 bg-white py-2 d-inline">${DestinationTop}</div>
			<div class="cDisplay-1 text-white pt-3 pb-5">${DestinationHeading}</div>
			<h5 class="cDisplay-2 text-white mb-0">${DestinationBottomHeading}</h5>
			<div class="cDisplay-3 text-white font-weight-bold pb-4">${DestinationBottomSubheading}</div>
			<div class="py-4" id="${module}destinationBtn">
			
			</div>
			<div class="text-white">${DestinationBody}</div>
		`;
		$(`#${module}destination`).append(destinationContent);

		if (DestinationVideoID){
			var destinationBtn = /*html*/`
			<a href="#${module}VideoModalDest" id="${module}PlayVideoDest" data-toggle="modal" data-target="#${module}VideoModalDest" aria-label="Play video" class="btn btn-primary position-relative text-uppercase pr-5">
				${DestinationVideoBtn} <i class="fal fa-arrow-circle-right position-absolute"></i>
			</a>
			`;
			$(`#${module}destinationBtn`).append(destinationBtn);
		}
		

		videoModalDest = /*html*/ `
			<div class="modal fade" id="${module}VideoModalDest" tabindex="-1" role="dialog" aria-labelledby="VideoTitleIdDest" aria-hidden="true">
				<div class="modal-dialog modal-xl border-0" role="document">
					<div class="modal-content bg-transparent border-0 shadow-none">
						<div class="modal-body p-0">
							<button type="button" class="close text-white h2" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<div class="position-absolute w-100 h-100 mt-4" style="z-index: 1000"></div>
							<div class="embed-responsive embed-responsive-16by9">
								<div class="embed-responsive-item" id="${module}ytplayerDest"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		`;
		$("body").append(videoModalDest);

		$('#' + module + 'PlayVideoDest').on('click', function (e) {
			e.preventDefault();
			var playerVars = {
				height: '450',
				width: '850',
				modestbranding: 1,
				autoplay: 1,
				loop: 1,
				rel: 0,
				showinfo: 0,
				controls: 0, 
				disablekb: 1,
				iv_load_policy: 3
			};
			triggerYoutubeVideoModal(DestinationVideoID, playerVars, module + 'ytplayerDest');
		});

		var fullscreenGalTemplate = /*html*/ `
			<div class="${module}ImagesTab-zoom-swiper-container swiper-container full-screen">
				<span class="close">×</span>
				<div class="${module}ImagesTab-zoom-swiper-wrapper swiper-wrapper h-100"></div>
				<div class="${module}ImagesTab-zoom-swiper-button-next swiper-button-next swiper-button-white"></div>
				<div class="${module}ImagesTab-zoom-swiper-button-prev swiper-button-prev swiper-button-white"></div>
			</div>
		`;
		$('body').append(fullscreenGalTemplate);

		//distribute images
		sliderCounter = -1;
		$.each(data.Destination_gallery, function (index, value) {
			sliderCounter++;
			//thumb slider
			var ImageID = value.path;
			var ImageWidth = 380;
			var ImageHeight = 285;
			var thumbItem = /*html*/ `
				<div class="swiper-slide" id="${module}ImagesTab-panel">
					<div class="zoom d-flex justify-content-center align-items-center" data-slide="${sliderCounter}">
						<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/${ImageID}&w=${ImageWidth}&h=${ImageHeight}&o=1&m=thumbnail&q=80" class="img-fluid ${module}image${index} w-100">
						<i class="fal fa-search-plus fa-2x text-white position-absolute zoom-icon"></i>
					</div>
				</div>
			`;
			$(`.${module}destinationGallery-swiper-wrapper`).append(thumbItem);

			//fullscreen gallery populate
			var galleryImageZoomID = value.path;
			var galleryImageZoomWidth = '';
			var galleryImageZoomHeight = 768;
			var thumb = `${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/${galleryImageZoomID}&w=${galleryImageZoomWidth}&h=${galleryImageZoomHeight}&o=1&m=thumbnail&q=80" class="m-auto img-fluid ${module}image-zoom-slide${index}`;
			var fullscreenImage = /*html*/ `
				<div class="swiper-slide d-flex">
					<img src="${thumb}" id="image-zoom-slide${index}">
				</div>
			`;
			$('.' + module + 'ImagesTab-zoom-swiper-wrapper').append(fullscreenImage);

		}); //each
		//activate fullscreen gallery
		setTimeout(function () {
			activateSwiperAutoplayFunc(module + 'destinationGallery', 4, 30, 7000);
			activateSwiperImagesGalleryZoomFunc(module + 'ImagesTab', module + 'Gal');
		}, 1000);
		//#endregion

		//#region Programme
		var ProgrammeTop = data.Programme_top;
		var ProgrammeHeading = data.Programme_heading;
		var ProgrammeBody = data.Programme_body;
		var ProgrammeBodyPart2 = data.Programme_body_part2;
		var ProgrammeVideoBtn = data.Programme_video_btn;
		var ProgrammeVideoID = data.Programme_video_id;
	
		var ProgrammeContent = /*html*/`
			<div class="text-center"><div class="cDisplay-4 text-secondary border-top border-bottom d-block d-md-inline py-2">${ProgrammeTop}</div></div>
			<div class="cDisplay-1 mt-3 mb-5 text-center">${ProgrammeHeading}</div>
			<div id="CongressTables">${ProgrammeBody}</div>
		`;
		$(`#${module}programme`).append(ProgrammeContent);
		

		var fullscreenGalProgrammeTemplate = /*html*/ `
			<div class="${module}ProgrammeImagesTab-zoom-swiper-container swiper-container full-screen">
				<span class="close">×</span>
				<div class="${module}ProgrammeImagesTab-zoom-swiper-wrapper swiper-wrapper h-100"></div>
				<div class="${module}ProgrammeImagesTab-zoom-swiper-button-next swiper-button-next swiper-button-white"></div>
				<div class="${module}ProgrammeImagesTab-zoom-swiper-button-prev swiper-button-prev swiper-button-white"></div>
			</div>
		`;
		$('body').append(fullscreenGalProgrammeTemplate);

		//distribute images
		sliderCounter = -1;
		$.each(data.Programme_gallery, function (index, value) {
			sliderCounter++;
			//thumb slider
			var ImageID = value.path;
			var ImageWidth = 380;
			var ImageHeight = 285;
			var thumbItem = /*html*/ `
				<div class="swiper-slide" id="${module}ProgrammeImagesTab-panel">
					<div class="zoom d-flex justify-content-center align-items-center" data-slide="${sliderCounter}">
						<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/${ImageID}&w=${ImageWidth}&h=${ImageHeight}&o=1&m=thumbnail&q=80" class="img-fluid ${module}image${index} w-100">
						<i class="fal fa-search-plus fa-2x text-white position-absolute zoom-icon"></i>
					</div>
				</div>
			`;
			$(`.${module}programmeGallery-swiper-wrapper`).append(thumbItem);

			//fullscreen gallery populate
			var galleryImageZoomID = value.path;
			var galleryImageZoomWidth = '';
			var galleryImageZoomHeight = 768;
			var thumb = `${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/${galleryImageZoomID}&w=${galleryImageZoomWidth}&h=${galleryImageZoomHeight}&o=1&m=thumbnail&q=80" class="m-auto img-fluid ${module}image-zoom-slide${index}`;
			var fullscreenImage = /*html*/ `
				<div class="swiper-slide d-flex">
					<img src="${thumb}" id="image-zoom-slide${index}">
				</div>
			`;
			$('.' + module + 'ProgrammeImagesTab-zoom-swiper-wrapper').append(fullscreenImage);

		}); //each
		//activate fullscreen gallery
		setTimeout(function () {
			activateSwiperAutoplayFunc(module + 'programmeGallery', 4, 30, 7000);
			activateSwiperImagesGalleryZoomFunc(module + 'ProgrammeImagesTab', module + 'GalP');
		}, 1000);


		var ProgrammeContentP2 = /*html*/`
			<div class="">${ProgrammeBodyPart2}</div>
			<div class="pt-4 text-center">
				<a href="#${module}VideoModalProg" id="${module}PlayVideoProg" class="btn btn-primary position-relative pr-5" data-toggle="modal" data-target="#${module}VideoModalProg" aria-label="Play video" class="btn btn-primary position-relative">
					${ProgrammeVideoBtn} <i class="fal fa-arrow-circle-right position-absolute"></i>
				</a>
			</div>
		`;
		$(`#${module}programmeP2`).append(ProgrammeContentP2);

		
		videoModalProg = /*html*/ `
			<div class="modal fade" id="${module}VideoModalProg" tabindex="-1" role="dialog" aria-labelledby="VideoTitleIdProg" aria-hidden="true">
				<div class="modal-dialog modal-xl border-0" role="document">
					<div class="modal-content bg-transparent border-0 shadow-none">
						<div class="modal-body p-0">
							<button type="button" class="close text-white h2" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<div class="position-absolute w-100 h-100 mt-4" style="z-index: 1000"></div>
							<div class="embed-responsive embed-responsive-16by9">
								<div class="embed-responsive-item" id="${module}ytplayerProg"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		`;
		$("body").append(videoModalProg);

		$('#' + module + 'PlayVideoProg').on('click', function (e) {
			e.preventDefault();
			var playerVars = {
				height: '450',
				width: '850',
				modestbranding: 1,
				autoplay: 1,
				loop: 1,
				rel: 0,
				showinfo: 0,
				controls: 0, 
				disablekb: 1,
				iv_load_policy: 3
			};
			triggerYoutubeVideoModal(ProgrammeVideoID, playerVars, module + 'ytplayerProg');
		});

		//#endregion
		
		//#region Speakers
		var SpeakersTop = data.Speakers_top;
		var SpeakersHeading = data.Speakers_heading;

		var SpeakersContentHeading = /*html*/`
			<div class="cDisplay-4 border-top border-bottom d-block d-md-inline py-2">${SpeakersTop}</div>
			<div class="cDisplay-1 mt-3 mb-5 text-center">${SpeakersHeading}</div>
		`;
		$(`#${module}speakersHeading`).append(SpeakersContentHeading);
		$.each(data.Speakers, function(index, value){
			var Name = value.value.Name;
			// var Identifier = value.value.Name.replace(/\s/g, '');
			var Identifier = value.value.Name.replace(/[^a-z0-9]/gi, '_').toLowerCase();
			var Image = value.value.Speaker_image.path;
			var CVbtn = value.value.Btn_txt;
			var CVtext = value.value.CV;
            var SpeakerItem = /*html*/ `
                <div class="col-lg-3 col-sm-6 pb-3">
					<div class="card text-center border-0">
						<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=676&o=1&m=thumbnail&q=80" class="card-img-top img-fluid" alt="${Name}">
						<div class="card-body">
							<h5 class="card-title font-weight-normal mb-0">${Name}</h5>
							<a class="small text-uppercase text-primary" data-toggle="modal" data-target="#${Identifier}SPKRModal"href="#${Identifier}SPKRModal">${CVbtn}</button>
						</div>
					</div>	
				</div>
            `;
			$(`#${module}speakers`).append(SpeakerItem);
			
			var SpeakerModal = /*html*/ `
				<div class="modal fade" id="${Identifier}SPKRModal" tabindex="-1" role="dialog" aria-labelledby="${Identifier}spkrTitle" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<div class="mx-auto pb-5"><img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=676&o=1&m=thumbnail&q=80" class="img-fluid" alt="${Name}"></div>
								${CVtext}
							</div>
						</div>
					</div>
				</div>
			`;
			$('body').append(SpeakerModal);
		});
		//#end Speakers

		//#region Partners
		var SponsorsTop = data.Sponsors_top;
		var SponsorsHeading = data.Sponsors_heading;

		var SponsorsContentHeading = /*html*/`
			<div class="cDisplay-4 text-secondary border-top border-bottom d-block d-md-inline py-2">${SponsorsTop}</div>
			<div class="cDisplay-1 mt-3 mb-5 text-center">${SponsorsHeading}</div>
		`;
		$(`#${module}sponsorsHeading`).append(SponsorsContentHeading);
		

		$.each(data.Sponsors_packages, function(index, value){
			var Image = value.path;
			var Alt = value.meta.title;
            var SponsorItem = /*html*/ `
                <div class="col-lg-2 col-md-3 col-sm-4 col-6 pb-3">
					<img src="${streamSRV}${Image}" class="img-fluid" alt="${Alt}">
				</div>
            `;
            $(`#${module}sponsors`).append(SponsorItem);
        });
		//#endregion

		//#region Contacts
		var ContactsTop = data.Contacts_top;
		var ContactsHeading = data.Contacts_heading;

		var ContactsContentHeading = /*html*/`
			<div class="cDisplay-4 border-top border-bottom d-block d-md-inline py-2">${ContactsTop}</div>
			<div class="cDisplay-1 mt-3 mb-5 text-center">${ContactsHeading}</div>
		`;
		$(`#${module}contactsHeading`).append(ContactsContentHeading);
		
		//form
		var formTemplate = /*html*/ `
			<form class="pb-5 text-left" id="${module}Form">
				<div class="input-group input-group-sm mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text px-3 py-2 border-0 bg-white"><i class="fal fa-user"></i></span>
					</div>
					<input name="Name" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Name" type="text" placeholder="${translationArray['form_name' + language]}">
					<label for="${module}Name" class="sr-only">${translationArray['form_name']}</label>
				</div>
				<div class="input-group input-group-sm mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text px-3 py-2 border-0 bg-white"><i class="fal fa-envelope"></i></span>
					</div>
					<input name="Email" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Email" type="text" placeholder="${translationArray['form_email' + language]}">
					<label for="${module}Email" class="sr-only">${translationArray['form_email']}</label>
				</div>
				<div class="input-group input-group-sm mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text px-3 py-2 border-0 bg-white"><i class="fal fa-align-justify"></i></span>
					</div>
					<textarea name="Message" class="form-control shadow-none rounded-0 border-left border-primary" id="${module}Message" type="textarea" rows="5" placeholder="${translationArray['form_message']}"></textarea>
					<label for="${module}Message" class="sr-only">${translationArray['form_message']}</label>
				</div>
				<div class="text-center"><button type="submit" class="btn btn-primary position-relative pr-5" id="${module}SendBtn">${translationArray['send_button']} <i class="fal fa-arrow-circle-right position-absolute"></i></button></div>
				<input type="hidden" name="Page" value="Contacts">
			</form>
			<div class="modal fade" tabindex="-1" role="dialog" id="thankyouModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<div class="m-auto text-center lead pt-4" id="${module}ThankYouMessage">${translationArray['thankyou_form']}</div>
						</div>
					</div>
				</div>
			</div>	
		`;
		$('#' + module + 'contactsForm').append(formTemplate);
		// setTimeout(function () {
		// 	$('#' + module + 'Name').attr('placeholder', translationArray['form_name' + language]).next().append(translationArray['your_name' + language]);
		// 	$('#' + module + 'Email').attr('placeholder', translationArray['form_email' + language]).next().append(translationArray['your_email' + language]);
		// 	$('#' + module + 'Message').attr('placeholder', translationArray['form_message' + language]).next().append(translationArray['your_message' + language]);
		// 	$('#' + module + 'SendBtn').append(translationArray['send_button' + language]);
		// 	$('#' + module + 'ThankYouMessage').append(translationArray['thankyou_form' + language]);
		// }, 700);


		$.validator.setDefaults({
			submitHandler: function () {
				const FormArray = $('#' + module + 'Form').serializeArray();
				console.log(FormArray);
				var formName = 'Contact';
				sendFormFunc(formName, FormArray);
				$('#' + module + 'Form').trigger("reset");
				$('#thankyouModal').modal('show');
				setTimeout(function () {
					$('#thankyouModal').modal('hide');
				}, 5000);
			}
		});
		$('#' + module + 'Form').validate({
			rules: {
				Name: "required",
				Email: {
					required: true,
					email: true
				},
				Message: "required"
			},
			messages: {
				Name: translationArray['invalid_name' + language],
				Email: translationArray['invalid_email' + language],
				Message: translationArray['invalid_message' + language]
			},
			errorElement: "div",
			errorPlacement: function (error, element) {
				// Add the `help-block` class to the error element
				error.addClass("invalid-feedback");
				if (element.prop("type") === "checkbox") {
					error.insertAfter(element.parent("label"));
				} else {
					error.insertAfter(element);
				}
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass("is-invalid").removeClass("is-valid");
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).addClass("is-valid").removeClass("is-invalid");
			}
		});
		//#endForm

		//data is populated from HFSection (main singelton)
		var ContactsAddressContent = /*html*/`
			<div class="col-md-4 col-12 pb-3 text-center">
				<i class="fal fa-envelope text-primary fa-2x"></i>
				<h5 class="font-weight-normal pt-3">${translationArray['contact_email']}</h5>
				<div class="email-container text-primary"></div>
			</div>
			<div class="col-md-4 col-12 pb-3 text-center">
				<i class="fal fa-location-arrow text-primary fa-2x"></i>
				<h5 class="font-weight-normal pt-3">${translationArray['event_location']}</h5>
				<div class="location-container text-primary"></div>
			</div>
			<div class="col-md-4 col-12 pb-3 text-center">
				<i class="fal fa-map-marked-alt text-primary fa-2x"></i>
				<h5 class="font-weight-normal pt-3">${translationArray['contact_view_locations']}</h5>
				<div class="address-container text-primary"></div>
			</div>
		`;
		$(`#${module}contactsAddress`).append(ContactsAddressContent);
		//#end region
	})
	.then(function(){
		// Add smooth scrolling to all links
		$("nav a").on('click', function(event) {
			// Make sure this.hash has a value before overriding default behavior
			if (this.hash !== "") {
				// Prevent default anchor click behavior
				event.preventDefault();
			
				// Store hash
				var hash = this.hash;
			
				// Using jQuery's animate() method to add smooth page scroll
				// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
				$('html, body').animate({
				scrollTop: $(hash).offset().top
				}, 800, function(){
			
					// Add hash (#) to URL when done scrolling (default click behavior)
					window.location.hash = hash;
				});
			} // End if
		});
		$('#navbarLogo').on('click', function(){
			$('html, body').animate({scrollTop: 0}, 800);
		});
	});
}
