function cookieLoadContent() {
	listingPage = true;
	module = "cookie";
	var mainTemplate = /*html*/ `
		<div id="introduction" >
			<div class="container py-5">
				<div class="row pt-5">
					<div class="col-12" id="bodyText"></div>
				</div>
			</div>
		</div>
		
    `;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + module + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			//#region Introduction
			let bodyText = data['Text' + language];
			$('#bodyText').append(bodyText);
			//#endregion Introduction
			

		});
}