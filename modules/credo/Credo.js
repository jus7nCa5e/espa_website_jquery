function credoLoadContent() {
	listingPage = true;
	module = "credo";
	var mainTemplate = /*html*/ `
		<div class="container" id="sectionDataContainer"></div>
    `;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + module + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			console.log(data);
			
			//#region Section data
			for (let value of data['Sections_data' + language]) {
				
				let sectionTemaplate = /*html*/ `
					<div class="container pt-0 pt-md-3 pt-lg-5 credo-spa-resort">
						<div class="row pt-5 pb-4 pb-sm-5">
							<div class="col-12">
								<h2 class="section-heading"><span>${value.value.Title}</span></h2>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								${value.value.Text}
							</div>
						</div>
					</div>
				`;
				$('#sectionDataContainer').append(sectionTemaplate);
			}
			
			//#endregion Section data

			


		});
}