function glossaryLoadContent() {
	listingPage = true;
	module = "glossary";
	var mainTemplate = /*html*/ `
		<div class="container py-5" id="sectionDataContainer">
			<div class="row py-5">
				<div class="col-12">
					<ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
					</ul>
					<div class="tab-content" id="pills-tabContent">
					</div>
				</div>
			</div>
		</div>
	`;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + module + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			console.log(data);
			
			//#region Section data
			for (let [ind, val] of data['Sections_data' + language].entries()) {
				let pillTemaplate = /*html*/ `
					<li class="nav-item">
						<a class="nav-link serif-font h3" id="pills-${ind}-tab" data-toggle="pill" href="#pills-${ind}-panel" role="tab" aria-controls="pills-${ind}-panel" aria-selected="true">${val.value.Title}</a>
					</li>
				`;
				$('#pills-tab').append(pillTemaplate);
				let panelTemaplate = /*html*/ `
					<div class="tab-pane fade" id="pills-${ind}-panel" role="tabpanel" aria-labelledby="pills-${ind}-tab">${val.value.Text}</div>
				`;
				$('#pills-tabContent').append(panelTemaplate);
			}
			
			//#endregion Section data

			


		})
		.then(function(){
			$('#pills-tab li:first-child a').tab('show');
		});
}