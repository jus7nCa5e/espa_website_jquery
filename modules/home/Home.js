videos = [];
var isVideo = false;
var myLatLng;
function callMap() {
    setTimeout(function () {
        initMap();
    }, 1000);
}

function homeLoadContent() {
	$('#cover').addClass('d-none');
	$('#shareContainer').addClass('d-none');

	var module = "home";
	listing = false;
	var mainTemplate = /*html*/ `
        <div class="${module}-cover-swiper-container swiper-container pоsition-relative" id="${module}PageSlider">
			<div class="${module}-cover-swiper-wrapper swiper-wrapper"></div>
			<div class="${module}-cover-swiper-button-prev swiper-button-prev swiper-button-white"></div>
			<div class="${module}-cover-swiper-button-next swiper-button-next swiper-button-white"></div>
		</div>
		<div id="introduction" >
			<div class="container pt-0 pt-lg-5">
				<div class="row pb-3 pb-lg-5">
					<div class="col-12" id="introductionBody"></div>
				</div>
				<div class="row py-5" id="threePanels">
				</div>
			</div>
		</div>
		<div id="membersData">
			<div class="container pb-4 pb-md-5">
				<div class="row pt-5 pb-3 pb-xl-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_members' + language]}</span></h2>
					</div>
				</div>
				<div class="row pt-4 pt-md-5 px-5 px-sm-3">
					<div class="col-md-3">
						<ul class="list-unstyled" id="membersList"></ul>
					</div>
					<div class="col-md-9">
						<div id="map-europe">
							<ul class="europe">
								<!-- <li class="eu1"><a href="#albania">Albania</a></li> -->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="newsData">
			<div class="container">
				<div class="row pt-5 pb-3 pb-xl-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_latestnews' + language]}</span></h2>
					</div>
				</div>
				<div class="row pb-xl-5">
					<div class="col-12">
						<ul class="list-group list-group-flush" id="newsListing"></ul>
					</div>
				</div>
			</div>
		</div>
		<div id="eventsData">
			<div class="container">
				<div class="row pt-5 pb-3 pb-xl-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_latestevents' + language]}</span></h2>
					</div>
				</div>
			</div>
			<div class="container-fluid no-gutters px-0">
				<div class="row no-gutters" id="eventsListing">
				</div>
			</div>
		</div>
		<div id="secitonData">
			<div class="container py-0 py-md-4 py-lg-5">
				<div class="row py-5" id="sectionDataContainer">
				</div>
			</div>
		</div>
    `;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + module + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			//#region slider
			for (var [index, value] of data["Slider" + language].entries()) {

				var SliderTitle = value.value.title ? `<h2 class="text-white display-4">${value.value.title}</h2>` : "";
				var SliderDescription = value.value.description ? `<p class="text-white lead py-sm-3">${value.value.description}</p>` : "";

				var Button = "";
				if (value.value.btntxt && value.value.btnlink) {
					Button = /*html*/ `<a href="${value.value.btnlink}" class="btn-outline-light btn mt-sm-3">${value.value.btntxt}</a>`;
				}
				//if video - play video, else - slider
				if (value.value.videoID) {
					isVideo = true;
					var videoSlide = /*html*/ `
						<div class="swiper-slide home-slide ${module}slider-image${index} bgVideo" data-swiper-autoplay="${(value.value.duration) * 1000}">
							<div class="position-absolute w-100 video-overlay"></div> <!--//empty overlay to block video controls-->
							<div class=" iframe-youtube" id="${module}ytplayer${index}">
						</div>
					`;
					$(`.${module}-cover-swiper-wrapper`).append(videoSlide);

					videos.push(value.value.videoID);

					playerVars = {
						height: '450',
						width: '850',
						modestbranding: 1,
						rel: 0,
						showinfo: 0,
						controls: 0,
						enablejsapi: 1,
						origin: '//rosslyn-hotels.com',
						fs: 0,
						loop: 1
					};
				} else { //is image
					var Image = value.value.Image.path;
					var ImageWidth = 1920;
					var ImageHeight = 1080;
					var imageSlide = /*html*/ `
						<div class="swiper-lazy home-slide swiper-slide ${module}slider-image${index}" data-swiper-autoplay="${value.value.duration * 1000}" style="background-image: url('${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=${ImageWidth}&h=${ImageHeight}&o=1&m=thumbnail&q=80')"></div>
					`;
					$(`.${module}-cover-swiper-wrapper`).append(imageSlide);
				}
				if (SliderTitle || SliderDescription || Button) {
					var Panel = /*html*/ `
							<div class="container d-flex">
								<div class="row w-100" style="z-index: 10;">
										<div class="col-12 my-auto text-center" >
												<div id="${module}Jumbotron_${index}" class="">
														${SliderTitle}
														${SliderDescription}
														${Button}
												</div>
										</div>
								</div>
							</div>
						`;
					$(`.${module}slider-image${index}`).append(Panel);
				}
			} //end for-of
			if (isVideo) {
				triggerYoutubeVideoSlider(playerVars);
			} //is video

			setTimeout(function () {
				activateSwiperFunc(module + "-cover");
				mySwiper.params.autoplay.disableOnInteraction = false;
				mySwiper.params.preloadImages = false;
				// mySwiper.params.Lazy = true;
				mySwiper.params.loop = false;
				mySwiper.params.effect = 'fade';
				mySwiper.init();
				mySwiper.autoplay.start();
			}, 200);

			//#endregion Slider
			// console.log(data);

			//#region Intro
			let introTemplate = /*html*/ `
				<h1 class="pt-5 pb-3 pb-xl-5 text-center text-primary display-4">${data.Title}</h1>
				${data.Intro_text}
			`;
			$('#introductionBody').append(introTemplate);

			//#endregion Intro
			//#region Panels
			for (let panel of data.Panels) {
				let panelTemplate = /*html*/ `
					<div class="col-xl-4 text-center">
						<a class="text-decoration-none" href="${urlLanguage}/${panel.value.Link}">
							<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${panel.value.Image.path}&w=64&h64&o=1&m=thumbnail&q=80" class="" alt="${panel.value.Title}">
							<h3 class="text-secondary serif-font py-3">${panel.value.Title}</h3>
						</a>
						${panel.value.Text}
					</div>
				`;
				$('#threePanels').append(panelTemplate);
			}
			//#endregion Panels
			//#region Map
			var activeCountries = [];
			fetch(getURL + 'members' + token + '&simple=1&')
			.then (function(response) {
				return response.json();
			})
			.then (function (data){
				for (let value of data) {
				
					var memberemplate = /*html*/ `
						<li class="mb-2">
							<a href="${urlLanguage}/members/${value.Alias_slug}" class="media  text-decoration-none">
								<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${value.Flag.path}&w=30&h=30&o=1&m=thumbnail&q=80" class="mr-3 rounded-circle" alt="${value.Title}">
								<div class="media-body align-self-center">
									<span class="my-0 text-primary serif-font">${value.Title}</span>
								</div>
							</a>
						</li>
					`;
					$('#membersList').append(memberemplate);

					var mapTemplate = /*html*/ `
						<li class="${value.Map_code}"><a href="${urlLanguage}/members/${value.Alias_slug}">${value.Title}</a></li>
					`;
					$('.europe').append(mapTemplate);
					activeCountries.push(value.Map_code);
				} //for

			}).then(function(){
				// CSSMap;
				$("#map-europe").CSSMap({
					"size": 1280,
					"tooltips": "floating-top-center",
					"responsive": "auto",
					"tapOnce": true,
					"activateOnLoad": activeCountries,
					"mapStyle": "custom"
				});
				// END OF THE CSSMap;
				
			});
			//#endregion Map
			//#region News
			fetch(getURL + 'news' + token + '&simple=1&filter[Category]=news&sort[Date]=-1&limit=3')
                .then(function(response) {
                    $('#loader').addClass('d-flex').removeClass('d-none');
                    return response.json();
                })
                .then(function(data) {
                    //#region fetch 
                    for (let value of data) {

						let newsListingTemplate = /*html*/ `
							<li class="list-group-item">
								<div class="row">
									<div class="py-3 py-xl-0 col-xl-3 d-flex justify-content-center align-items-center">
										<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}${value.Gallery[0].path}&w=512&h=288&o=1&m=thumbnail&q=80" class="img-fluid" alt="${value.Title}">
									</div>
									<div class="col-xl-9">
										<a class="text-dark stretched-link" href="${urlLanguage}/news-events/news/${value.Alias_slug}"><h3 class="serif-font">${value.Title}</h3></a>
										<p class="mb-2">${value.Intro_text}</p>
										<small>${newsDate(value.Date)}</small>
									</div>
								</div>
							</li>
						`;
						$(`#newsListing`).append(newsListingTemplate);
                        

                    }
                    //#endregion fetch  
                });
			//#endregion News
			//#region Events
			fetch(getURL + 'news' + token + '&simple=1&filter[Category]=events&sort[Date]=-1&limit=3')
                .then(function(response) {
                    $('#loader').addClass('d-flex').removeClass('d-none');
                    return response.json();
                })
                .then(function(data) {
                    //#region fetch 
                    for (let value of data) {
						let eventsListingTemplate = /*html*/ `
							<div class="col-lg-4">
								<div class="card bg-dark text-white border-0">
									<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}${value.Gallery[0].path}&w=850&h=478&o=1&m=thumbnail&q=80" class="card-img" alt="${value.Title}">
										<div class="card-img-overlay">
											<h3 class="card-title serif-font shadow-dark">${value.Title}</h3>
											<p class="card-text shadow-dark">${value.Intro_text}</p>
											<small class="shadow-dark">${newsDate(value.Date)}</small>
											<div class="pt-4 card-btn">
												<a class="btn btn-secondary stretched-link" href="${urlLanguage}/news-events/events/${value.Alias_slug}">${translationArray['read_more' + language]}</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						`;
					$(`#eventsListing`).append(eventsListingTemplate);
                        

                    }
                    //#endregion fetch  
                });
			//#endregion Events
			//#region Borchures
			for (let val of data.Brochures) {
				// console.log(val)
				let Image = val.value.Image.path;
				let button = '';
				if (val.value.Button_text) {
					button = /*html*/ `<a href="${streamSRV}/storage/uploads${val.value.Document.path}" class="btn btn-secondary" target="_blank">${val.value.Button_text}</a> `;
				}
				let brochureTemplate = /*html*/ `
					<div class="col-12 col-md-6">
						<div class="card mb-3 border-0" >
							<div class="row no-gutters">
								<div class="col-4 col-lg-3 pt-3 align-self-center">
									<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=1250&o=1&m=thumbnail&q=80" class="card-img img-fluid" alt="${val.value.Title}">
								</div>
								<div class="col-8 col-lg-9">
									<div class="card-body">
										<h3 class="serif-font">${val.value.Title}</h3>
										${val.value.Text}
										${button}
									</div>
								</div>
							</div>
						</div>
					</div>
					`;
				$(`#sectionDataContainer`).append(brochureTemplate);
			}
			//#endregion Brochures	
		});
}

//init map
function initMap() {
        
    // Create a map object and specify the DOM element for display.
    var map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        zoom: 18
    });
    // Create a marker and set its position.
    var logoPin = mapPin;
    var marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
        animation: google.maps.Animation.DROP,
        icon: logoPin
    });
    var styles = {
        default: null,
        hide: [
            {
                featureType: 'poi.business',
                stylers: [{visibility: 'off'}]
            },
            {
                featureType: 'transit',
                elementType: 'labels.icon',
                stylers: [{visibility: 'off'}]
            }
        ]
    };
    map.setOptions({styles: styles['default']});
}
function newsDate(date){
	var mydate = new Date(date);

	var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][mydate.getMonth()];
	var str = month + ' ' + mydate.getDate() + ',' + mydate.getFullYear();
	return str;
}