
module = 'members';
moduleAdvisory = 'advisorymembers';

function membersLoadContent() {
    listingPage = true;
    var membersPageTemaplte = /*html*/ `
        <div class="container pt-5" id="map">
            <div class="row pt-5">
                <div class="col-xl-10 offset-xl-1">
                    <div id="map-europe">
                        <ul class="europe">
                            <!-- <li class="eu1"><a href="#albania">Albania</a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pt-5">
            <div class="row">
                <div class="col-12">
                    <h2 class="section-heading"><span>${translationArray['title_members' + language]}</span></h2>
                </div>
            </div>
            <div class="row pb-5 pt-4 pt-md-5"  id="${module}Container">

            </div>
            <div class="row pt-5" id="advisoryHeading">
              
            </div>
            <div class="row pb-5 pt-4 pt-md-5 align-items-end"  id="${moduleAdvisory}Container">

            </div>
        </div>
    `;
    $('header').after(membersPageTemaplte);

    var activeCountries = [];
    fetch(getURL + module + token + '&simple=1&')
    .then (function(response) {
        return response.json();
    })
    .then (function (data){
        for (let value of data) {
            // console.log(data);
            var memberemplate = /*html*/ `
                <div class="col-lg-4 col-xl-3 py-2">
                        <a href="${window.location.pathname}/${value.Alias_slug}" class="media  text-decoration-none">
                            <img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${value.Flag.path}&w=64&h=64&o=1&m=thumbnail&q=80" class="m-3 rounded-circle" alt="${value.Title}">
                            <div class="media-body align-self-center">
                                <h5 class="my-0 text-primary serif-font">${value.Title}</h5>
                            </div>
                        </a>
                    </div>    
            `;
            $('#'+module+'Container').append(memberemplate);

            var mapTemplate = /*html*/ `
                <li class="${value.Map_code}"><a href="${window.location.pathname}/${value.Alias_slug}">${value.Title}</a></li>
            `;
            $('.europe').append(mapTemplate);
            activeCountries.push(value.Map_code);
        } //for
        
    }).then(function(){
        // CSSMap;
        $("#map-europe").CSSMap({
            "size": 1280,
            "tooltips": "floating-top-center",
            "responsive": "auto",
            "tapOnce": true,
            "activateOnLoad": activeCountries,
            "mapStyle": "custom"
        });
        // END OF THE CSSMap;
        
    });

    fetch(getURL + moduleAdvisory + token + '&simple=1&')
    .then (function(response) {
        return response.json();
    })
    .then (function (data){
        if (data.length > 0 ){
            var advheadingtemplate = /*html*/ `
                <div class="col-12">
                    <h2 class="section-heading">
                        <span>${translationArray['title_advisory_members' + language]}</span>
                    </h2>   
                </div>
            `;
            $('#advisoryHeading').append(advheadingtemplate);

            for (let value of data) {
                var advmemberemplate = /*html*/ `
                    <div class="col-sm-12 col-lg-6 col-xl-4 pb-5">
                        <a href="${baseUrl}${urlLanguage}/advisory-members/${value.Alias_slug}" class="media text-decoration-none flex-column align-items-center">
                            <!-- <img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${value.Logo.path}&w=64&h=64&o=1&m=thumbnail&q=80" class="pb-5 row" alt="${value.Title}"> -->
                            <img src="${streamSRV}/storage/uploads${value.Logo.path}" class="pb-4 row w-50" alt="${value.Title}">
                            <div class="row media-body align-self-center text-center px-5">
                                <h5 class="my-0 text-primary serif-font">${value.Title}</h5>
                            </div>
                        </a>
                    </div>    
                `;
                $('#'+moduleAdvisory+'Container').append(advmemberemplate);
            } //for
        }
    });


}

//inner page
function membersInnerPage() {
    var pageTmplate = /*html*/ `
        <div id="introduction" >
			<div class="container pt-5">
				<div class="row">
					<div class="col-12" id="introductionBody"></div>
				</div>
			</div>
        </div>
        <div id="associations" >
			<div class="container pt-5">
				<div class="row">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_associations' + language]}</span></h2>
					</div>
				</div>
				<div class="row justify-content-center py-5" id="associationsBody"></div>
			</div>
        </div>
        <div id="resort" >
			<div class="container pt-5">
				<div class="row">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_resortindications' + language]}</span></h2>
					</div>
				</div>
				<div id="indicationsColumns" class="row pt-5"></div>
			</div>
        </div>
        <div id="indications" >
			<div class="container pt-5">
				<div class="row">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_mainindications' + language]}</span></h2>
					</div>
				</div>
				<div id="indicationsAllColumns" class="row pt-5"></div>
			</div>
        </div>
        <div id="destinations" >
			<div class="container pt-5">
				<div class="row">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_destinations' + language]}</span></h2>
					</div>
				</div>
				<div id="destinationsAllColumns" class="card-deck py-5"></div>
			</div>
		</div>
    `;
    $('header').after(pageTmplate);
    //get location from URL (explode url)
    var url = window.location.pathname;
    var pageParam = decodeURI(url.substr(url.lastIndexOf('/') + 1));
    
    fetch(getURL + module + token + '&simple=1&populate=1&filter[Alias_slug]='+pageParam)
    .then (function(response) {
        return response.json();
    })
    .then (function (data){
        // console.log(data);
        value = data[0];

       
        //populate cover image 
        let Cover = value.Cover_image.path;
        setTimeout(function(){
            $('#cover').css("background-image", `url('${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Cover}&h=400&o=1&m=thumbnail&q=80')`);
            $('#coverTitle').append(`<h1 class="display-2 light text-white">${value['Title'+ language]}</h1>`);
        }, 500);
        

        $('#introductionBody').append(value['Destinations_text' + language]);

        //asscoiations
        if (value.Associations) {
            for (let val of value.Associations) {
                var associationTemplate = /*html*/ `
                    <div class=" col-xl col-lg-4 col-md-3 col-sm-6 col-12 text-center">
                        <a href="${val.value.Link}" target="_blank">
                            <img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${val.value.Image.path}&h=160&o=1&m=thumbnail&q=80" class="img-fluid" alt="${val.value.Title}">
                            <h4 class="pt-3">${val.value.Title}</h4>
                        </a>
                    </div>
                `;
                $('#associationsBody').append(associationTemplate);
            }
        }
        //Indications data
        if (value.Indications_data) {
            var indicationsData = value['Indications_data' + language];
            
            for (let val of indicationsData) {
                let Image = val.value.Image.path;
                let item = /*html*/ `
                    <div class="col-lg-6 col-12">
                        <div class="card mb-3 border-0" >
                            <div class="row no-gutters">
                                <div class="col-4 col-lg-2 col-xl-1 pt-3">
                                    <img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=64&h=64&o=1&m=thumbnail&q=80" class="card-img img-fluid" alt="${val.value.Title}">
                                </div>
                                <div class="col-8 col-lg-10 col-xl-11">
                                    <div class="card-body">
                                        <h4>${val.value.Title}</h4>
                                        ${val.value.Text}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `;
                $('#indicationsColumns').append(item);
            }
        }

        //Main Indications section
        if(value.Destinations.length > 0) { 
            fetch(getURL + 'indications' + token + '&simple=1&populate=1')
            .then (function(responseInd) {
                return responseInd.json();
            })
            .then (function (dataInd){
                for (let valInd of dataInd) {
                    let item = /*html*/ `
                        <div class="col-lg-6 col-12 d-none" id="item${valInd.Alias_slug}">
                            <h4>${valInd['Title' + language]}</h4>
                            <ul class="list-inline" id="${valInd.Alias_slug}"></ul>
                        </div>
                    `;
                    $('#indicationsAllColumns').append(item);
                }
            }).then(function(){
                for (let [index, hotels] of value.Destinations.entries()) {
                    //populate indications
                    for (let indic of hotels.Indication) {
                        $(`#item${indic.display}`).removeClass('d-none');
                        $(`#${indic.display}`).append(`<li class="list-inline-item"><a href="${hotels.Link}" class="small" target="_blank" >${hotels.Title}</a></li>`);
                    }

                    let Address = hotels.Address ? `<li class=""><a href="${hotels.Google_maps}" target="_blank"><i class="far fa-map-marker-alt"></i> &nbsp;${hotels.Address}</a></li>` : '';
                    let Phone = hotels.Phone ? `<li class=""><a href="tel:${hotels.Phone}" target="_blank"><i class="far fa-phone"></i> &nbsp;${hotels.Phone}</a></li>` : '';
                    let Email = hotels.Email ? `<li class=""><a href="mailto:${hotels.Email}" target="_blank"><i class="far fa-envelope"></i> &nbsp;${hotels.Email}</a></li>` : '';

                    //populate hotels as destinations
                    var hotelTemplate = /*html*/ `
                        <div class="card pb-3">
                            <img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${hotels.Image.path}&w=512&h=384&o=1&m=thumbnail&q=80" class="card-img-top" alt="${hotels.Title}">
                            <div class="card-body">
                                <h5 class="card-title serif-font mb-0">${hotels.Title}</h5>
                                <div class="category-index-${index} pb-3"></div>
                                <ul class="list-unstyled">
                                    ${Address}
                                    ${Phone}
                                    ${Email}
                                </ul>
                                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                            </div>
                            <div class="card-footer bg-transparent border-0 pt-0">
                                <div class="text-right"><a href="${hotels.Link}" class="btn btn-primary" target="_blank">${translationArray['view_hotel' + language]}</a></div>
                            </div>
                        </div>
                    `;
                    $('#destinationsAllColumns').append(hotelTemplate);
                    // console.log(hotels)
                    for (var i = 0; i < hotels.Category; i++) {
                        $(`.category-index-${index}`).append('<i class="far fa-star fa-sm text-muted"></i>');
                    }
                }
            });
        } else {
            $('#indicationsAllColumns').append('<h2 class="text-center w-100"><em>Information coming soon!</em></h2>');
            $('#destinationsAllColumns').append('<h2 class="text-center w-100"><em>Information coming soon!</em></h2>');
        }
        
    });
}
