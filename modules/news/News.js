function newsLoadContent() {
    listingPage = true;
    module = "news";


    var mainTemplate = /*html*/ `
		<div class="container pt-5">
			<div class="row pt-3 p-md-5">
				<div class="col-12">
				
					<div class="" id="newsListing">
						<div id="loader" class="position-absolute h-100 w-100 bg-white modal mt-2 d-flex align-items-center justify-content-center"><i class="far fa-circle-notch fa-spin fa-4x text-primary"></i></div>
					</div>
					<div class="pt-5" id="${module}Pagination"></div>
				</div>
			</div>
		</div>
    `;
    $("header").after(mainTemplate);


    var url = window.location.pathname;
    // var pageParam = decodeURI(url.substr(url.lastIndexOf('/') + 1));

    drawPage(module);

    function drawPage() {
        var Params = getAllUrlParams(window.location.href);
        // console.log(Params)

        /******* here change limit */
        var Limit = 6;
        // var moduleFetch = getURL + module + token + '&simple=1&filter[$or][][Make]=lg&filter[$or][][Make]=samsung&filter[Energy_eff_heat]=a2&filter[Energy_eff_cool]=a2';
        var moduleFetch = getURL + module + token + '&simple=1&filter[Category]=' + module + '&sort[Date]=-1';
        var fetchURLinit;
        var curPage, prev, next;

        //getting data from URL
        if (jQuery.isEmptyObject(Params) || (Params.page === "1")) {
            fetchURLinit = moduleFetch + '&limit=' + Limit;
        } else {
            var skip;
            curPage = Params.page;
            skip = (curPage * Limit) - Limit;
            fetchURLinit = moduleFetch + '&limit=' + Limit + '&skip=' + skip;
        }

        function buildFetch(currentPage) { //builded fetch url from pagination, without reloading the page
            var skip;
            curPage = currentPage;
            // console.log(curPage)
            if (curPage === 1) {
                fetchURLafter = moduleFetch + '&limit=' + Limit;
            } else {

                skip = (curPage * Limit) - Limit;
                fetchURLafter = moduleFetch + '&limit=' + Limit + '&skip=' + skip;
                console.log(fetchURLafter)
            }
            fetchData(fetchURLafter);
        }


        function fetchData(fetchURL) { //actual fetch function
            fetch(fetchURL)
                .then(function(response) {
                    $('#loader').addClass('d-flex').removeClass('d-none');
                    return response.json();
                })
                .then(function(data) {
                    //#region fetch 
                    $('#newsListing').append(`<ul id="${module}ListItem" class="list-group list-group-flush"></ul>`);

                    // for (let value of data) {   
                    $.each(data, function(index, value){ 
                        // console.log(value.Date.split('-').join(''));
                        let newsListingTemplate = /*html*/ `
							<li class="list-group-item">
								<div class="row">
									<div id="${module}Image${index}" class="py-3 py-xl-0 col-xl-3 d-flex justify-content-center align-items-center">
										
									</div>
									<div class="col-xl-9">
										<a class="text-dark stretched-link" href="${window.location.pathname}/${value.Alias_slug}"><h3 class="serif-font">${value.Title}</h3></a>
										<p class="mb-2">${value.Intro_text}</p>
										<small>${newsDate(value.Date)}</small>
									</div>
								</div>
							</li>
						`;
                        $(`#${module}ListItem`).append(newsListingTemplate);
                        if (!(value.Gallery[0] === undefined)){
                            let newsListingImgTemplate = /*html*/ `
                                <img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}${value.Gallery[0].path}&w=512&h=288&o=1&m=thumbnail&q=80" class="img-fluid" alt="${value.Title}">
                            `;
                            $(`#${module}Image${index}`).append(newsListingImgTemplate);
                        }
                    });
                    //#endregion fetch  
                })
                .then(function() {
                    $('#loader').removeClass('d-flex').addClass('d-none');
                });
        }
        fetchData(fetchURLinit); //initial fetch

        function Pagination() {
            fetch(moduleFetch) //all items fetch
                .then(function(responsePage) {
                    return responsePage.json();
                })
                .then(function(dataPage) {
                    //#region pagination
                    var pages = Math.ceil(dataPage.length / Limit);
                    var PaginationTemplate = /*html*/ `
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center"></ul>
                    </nav>
                `;
                    $(`#${module}Pagination`).append(PaginationTemplate);

                    //populate pages
                    for (let i = 1; i <= pages; i++) {
                        if ((i === parseInt(Params.page)) && (pages > 1)) {
                            $('.pagination').append(`
                            <li class="page-item page-item-action active" aria-current="page">
                                <a class="page-link page-action" data-page=${i} href="#">${i}<span class="sr-only">(current)</span></a>
                            </li>
                    `);
                        } else {
                            $('.pagination').append(`
                            <li class="page-item page-item-action">
                                <a class="page-link page-action" data-page=${i} href="${window.location.pathname}?page=${i}">${i}</a>
                            </li>
                        `);
                        }
                    }
                    //if there's no page in URL
                    if (!Params.page) {
                        $('[data-page="1"]').closest('li.page-item-action').addClass('active');
                    }

                    $('.page-action').on('click', function(e) {
                        e.preventDefault();
                        history.pushState({
                            page: this.dataset.page
                        }, "title " + this.dataset.page, window.location.pathname + "?page=" + this.dataset.page); //change url without reloading page

                        $('#loader').addClass('d-flex').removeClass('d-none');
                        $(`#${module}ListItem`).remove(); //removes items container
                        buildFetch(this.dataset.page); //make new fetch

                        //toggle class active
                        $('.page-item-action').removeClass('active');
                        $(this).closest('li.page-item-action').addClass('active');

                        //check if last or first
                        $('.page-item-prev').removeClass('disabled');
                        $('.page-item-next').removeClass('disabled');

                        // change prev and nex buttons
                        if (parseInt(curPage) === 1) {
                            prev = 1;
                            $('.page-item-prev').addClass('disabled');
                            next = parseInt(curPage) + 1;
                            $('.nextBtn').attr('href', `${window.location.pathname}?page=${next}`);
                        } else if (parseInt(curPage) < pages) {
                            prev = parseInt(curPage) - 1;
                            $('.prevBtn').attr('href', `${window.location.pathname}?page=${prev}`);
                            next = parseInt(curPage) + 1;
                            $('.nextBtn').attr('href', `${window.location.pathname}?page=${next}`);
                        } else if (parseInt(curPage) === pages) {
                            prev = parseInt(curPage) - 1;
                            $('.prevBtn').attr('href', `${window.location.pathname}?page=${prev}`);
                            next = pages;
                            $('.page-item-next').addClass('disabled');
                        }

                    });

                    //prev button
                    if ((!Params.page) || (!curPage)) {
                        curPage = 1;
                        prev = curPage;
                    } else {
                        prev = parseInt(curPage) - 1;
                    }
                    //append prev button
                    if ((parseInt(Params.page) === 1) || (!Params.page)) {
                        $('.pagination').prepend(`
                        <li class="page-item page-item-prev disabled">
                            <a class="page-link prevBtn" href="${window.location.pathname}?page=${prev}"><i class="far fa-chevron-left"></i> ${translationArray.previous}</a>
                        </li>
                    `);
                    } else {
                        $('.pagination').prepend(`
                        <li class="page-item page-item-prev">
                            <a class="page-link prevBtn" href="${window.location.pathname}?page=${prev}"><i class="far fa-chevron-left"></i> ${translationArray.previous}</a>
                        </li>
                    `);
                    }
                    //prev button action
                    $('.prevBtn').on('click', function(e) {
                        e.preventDefault();
                        $('.page-item-prev').removeClass('disabled');
                        $('.page-item-next').removeClass('disabled');
                        prev = parseInt(curPage) - 1;
                        history.pushState({
                            page: prev
                        }, "title " + this.dataset.page, window.location.pathname + "?page=" + prev); //change url without reloading page
                        $('#loader').addClass('d-flex').removeClass('d-none');
                        $(`#${module}ListItem`).remove(); //removes items container
                        buildFetch(prev); //make new fetch

                        //toggle class active
                        $('.page-item-action').removeClass('active');
                        $('[data-page="' + prev + '"]').closest('li.page-item-action').addClass('active');

                        //change button for next click
                        $(this).attr('href', `${window.location.pathname}?page=${prev - 1}`); //change href value
                        if (prev === 1) {
                            $('.page-item-prev').addClass('disabled');
                        }

                        //change next button
                        $('.nextBtn').attr('href', `${window.location.pathname}?page=${curPage + 1}`); //change href value
                    });

                    //next button
                    if (!Params.page) {
                        next = 2;
                    } else {
                        next = parseInt(curPage) + 1;
                    }

                    // append next button
                    // console.log(parseInt(Params.page));
                    if ((parseInt(Params.page) === pages) || pages === 1){
                        $('.pagination').append(`
                        <li class="page-item page-item-next disabled">
                            <a class="page-link nextBtn" href="${window.location.pathname}?page=${next}">${translationArray.next} <i class="far fa-chevron-right"></i></a>
                        </li>
                    `);
                    } else {
                        $('.pagination').append(`
                        <li class="page-item page-item-next">
                            <a class="page-link nextBtn" href="${window.location.pathname}?page=${next}">${translationArray.next} <i class="far fa-chevron-right"></i></a>
                        </li>
                    `);
                    }

                    $('.nextBtn').on('click', function(e) {
                        e.preventDefault();
                        $('.page-item-prev').removeClass('disabled');
                        $('.page-item-next').removeClass('disabled');
                        if (!curPage) {
                            curPage = 1;
                        }
                        next = parseInt(curPage) + 1;
                        history.pushState({
                            page: next
                        }, "title " + this.dataset.page, window.location.pathname + "?page=" + next); //change url without reloading page
                        $('#loader').addClass('d-flex').removeClass('d-none');
                        $(`#${module}ListItem`).remove(); //removes items container
                        buildFetch(next); //make new fetch

                        //toggle class active
                        $('.page-item-action').removeClass('active');
                        $('[data-page="' + next + '"]').closest('li.page-item-action').addClass('active');

                        //change button for next click
                        $(this).attr('href', `${window.location.pathname}?page=${next + 1}`); //change href value
                        if (next === pages) {
                            $('.page-item-next').addClass('disabled');
                        }

                        //change prev button
                        $('.prevBtn').attr('href', `${window.location.pathname}?page=${curPage - 1}`); //change href value
                    });

                    //#endregion pagination
                });
        }
        Pagination();
    }

}

function newsInnerPage() {
	listing = false;
	module = "news";
	$('#cover').addClass('d-none');
    var mainTemplate = /*html*/ `
		<div class="container pt-5" >
			<div class="row pt-5">
                <div class="col-xl-10 offset-xl-1" id="newsImage"></div>
			</div>
        </div>
        <div class="container py-5" >
			<div class="row" id="newsContainer"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="section-heading"><span>${translationArray['title_morenews' + language]}</span></h2>
                </div>
            </div>
            <div class="row py-5">
				<div class="col-12"  id="newsListing"></div>
            </div>
        </div>
    `;
    $("header").after(mainTemplate);

    //get location from URL (explode url)
    var url = window.location.pathname;
    var pageParam = decodeURI(url.substr(url.lastIndexOf('/') + 1));
    var moreItemsURL = url.substr(0, url.lastIndexOf('/'));

    //#region Main data
    fetch(getURL + module + token, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                filter: {
                    "Alias_slug": pageParam
                },
                limit: 1,
                populate: 0,
                simple: 1
            })
        })
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
			let value = data[0];
			if (value.Gallery.length > 1) {
				var swiperContainer = /*html*/ `
					<div class="news-swiper-container swiper-container">
						<div class="news-swiper-wrapper swiper-wrapper h-100"></div>
						<div class="news-swiper-button-next swiper-button-next swiper-buton-white"></div>
						<div class="news-swiper-button-prev swiper-button-prev swiper-buton-white"></div>
					</div>
				`;
				$(`#newsImage`).append(swiperContainer);
				for (let val of value.Gallery) {
					var imageTemplate = /*html*/ `
						<div class="swiper-slide">
							<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}${val.path}&w=1338&h=752&o=1&m=thumbnail&q=80" class="img-fluid">
						</div>		
					`;
					$(`.news-swiper-wrapper`).append(imageTemplate);
				}
			} else {
				$('#newsImage').append(`<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}${value.Gallery[0].path}&w=1338&h=752&o=1&m=thumbnail&q=80" class="img-fluid">`);
			}

			let pageTemplate = /*html*/ `
				<div class="col-12 pt-5">
					<h1>${value['Title' + language]}</h1>
					<h3>${value['Subtitle'+language]}</h3>
					<span>${newsDate(value.Date)}</span>
				</div>
				<div class="pt-5 col-12">
					${value['Full_text' + language]}
				</div>
			`;
			$('#newsContainer').append(pageTemplate);
			var newsMoreArray = [];
			fetch(getURL + module + token + '&simple=1&limit=4&filter[Category]=' + module + '&sort[Date]=-1')
                .then(function(responseMore) {
                    $('#loader').addClass('d-flex').removeClass('d-none');
                    return responseMore.json();
                })
                .then(function(dataMore) {
                    //#region fetch 
					$('#newsListing').append(`<ul id="${module}ListItem" class="list-group list-group-flush"></ul>`);
                    for (let val of dataMore) {
                        if (val.Alias_slug !== pageParam) {
							newsMoreArray.push(val);
						}
					}
                    //#endregion fetch  
                }).then(function(){
					for (let [ind, val] of newsMoreArray.entries()) {
						if (ind <= 2) {
							let newsListingTemplate = /*html*/ `
								<li class="list-group-item">
									<div class="row">
										<div class="py-3 py-xl-0 col-xl-3 d-flex justify-content-center align-items-center">
											<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}${val.Gallery[0].path}&w=512&h=288&o=1&m=thumbnail&q=80" class="img-fluid" alt="${val.Title}">
										</div>
										<div class="col-xl-9">
											<a class="text-dark stretched-link" href="${moreItemsURL}/${val.Alias_slug}"><h3 class="serif-font">${val.Title}</h3></a>
											<p class="mb-2">${val.Intro_text}</p>
											<small>${newsDate(val.Date)}</small>
										</div>
									</div>
								</li>
							`;
							$(`#${module}ListItem`).append(newsListingTemplate);
						}
						
					}
				});
		})
		.then(function(){
            var galleryTop = new Swiper(`.news-swiper-container`, {
                // spaceBetween: 10,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                loop: true,
                loopedSlides: 4,
                autoHeight: true
			});
        });
        //#endregion Main data

       
}


function getAllUrlParams(url) {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // set parameter name and value (use 'true' if empty)
            var paramName = a[0];
            var paramValue = typeof(a[1]) === 'undefined' ? true : a[1];

            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

            // if the paramName ends with square brackets, e.g. colors[] or colors[2]
            if (paramName.match(/\[(\d+)?\]$/)) {

                // create key if it doesn't exist
                var key = paramName.replace(/\[(\d+)?\]/, '');
                if (!obj[key]) obj[key] = [];

                // if it's an indexed array e.g. colors[2]
                if (paramName.match(/\[\d+\]$/)) {
                    // get the index value and add the entry at the appropriate position
                    var index = /\[(\d+)\]/.exec(paramName)[1];
                    obj[key][index] = paramValue;
                } else {
                    // otherwise add the value to the end of the array
                    obj[key].push(paramValue);
                }
            } else {
                // we're dealing with a string
                if (!obj[paramName]) {
                    // if it doesn't exist, create property
                    obj[paramName] = paramValue;
                } else if (obj[paramName] && typeof obj[paramName] === 'string') {
                    // if property does exist and it's a string, convert it to an array
                    obj[paramName] = [obj[paramName]];
                    obj[paramName].push(paramValue);
                } else {
                    // otherwise add the property
                    obj[paramName].push(paramValue);
                }
            }
        }
    }

    return obj;
}
function newsDate(date){
	var mydate = new Date(date);

	var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][mydate.getMonth()];
	var str = month + ' ' + mydate.getDate() + ',' + mydate.getFullYear();
	return str;
}