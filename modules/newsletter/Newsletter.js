function newsletterLoadContent() {
	listingPage = true;
	module = "newsletter";
	var mainTemplate = /*html*/ `
	<div id="introduction">
			<div class="container pt-5">
				<div class="row">
					<div class="col-12" id="introductionText"></div>
				</div>
				<div class="row pt-5">
					<div class="col-12" id="form">
						<!-- Begin Mailchimp Signup Form -->
						<div id="mc_embed_signup">
							<form action="https://gmail.us20.list-manage.com/subscribe/post?u=dd2da121ef9e37c8fc276a52c&amp;id=6a93343713" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<div id="mc_embed_signup_scroll">
									<div class="row">
										<div class="col-xl-6">
											<div class="mc-field-group form-group">
												<label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label>
												<input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
											</div>
											<div class="mc-field-group form-group">
												<label for="mce-FNAME">First Name </label>
												<input type="text" value="" name="FNAME" class="form-control" id="mce-FNAME">
											</div>
											<div class="mc-field-group form-group">
												<label for="mce-LNAME">Last Name </label>
												<input type="text" value="" name="LNAME" class="form-control" id="mce-LNAME">
											</div>
										</div>
										<div class="col-xl-6">
											<div id="mergeRow-gdpr" class="mergeRow gdpr-mergeRow content__gdprBlock mc-field-group">
												<div class="content__gdpr">
													<label><h4>Marketing Permissions</h4></label>
													<p>Please select all the ways you would like to hear from ESPA:</p>
													<fieldset class="mc_fieldset gdprRequired mc-field-group mb-3" name="interestgroup_field">
														<div class="custom-control custom-checkbox custom-control-inline">
															<input type="checkbox" id="gdpr_31001" name="gdpr[31001]" value="Y" class="av-checkbox custom-control-input ">
															<label class="checkbox subfield custom-control-label" for="gdpr_31001"><span>Email</span></label>
														</div>
														<div class="custom-control custom-checkbox custom-control-inline">
															<input type="checkbox" id="gdpr_31005" name="gdpr[31005]" value="Y" class="av-checkbox custom-control-input">
															<label class="checkbox subfield custom-control-label" for="gdpr_31005"><span>Direct Mail</span></label>
														</div>
														<div class="custom-control custom-checkbox custom-control-inline">
															<input type="checkbox" id="gdpr_31009" name="gdpr[31009]" value="Y" class="av-checkbox custom-control-input">
															<label class="checkbox subfield custom-control-label" for="gdpr_31009"><span>Customized Online Advertising</span></label>
														</div>
													</fieldset>
													<p>You can unsubscribe at any time by clicking the link in the footer of our emails. For information about our privacy practices, please visit our website.</p>
												</div>
												<div class="content__gdprLegal">
													<p>We use Mailchimp as our marketing platform. By clicking below to subscribe, you acknowledge that your information will be transferred to Mailchimp for processing. <a href="https://mailchimp.com/legal/" target="_blank">Learn more about Mailchimp's privacy practices here.</a></p>
												</div>
											</div>
											<div id="mce-responses" class="clear">
												<div class="response" id="mce-error-response" style="display:none"></div>
												<div class="response" id="mce-success-response" style="display:none"></div>
											</div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
											<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_dd2da121ef9e37c8fc276a52c_6a93343713" tabindex="-1" value=""></div>
										</div>
										<div class="col-xl-12 text-center">
											<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-secondary btn-lg">
										</div>
									</div>
								</div>
							</form>
						</div>
						<!--End mc_embed_signup-->
					</div>
				</div>
			</div>
		</div>
		<div id="secitonData">
			<div class="container">
				<div class="row py-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_newsletter' + language]}</span></h2>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-12 card-columns" id="sectionDataContainer"></div>
				</div>
			</div>
		</div>
	`;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + module + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			console.log(data);
			//#region Introduction
			let IntroductionText = data['Introduction_text' + language];
			$('#introductionText').append(IntroductionText);
			//#endregion Introduction
			
			//#region Section data
			for (let [index,value] of data['Previous' + language].entries()) {
				let sectionTemaplate = /*html*/ `
					<div class="card border-0 mb-5">
						<a href="${value.value.Link}" class="btn btn-link" target="_blank"><h5 class="serif-font">${value.value.Title}</h5></a>
					</div>
				`;
				$('#sectionDataContainer').append(sectionTemaplate);
				
			}
			
			//#endregion Section data

		});
}