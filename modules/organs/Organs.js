function organsLoadContent() {
	listingPage = true;
	module = "organs";
	var mainTemplate = /*html*/ `
		<div id="introduction" >
			<div class="container pt-5">
				<div class="row">
					<div class="col-12" id="introductionBody"></div>
				</div>
			</div>
		</div>
		<div id="secitonData">
			<div class="container">
				<div class="row pt-5 pb-4 pb-sm-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_organs' + language]}</span></h2>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-sm-6 col-lg-3" id="sectionDataContainerLeft">

					</div>
					<div class="col-12 col-sm-6 col-lg-3" id="sectionDataContainerMiddle">

					</div>
					<div class="col-12 col-lg-6" id="sectionDataContainerRight">

					</div>
				</div>
			</div>
		</div>
    `;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + module + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			// console.log(data);
			//#region Introduction
			let IntroductionText = data['Organs_text' + language];
			$('#introductionBody').append(IntroductionText);
			//#endregion Introduction
			
			//#region Section data
			for (let [index,value] of data['Sections_data' + language].entries()) {
				let colWide = '';
				let sectionTemaplate = /*html*/ `
					<div class="border-0 my-3">
						<h3 class="row mx-0 serif-font text-primary mb-3">${value.value.Title}</h3>
						<div class="row mx-0 ${index}-member-data"></div>
					</div>
				`;

				if (index < 3) {
					$('#sectionDataContainerLeft').append(sectionTemaplate);
				} else if (index >= 3  && index < 6 ) {
					$('#sectionDataContainerMiddle').append(sectionTemaplate);
				} else {
					$('#sectionDataContainerRight').append(sectionTemaplate);
					colWide = "col-sm-6"
				}				
				// console.log('index - ' + index);
				if (value.value.Members) {
					for (let [ind, val] of value.value.Members.entries()) {
						// console.log('ind - ' + ind);
						let memberTemplate = /*html*/ `
							<div class="col-12 ${colWide} mb-4 border-0">
								<h5 class="mb-2">${val.value.Title}</h5>
								<div class="small ${index}-${ind}-email"></div>
								<div class="small">${val.value.Country}</div>
							</div>
						`;
						$(`.${index}-member-data`).append(memberTemplate);
						if (val.value.Email.length > 0) {
							for (let v of val.value.Email) {
								$(`.${index}-${ind}-email`).append(`<a href="mailto:${v.value}">${v.value}</a>`)
							}
						} 
					}
				}
			}
			//#endregion Section data
		});
}