function recoveryLoadContent() {
	listingPage = true;
	module = "recovery";
	var mainTemplate = /*html*/ `
		<div id="introduction" >
			<div class="container py-5">
				<div class="row pt-4 pt-sm-5 pb-4 pb-sm-5">
					<div class="col-12">
						<h2 class="${module}-heading text-center"></h2>
					</div>
				</div>
				<div id="${module}Video" class="row"></div>
				<div class="row pt-3 p-md-5">
					<div class="col-12" id="introductionBody"></div>
				</div>
			</div>
		</div>
		
    `;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + module + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			//Append Title
			let Title = data['Title'+language];
			$(`.${module}-heading`).append(`<span class="text-center text-primary">${Title}</span>`);
			
			//Append if video
			if(data.Video){
				let VideoId = data.Video;
				let embedVideoTemplate = /*html*/ `
					<div class="video-container px-md-5">
						<iframe src="https://www.youtube.com/embed/${VideoId}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
						</iframe>
					</div>
				`;

				$(`#${module}Video`).append(embedVideoTemplate);
			}

			//Append description
			let IntroductionText = data['Description' + language];
			$('#introductionBody').append(IntroductionText);


		});
}