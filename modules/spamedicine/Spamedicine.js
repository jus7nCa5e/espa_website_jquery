function spamedicineLoadContent() {
	listingPage = true;
	module = "spamedicine";
	var mainTemplate = /*html*/ `
		<div id="introduction" >
			<div class="container pb-5 pt-3 pt-md-5">
				<div class="row pt-5 pb-4 pb-sm-5">
					<div class="col-12">
						<h2 class="section-heading"><span>${translationArray['title_spamedicine' + language]}</span></h2>
					</div>
				</div>
				<div class="row pb-0 pb-sm-3 pb-md-5">
					<div class="col-12" id="introductionBody"></div>
				</div>
			</div>
		</div>
		<div id="secitonData">
			<div class="container" id="sectionDataContainer"></div>
		</div>
    `;
	$("header").after(mainTemplate);
	fetch(getSingletoneURL + module + token)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			//#region Introduction
			// console.log(data);
			let IntroductionText = data['SPA_text' + language];
			$('#introductionBody').append(IntroductionText);
			//#endregion Introduction
			
			//#region Section data
			for (let [index,value] of data['Sections_data' + language].entries()) {
				
				let sectionTemaplate = /*html*/ `
					<div class="row">
						<div class="col-12">
							<h3>${value.value.Title}</h3>
							${value.value.Text}
						</div>
					</div>
					<div class="card-columns" id="${index}Brochures"></div>
				`;
				$('#sectionDataContainer').append(sectionTemaplate);

				if (value.value.Brochures) {
					for (let val of value.value.Brochures) {
						// console.log(val.value.File.path)
						let Image = val.value.Image.path;
						let button;
						if (val.value.Button_text) {
							console.log(val.value);
							button = /*html*/ `<a href="${streamSRV}/storage/uploads${val.value.File.path}" class="btn btn-secondary" target="_blank">${val.value.Button_text}</a> `;
						}
						let brochureTemplate = /*html*/ `
							<div class="card mb-3 border-0" >
								<div class="row no-gutters">
									<div class="col-4 col-lg-3 pt-3 align-self-center">
										<img src="${streamSRV}/api/cockpit/image${token}&src=${streamSRV}/storage/uploads${Image}&w=250&o=1&m=thumbnail&q=80" class="card-img img-fluid" alt="${val.value.Title}">
									</div>
									<div class="col-8 col-lg-9">
										<div class="card-body">
											<h5>${val.value.Title}</h5>
											${val.value.Text}
											${button}
										</div>
									</div>
								</div>
							</div>
							`;
						$(`#${index}Brochures`).append(brochureTemplate);
					}
				}
			}
			
			//#endregion Section data

			


		});
}