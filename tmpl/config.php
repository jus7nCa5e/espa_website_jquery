<?php

/* 
 * This is configuration file with all specific variables.
 * Pay attention to the first section
 * 
 */

//************** variables
$projectName = 'WAW / ESPA';
$streamSRV = 'https://cms.europeanspas.eu';
$token = '?token=3bf86289702a4a8f129d2ffdb3613f';
$saveToken = '?token=889912640dfa04b5953d1a57c25dba';
$usersToken = '?token=db1d09a3fed73858b28d64eb76c897';
$mainData = 'mainbig';
$defaultLanguage = 'en';
$otherLanguages = ['de'];
$cache = false;
$captive = false;
$multibase = false;
$production = false;

$streamSRVuploads = $streamSRV . '/storage/uploads/';
$getURL = $streamSRV . '/api/collections/get/';
$getSingletoneURL = $streamSRV . '/api/singletons/get/';
$getimages = $streamSRV . '/api/cockpit/image' . $token;
$getImageJS = $streamSRV . '/api/cockpit/image';
$getLanguages = $streamSRV . '/api/lokalize/project/General';
$getRegions = $streamSRV . '/api/regions/data/';
$getUsers = $streamSRV . '/api/cockpit/listUsers';
$saveUser = $streamSRV . '/api/cockpit/saveUser';
$authUser = $streamSRV . '/api/cockpit/authUser';
$saveURL = $streamSRV . '/api/collections/save/';
$formURL = $streamSRV . '/api/forms/submit/';
$loader = '/config/images/loader.svg';
$devider = '/config/images/devider.svg';
$imgPrefix = $streamSRV . '/api/cockpit/image'.$token.'&src='.$streamSRV.'/';


if ($cache) {
   $token = $token . '&rspc=1'; 
}

//********** modules/collections
$navigation = 'navigation';
$roomservice = 'roomservice';

$url = $getURL . $navigation . $token . '&simple=1';

