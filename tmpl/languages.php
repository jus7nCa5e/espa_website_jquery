<?php
//get lang $_GET['uri']) [0] - base, [1] - lang, [2] - module
    if (isset($urlArray) && !empty($urlArray)) {
        $getLang = $urlArray[1];
    } else {
        $getLang = 'default';
    }
    if (($getLang == $defaultLanguage) || ($getLang == '') || ($getLang == 'default')) { //default language
        $lang = '';
//    } elseif ($getLang == 'en') { //you can add more nondefault languages with || (or)
    } elseif (in_array($getLang, $otherLanguages)) { //checks if get languag is in otherLanguages array (this array is configured in config.php)
        $lang = '_' . $getLang;
    } else {
        $lang = '';
        $getLang = 'default';
        $module = '404';
    }