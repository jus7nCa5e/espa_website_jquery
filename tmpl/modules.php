<?php

$URI = filter_input(INPUT_GET, 'uri', FILTER_SANITIZE_STRING); //safer variant to $_GET['uri']
//get module - $_GET['uri']) [0] - base, [1] - lang, [2] - module
if (isset($URI)) {
    $urlArray = explode("/", $URI);
    if (count($urlArray) <= 2) {
        $module = 'home';
    } else {
        $module = $urlArray[2];
    }
} else {
    $urlArray = []; //empty array
    $module = 'home';
}
