<?php

include("config.php");

$data_string = json_encode($_POST);
$chForm = curl_init();
curl_setopt($chForm, CURLOPT_URL, $saveURL . 'newsletter' . $saveToken);
curl_setopt($chForm, CURLOPT_RETURNTRANSFER, true);
curl_setopt($chForm, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($chForm, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($chForm, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
);
$resultForm = curl_exec($chForm);
curl_close($chForm);
?>