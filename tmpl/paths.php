<?php
//define base URL
$currentPath = filter_input(INPUT_SERVER, 'PHP_SELF'); //old $_SERVER['PHP_SELF'];
if (!$currentPath) {
    $currentPath = $_SERVER['PHP_SELF'];
}
$pathInfo = pathinfo($currentPath);
$hostName = filter_input(INPUT_SERVER, 'SERVER_NAME'); //old $_SERVER['HTTP_HOST'];
if (!$hostName) {
    $hostName = $_SERVER['SERVER_NAME'];
}
// explode domain name
$domainArray = explode(".", $hostName);
$domainName = (array_key_exists(count($domainArray) - 2, $domainArray) ? $domainArray[count($domainArray) - 2] : "") . "." . $domainArray[count($domainArray) - 1];


/*  $stream = stream_context_create (array("ssl" => array("capture_peer_cert" => true)));
$read = fopen("https://www.stringmeteo.com/", "rb", false, $stream);
$cont = stream_context_get_params($read);
$var = ($cont["options"]["ssl"]["peer_certificate"]);
$result = (!is_null($var)) ? true : false;
echo $stream;
echo "<br>";
echo $read;
echo "<br>";
echo $cont;
echo "<br>";
echo $var;
echo "<br>";
echo var_dump($var);
echo "<br>";
echo $_SERVER['SERVER_NAME'];
echo $currentPath;
echo "<br>";
print_r($pathInfo);
echo "<br>";
echo $currentPath;
echo "<br>";
echo $_SERVER['PHP_SELF'];


$cars = array("Volvo", "BMW", "Toyota");
print_r($cars); */

$protocol = filter_input(INPUT_SERVER, 'HTTPS') == 'on' ? 'https://' : 'http://';   

$absoluteUrl = $protocol . $hostName . "/";
$baseUrl = $absoluteUrl;

//define router base
if (($domainName === "clientric.website") && ($captive === false)) {
    $routerBase = '/preview/';
    $absoluteUrl = $routerBase;
} elseif (($domainName === "clientric.website") && ($captive === true)) {
    $routerBase = '/preview/';
    $absoluteUrl = $routerBase;
} else {
    $routerBase = '/';
}

$pageBaseUrl = $protocol . $hostName . $routerBase;

//home module redirect
if (($module == 'home') && ($getLang == $defaultLanguage)) {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location:" . $absoluteUrl);
    exit();
}
