<?php
    
    $router = new AltoRouter(); //activate AltoRouter
    $router->setBasePath($routerBase); //set router base if needed

    $ch = curl_init();
    $curlConfig = array(
        CURLOPT_URL            => $url,
        CURLOPT_RETURNTRANSFER => true,
    );

    curl_setopt_array($ch, $curlConfig);
    $curlresult = curl_exec($ch);
    curl_close($ch);
    $result = $curlresult != "" ? json_decode($curlresult, true) : "";
    $nav_array = (object)[];
    
    if ($multibase) {
        $result =  $result[0]["Navigation"];
    }
    //this is where the magic happens
    foreach ($result as $index => $value) { //build $nav_array object with all module and collections values from matching alises of all nav items
        if ($multibase) { //not tested
            $value = $value["value"];
        }
        if (isset($value["children"])) {
            foreach ($value["children"] as $ind => $val) {
                if (isset($val["children"])) {
                    foreach ($val["children"] as $i => $v) {
                        if (isset($v["children"])) {
                            foreach ($v["children"] as $ideep => $vdeep) {
                                if (isset($vdeep["children"])) {
                                    foreach ($vdeep["children"] as $ideepest => $vdeepest) {
                                        $nav_array->{$vdeepest["Alias_slug"]} = $vdeepest["Module"];
                                    }
                                }
                                $nav_array->{$vdeep["Alias_slug"]} = [$vdeep["Module"], $vdeep["Collection"]];
                            }
                        }
                        $nav_array->{$v["Alias_slug"]} = [$v["Module"], $v["Collection"]];
                    }
                }
                $nav_array->{$val["Alias_slug"]} = [$val["Module"], $val["Collection"]];
            }
        }
        $nav_array->{$value["Alias_slug"]} = [$value["Module"], $value["Collection"]];
    }
    $urlLast = end ($urlArray); //last item from URL

    $urlPath = '';
    //if last item from url is in $nav_array.
    if (isset($nav_array->$urlLast[0])) {  //If it is inside, so last item is listing page (it's alias is in navigation collection)
        foreach($urlArray as $ind => $val) { //segment url array and remove first 2 items incl. language;
            if ($ind > 1) {
                $urlPath .= '/'. $val; //join the valid url
            }
        }
        $module = $urlLast; //$module is last item from URL
        
        if (count($urlArray) === 0) {
            $module = 'home';
            $key = 'home';
        } else {
            $key = $nav_array->$module[0]; //match the module value, according to alias
        }
        $router->map('GET', $getLang .  $urlPath, '/' . $getLang . $urlPath); //make path in the alto router
        $match = $router->match(); //dispatch the path in alto router
        $baseUrl = $baseUrl . '/' . $getLang . $urlPath;
        if (!$nav_array->$module[0] || ($nav_array->$module[0] === ''))  {
            $pathToJson = 'modules/404/param.json';
            header(filter_input(INPUT_SERVER, 'SERVER_PROTOCOL') . " 404 Not Found"); //old $_SERVER["SERVER_PROTOCOL"]
        } 
        else { //if url and alias didn't match - throw 404
            $pathToJson = 'modules/' . $key . '/param.json'; //call the path of the actual module
        }
        
    } elseif (count($urlArray) > 3) { //last item is not in navigation array and urlArray more than language and one item
        foreach($urlArray as $ind => $val) {
            if (($ind > 1) && ($ind < (count($urlArray) - 1))) { //segment url array and remove first 2 items incl. language, but exclude last item;
                $urlPath .= '/'. $val; //join the valid url
            }
        }
        $module = $urlArray[count($urlArray) - 2]; //get module from URL before last
        
        
        $router->map('GET', $getLang .  $urlPath . '/[:action]', '/' . $getLang .  $urlPath . '/?alias=[a:action]'); //make path in the alto router
        $match = $router->match(); //dispatch the path in alto router
        $baseUrl = $baseUrl . $module . '/' . $urlLast . '/' . $match["params"]["action"];
        if (!$nav_array->$module[0] || ($nav_array->$module[0] === '')) {
            $pathToJson = 'modules/404/param.json';
            header(filter_input(INPUT_SERVER, 'SERVER_PROTOCOL') . " 404 Not Found"); //old $_SERVER["SERVER_PROTOCOL"]
        } 
        else { //if url and alias didn't match - throw 404
            $key = $nav_array->$module[0]; //match the module value, according to alias
            $pathToJson = 'modules/' . $key . '/param.json'; //call the path of the actual module
        }
    } elseif (count($urlArray) === 0) {
        $pathToJson = 'modules/home/param.json';
    } 
    else {
        $pathToJson = 'modules/404/param.json';
        header(filter_input(INPUT_SERVER, 'SERVER_PROTOCOL') . " 404 Not Found"); //old $_SERVER["SERVER_PROTOCOL"]
    }

    $jsonFile = $pathToJson;
    $json = file_get_contents($jsonFile);
    $param = json_decode(html_entity_decode($json), true);

    //addLoadContent
if ($captive === true) {
    $addLoadContent = $param['loadInfo'][1];
} elseif ((isset($param['loadInfo'][2])) && (isset($_SESSION['login_user']))) { //added user support. If param.json has ['loadInfo'][2], load it
    $addLoadContent = $param['loadInfo'][2];
} else {
    $addLoadContent = $param['loadInfo'][0];
}
    
