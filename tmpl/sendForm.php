<?php

include("config.php");
$form_data = array("form" => $_POST);
$data_string = json_encode($form_data);
$chForm = curl_init();
curl_setopt($chForm, CURLOPT_URL, $formURL . $_POST['Page'] . $saveToken);
curl_setopt($chForm, CURLOPT_RETURNTRANSFER, true);
curl_setopt($chForm, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($chForm, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($chForm, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
);
$resultForm = curl_exec($chForm);
curl_close($chForm);
?>