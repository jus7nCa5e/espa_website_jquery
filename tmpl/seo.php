<?php
$getTitle = "";
$getDescription = "";
$getKeywords = "";
$getImage = "";

if (isset($match['params']['action'])) { //get request for inner pages 
    $metaTitle_array = (object) []; //array for meta title
    $metaDescription_array =  (object) []; //array for meta description
    $metakeywords_array =  (object) []; //array for meta keywords
    $metaimage_array =  (object) []; //array for meta image

    if ((isset($param['innerPages'][2])) && (isset($_SESSION['login_user']))) { //when user is logged
        $addLoadContent = $param['innerPages'][2];
    } else {	
        $addLoadContent = $param['innerPages'][0]; //getting function from param.json
    }
    
    $aliasSlug = $match['params']['action']; //inner page alias
    $moduleAlias = str_replace("/", "", parse_url($baseUrl, PHP_URL_PATH)); //get module alias from url and remove slashes
    //**from navigation collection array, get collection name
    //get current parent
    $parentUrl = $urlArray[count($urlArray) - 2];
    $collection = $nav_array-> $parentUrl[1]; //see routes.php
    
    
    if ((isset($collection)) && ($collection !== '')) {
        $getCollectionURL = $getURL . $collection . $token . '&simple=1&filter[Alias_slug]='.$aliasSlug; //get custom query
        $chSEO = curl_init();
        $curlConfigSEO = array(
            CURLOPT_URL => $getCollectionURL,
            CURLOPT_RETURNTRANSFER => true
        );

        curl_setopt_array($chSEO, $curlConfigSEO);
        $curResultSEO = curl_exec($chSEO);
        $resultSEO = $curResultSEO != "" ? json_decode($curResultSEO, true) : "";
        curl_close($chSEO);
        
        if (!empty($resultSEO)) { 
            foreach ($resultSEO as $index => $value) {
                $getTitle = isset($value['Meta_title' . $lang]) ? $value['Meta_title' . $lang] : "";
                $getDescription = isset($value['Meta_description' . $lang]) ? $value['Meta_description' . $lang] : "";
                $getKeywords = isset($value['Meta_keywords' . $lang]) ? $value['Meta_keywords' . $lang] : "";
                $getImage = isset($value['Meta_image']["_id"]) ? $value['Meta_image']["_id"] : "";
            }
        } else { //if result is empty, so the query is empty and url alias didn't match. Should perform another query to all collection and search for categories
            //*********************** NB! If there's a category, the collection name always should be name of the main collection plus '_category' ********************  */
            
            $getCollectionURL = $getURL . $collection .'_category' . $token . '&simple=1&populate=1&filter[Alias_slug]='.$aliasSlug; //get custom query for category! 
            $chSEO = curl_init();
            $curlConfigSEO = array(
                CURLOPT_URL => $getCollectionURL,
                CURLOPT_RETURNTRANSFER => true
            );
        
            curl_setopt_array($chSEO, $curlConfigSEO);
            $curResultSEO = curl_exec($chSEO);
            $resultSEO = $curResultSEO != "" ? json_decode($curResultSEO, true) : "";
            curl_close($chSEO);

            foreach ($resultSEO as $index => $value) {
                $getTitle = isset($value['Meta_title' . $lang]) ? $value['Meta_title' . $lang] : "";
                $getDescription = isset($value['Meta_description' . $lang]) ? $value['Meta_description' . $lang] : "";
                $getKeywords = isset($value['Meta_keywords' . $lang]) ? $value['Meta_keywords' . $lang] : "";
                $getImage = isset($value['Meta_image']["_id"]) ? $value['Meta_image']["_id"] : "";
            }
        }
    } else { //no collection
        if (isset($urlArray[3])) {
            $pageAlias = end ($urlArray);
        } else {
            $pageAlias = 'home';
        }
    
        $getTitle = $metaTitle_array->{$pageAlias};
        $getDescription = $metaDescription_array->{$pageAlias};
        $getKeywords = $metakeywords_array->{$pageAlias};
        $getImage = $metaimage_array->{$pageAlias};
    }
} else { //get meta data from navigation collection
    $metaTitle_array = (object) []; //array for meta title
    $metaDescription_array =  (object) []; //array for meta description
    $metakeywords_array =  (object) []; //array for meta keywords
    $metaimage_array =  (object) []; //array for meta image
    //this is where the magic happens

    //**
    //not fully tested
    //**
    foreach ($result as $index => $value) {
        if ($multibase) {
            $value = $value["value"];
        }

        isset($value["Meta_title" . $lang]) ? $metaTitle_array->{$value['Alias_slug']} = $value["Meta_title" . $lang] : $metaTitle_array->{$value['Alias_slug']} = "-";
        isset($value["Meta_description" . $lang]) ? $metaDescription_array->{$value['Alias_slug']} = $value["Meta_description" . $lang] : $metaDescription_array->{$value['Alias_slug']} = "-";
        isset($value["Meta_keywords" . $lang]) ? $metakeywords_array->{$value['Alias_slug']} = $value["Meta_keywords" . $lang] : $metakeywords_array->{$value['Alias_slug']} = "-";
        isset($value["Meta_image"]["_id"]) ? $metaimage_array->{$value['Alias_slug']} =  $value["Meta_image"]["_id"] : $metaimage_array->{$value['Alias_slug']} = "-";

        if (isset($value["children"])) {
            foreach ($value["children"] as $ind => $val) {
                isset($val["Meta_title" . $lang]) ? $metaTitle_array->{$val['Alias_slug']} = $val["Meta_title" . $lang] : $metaTitle_array->{$val['Alias_slug']} = "-";
                isset($val["Meta_description" . $lang]) ? $metaDescription_array->{$val['Alias_slug']} = $val["Meta_description" . $lang] : $metaDescription_array->{$val['Alias_slug']} = "-";
                isset($val["Meta_keywords" . $lang]) ? $metakeywords_array->{$val['Alias_slug']} = $val["Meta_keywords" . $lang] : $metakeywords_array->{$val['Alias_slug']} = "-";
                isset($val["Meta_image"]["_id"]) ? $metaimage_array->{$val['Alias_slug']} =  $val["Meta_image"]["_id"] : $metaimage_array->{$val['Alias_slug']} = "-";
                if (isset($val["children"])) {
                    foreach ($val["children"] as $i => $v) {
                        isset($v["Meta_title" . $lang]) ? $metaTitle_array->{$v['Alias_slug']} = $v["Meta_title" . $lang] : $metaTitle_array->{$v['Alias_slug']} = "-";
                        isset($v["Meta_description" . $lang]) ? $metaDescription_array->{$v['Alias_slug']} = $v["Meta_description" . $lang] : $metaDescription_array->{$v['Alias_slug']} = "-";
                        isset($v["Meta_keywords" . $lang]) ? $metakeywords_array->{$v['Alias_slug']} = $v["Meta_keywords" . $lang] : $metakeywords_array->{$v['Alias_slug']} = "-";
                        isset($v["Meta_image"]["_id"]) ? $metaimage_array->{$v['Alias_slug']} =  $v["Meta_image"]["_id"] : $metaimage_array->{$v['Alias_slug']} = "-";
                    }
                }
            }
        }
        
    } //extract alises
    if (isset($urlArray[2])) {
        $pageAlias = end ($urlArray);
    } else {
        $pageAlias = 'home';
    }

    $getTitle = isset($metaTitle_array->{$pageAlias}) ? $metaTitle_array->{$pageAlias} : '';
    $getDescription = isset($metaDescription_array->{$pageAlias}) ? $metaDescription_array->{$pageAlias} : '';
    $getKeywords = isset($metakeywords_array->{$pageAlias}) ? $metakeywords_array->{$pageAlias} : '';
    $getImage = isset($metaimage_array->{$pageAlias}) ? $metaimage_array->{$pageAlias} : '';
    
}

//get image from image api
$chImage = curl_init(); //curl image stream
$curlConfigImage = array(//set image parameters
    CURLOPT_URL => $getimages,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS => array('src' => $getImage, 'm' => 'fitToWidth', 'w' => 1200, 'd' => true)
);
curl_setopt_array($chImage, $curlConfigImage);
$resultImage = curl_exec($chImage);
curl_close($chImage);
//print meta data

$metaTitle = isset($getTitle) ? htmlspecialchars_decode($getTitle) : " ";
$metaDesc = isset($getDescription) ? htmlspecialchars_decode($getDescription) : " ";
$keywords = isset($getKeywords) ? htmlspecialchars_decode($getKeywords) : " ";
$metaImage = isset($resultImage) ? $resultImage : " ";


//get current URL for Facebook OpenGraph
$currentUrl = (filter_input(INPUT_SERVER, 'HTTPS') != 'on') ? 'http://' . filter_input(INPUT_SERVER, 'SERVER_NAME') : 'https://' . filter_input(INPUT_SERVER, 'SERVER_NAME');
$currentUrl .= filter_input(INPUT_SERVER, 'REQUEST_URI');


//get Google Analytics, TagManager or any other thirdparty script
$getDataURL = $getSingletoneURL . $mainData . $token;
$chData = curl_init();
$curlConfigData = array(
    CURLOPT_URL => $getDataURL,
    CURLOPT_RETURNTRANSFER => true
);

curl_setopt_array($chData, $curlConfigData);
$curResultData = curl_exec($chData);
$resultData = $curResultData != "" ? json_decode($curResultData, true) : "";
curl_close($chData);
$headTag = isset($resultData['Head_tag']) ? $resultData['Head_tag'] : "";
$bodyTag = isset($resultData['Body_tag']) ? $resultData['Body_tag'] : "";