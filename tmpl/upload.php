<?php

if(isset($_POST) == true){
    //generate unique file name
    $fileName = time().'_'.basename($_FILES["file"]["name"]);
    if (!is_dir('../uploads/')) {    
        mkdir('../uploads/', 0755, true);   
    }
    //file upload path
    $targetDir = "../uploads/";
    $targetFilePath = $targetDir . $fileName;
    
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]/uploads/" . $fileName;
    //allow certain file formats
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
    $allowTypes = array('pdf','doc','docx');
    
    if(in_array($fileType, $allowTypes)){
        //upload file to server
        if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
            //insert file data into the database if needed
            //........
            $response['status'] = 'ok';
            $response['filename'] = $actual_link;
            
        }else{
            $response['status'] = 'err';
        }
    }else{
        $response['status'] = 'type_err';
    }
    
    //render response data in JSON format
    echo json_encode($response);
    
}