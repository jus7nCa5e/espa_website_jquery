<?php

include("../config.php");
$username = $_POST['user'];
$password = $_POST['password'];
$passwordNew = $_POST['passwordNew'];
$passwordRenew = $_POST['passwordRenew'];


//get users
$chUsers = curl_init();
curl_setopt($chUsers, CURLOPT_URL, $getUsers . $usersToken);
curl_setopt($chUsers, CURLOPT_RETURNTRANSFER, true);
$resultUsers = curl_exec($chUsers);
curl_close($chUsers);
$dataUsers = json_decode($resultUsers, true);

if (in_array($username, array_column($dataUsers, 'user'))) { // search for username in all users array
    // Authenticate user
    session_start();
    $loginData = array("user" => $username, "password" => $password);
    $data_string = json_encode($loginData);
    $chUsers = curl_init();
    curl_setopt($chUsers, CURLOPT_URL, $authUser . $usersToken);
    curl_setopt($chUsers, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($chUsers, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($chUsers, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($chUsers, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );
    $resultUsers = curl_exec($chUsers);
    curl_close($chUsers);
    $dataUsers = json_decode($resultUsers, true);
    if (isset($dataUsers['error'])) { //authentication failed
        $response_array['status'] = $dataUsers['error'];
    } 
    else { //authentication success
        $response_array['status'] = 'success';
        // Save / update user
        $userData = array ("user" => array("_id" => $dataUsers['_id'], "password" => $passwordNew));
        $data_string = json_encode($userData);
        $chUsers = curl_init();
        curl_setopt($chUsers, CURLOPT_URL, $saveUser . $usersToken);
        curl_setopt($chUsers, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($chUsers, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($chUsers, CURLOPT_POSTFIELDS, $data_string);     
        curl_setopt($chUsers, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );  

        $resultUsers = curl_exec($chUsers);
        curl_close($chUsers);
        
    }
} else { // no such user
    $response_array['status'] = 'Wrong username';
}

echo $response_array['status'];
?>