<?php 

//session_start(); //started in index.php
if(isset($_SESSION['login_user'])){
    $chUsers = curl_init();
    curl_setopt($chUsers, CURLOPT_URL, $getUsers . $usersToken);
    curl_setopt($chUsers, CURLOPT_RETURNTRANSFER, true); 
    $resultUsers = curl_exec($chUsers);
    curl_close($chUsers);
    $dataUsers = json_decode($resultUsers, true);
    // print_r($dataUsers);
    $is_logged = false; 
    foreach ($dataUsers as $index => $value) {
        if ($_SESSION['login_user'] == $value['user']) {
            $is_logged = $value['user'];
        } 
    }
} 

$time = $_SERVER['REQUEST_TIME'];

/**
* for a 30 minute timeout, specified in seconds
*/
$timeout_duration = 1800;

/**
* Here we look for the user's LAST_ACTIVITY timestamp. If
* it's set and indicates our $timeout_duration has passed,
* blow away any previous $_SESSION data and start a new one.
*/
if (isset($_SESSION['LAST_ACTIVITY']) && 
   ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
    session_unset();
    session_destroy();
    session_start();
}

/**
* Finally, update LAST_ACTIVITY so that our timeout
* is based on it and not the user's login time.
*/
$_SESSION['LAST_ACTIVITY'] = $time;

?>
